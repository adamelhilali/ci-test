\incdir {inc-lan}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Réseaux locaux
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreA {Réseaux locaux}

\begin {frame} {Couches 1 et 2}

    Problèmes~:
    \begin {itemize}
	\item comment partager un médium unique ?
	\item comment coder l'information ?
	\item comment adresser un message à un destinataire ?
    \end {itemize}

    \medskip

    Technologies~:
    \begin {itemize}
	\item Token Ring
	\item Token Bus
	\item Ethernet
	\item WiFi
    \end {itemize}
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Partage du canal : Aloha
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Aloha}

\begin {frame} {Aloha}
    Contexte
    \begin {itemize}
	\item 1969
	\item Université d'Hawai -- Norman Abramson
	\item Aloha = Bonjour
	\item Réseau en étoile
	    \begin {itemize}
		\item ordinateur central à Honolulu
		\item terminaux dans les îles
	    \end {itemize}
	\item pas de câbles \implique transmission radio (100 km)
	\item un canal pour les communications terminaux $\rightarrow$ ordinateur
	    \\
	    canal partagé \implique collisions
	\item un canal pour les communications ordinateur $\rightarrow$ terminaux
	    \\
	    un seul émetteur possible \implique pas de collision
    \end {itemize}
\end {frame}

\begin {frame} {Aloha pur}
    Algorithme des terminaux~:
    \begin {enumerate}
	 \item s'il y a des données à émettre, les émettre
	    \label {aloha-pur}
	 \item si acquittement de l'ordinateur, alors ok
	 \item si pas d'acquittement reçu dans un délai court
	    \begin {enumerate}
		\item attendre un délai aléatoire
		\item goto \ref {aloha-pur}
	    \end {enumerate}
    \end {enumerate}
\end {frame}

\begin {frame} {Aloha pur}
    \begin {center}
	\fig {aloha} {}
    \end {center}

    Performances~: utilisation maximum du médium = 18~\%
\end {frame}

\begin {frame} {Aloha discrétisé}
    Meilleure utilisation du canal : Slotted Aloha (ou Aloha discrétisé)
    \begin {enumerate}
	 \item 1972
	 \item temps découpés en slots \\
	    \implique référence temporelle (ex : signal de synchronisation)
	 \item émission en début de slot
	\item moins de collisions : utilisation du medium à 36~\%
    \end {enumerate}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Partage du canal : CSMA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {CSMA}

\begin {frame} {CSMA}
    \begin {itemize}
	\item CSMA : Carrier Sense Multiple Access
	\item une station écoute le canal \textbf {avant} d'émettre
    \end {itemize}
\end {frame}

\begin {frame} {CSMA}
    Si la porteuse est actuellement occupée, la station souhaitant émettre~:
    \begin {itemize}
	\item CSMA non persistant~:
	    \begin {itemize}
		\item attend une durée aléatoire
	    \end {itemize}
	\item CSMA 1-persistant~:
	    \begin {itemize}
		\item émet dès la fin de l'émission en cours
	    \end {itemize}
	\item CSMA $p$-persistant~:
	    \begin {itemize}
		\item émet avec une probabilité $p$ à la fin de
		    l'émission en cours (sinon attend le début du
		    slot suivant)
		\item uniquement pour les canaux découpés en slots
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {CSMA}
    \begin {center}
	\fig {csma} {}
    \end {center}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Partage du canal : CSMA/CD et Ethernet
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {CSMA/CD et Ethernet}

\begin {frame} {CSMA/CD}
    CD = Collision detection

    \begin {enumerate}
	\item si trafic en cours, attente d'un délai aléatoire \\
	    (Ethernet : aléa $\in [1..16]$ slots)
	\item si pas de trafic, alors émission
	\item si écoute pendant l'émission indique une collision
	    alors arrêt immédiat, attente aléatoire et goto 1
	    \\
	    (Ethernet : émission d'un signal de brouillage de 48 bits
	    pour signaler la collision aux autres stations)
	\item abandon au bout d'un certain  nombre d'échecs
    \end {enumerate}
\end {frame}

\begin {frame} {CSMA/CD}
    Influence de la bande passante et du délai de transmission

    Soit $d$ le délai de transmission entre les deux stations les
    plus éloignées (i.e. longueur maximum du câble)

    \begin {center}
	\fig {csmacd-delai} {.7}
    \end {center}

    Une station doit attendre $2d$ après la fin de l'émission pour
    être sure qu'un message a pu être transmis sans collision
\end {frame}

%
% Ethernet
%

\begin {frame} {Ethernet -- Historique}
    \ctableau {\fA} {|cl|} {
	1973 & Xerox PARC, Bob Metcalfe, 3 Mb/s \\
	1979 & alliance DEC, Intel et Xerox \\
	1982 & 10 Mb/s \\
	1983 & norme IEEE 802.3 (10Base-5) \\
	1985 & norme 10Base-2 \\
	1990 & norme 10Base-T (RJ-45) \\
	1995 & Fast Ethernet (100Base-TX) \\
	1998 & Ethernet 1 Gb/s \\
	2002 & Ethernet 10 Gb/s \\
	2010 & Ethernet 40 et 100 Gb/s \\
    }

    Bob Metcalfe expliquant Ethernet~:
    \url {https://www.youtube.com/watch?v=Fj7r3vYAjGY}

\end {frame}

\begin {frame} {Support physique -- 10Base-5}

    \begin {itemize}
	\item 10 Mb/s (version originale en 1973 : 3 Mb/s)
	\item câble coaxial (jaune) \implique \textit {ThickNet}
	\item connexion de segments via des répéteurs
	    \begin {itemize}
		\item maximum 500 m par segment
		\item maximum 4 répéteurs \\
		    (\implique distance max entre deux stations = 2500 m)
		\item maximum 3 segments avec des stations \\
		    (\implique 2 segments pour prolonger le réseau)
		\item maximum 100 stations par segment
	    \end {itemize}
	\item résistances terminales de 50 $\Omega$ pour absorber le signal
	\item repères tous les 2,5 m (distance choisie pour ne
	    pas correspondre à la longueur d'onde)
	\item transceivers, prises vampire
    \end {itemize}

\end {frame}

\begin {frame} {Support physique -- 10Base-2}

    \begin {itemize}
	\item 10 Mb/s
	\item câble coaxial (noir) \implique \textit {ThinNet}
	\item prises BNC en T (attention à l'insertion ou au retrait)
	\item bouchons = résistances terminales de 50 $\Omega$
	\item distance max d'un segment = 185 m
	\item 30 stations maximum par segment
	\item mêmes règles que 10Base-5 pour les répéteurs \\
	    \implique distance max = 925 m (= 5 $\times$ 185 m)
    \end {itemize}

\end {frame}

\begin {frame} {Ethernet -- En-tête}
    Format Ethernet II (ou Ethernet DIX)~:
    \begin {center}
	\fig {eth-fmt-eth2} {.7}
    \end {center}

    \begin {itemize}
	\item Type : alloué par l'IEEE
	\item FCS : Frame Check Sequence (= Cyclic Redundancy Check)
    \end {itemize}

\end {frame}

\begin {frame} {Ethernet -- En-tête}
    Format Ethernet IEEE 802.3~:
    \begin {center}
	\fig {eth-fmt-8023} {.7}
    \end {center}

    \begin {itemize}
	\item DSAP/SSAP : Destination/Source Service Access Point
	\item Control : type de trame LLC
	\item SNAP : Sub-Network Access Protocol
    \end {itemize}
\end {frame}

\begin {frame} {Ethernet -- En-tête}

    Deux formats différents pour Ethernet ?

    \begin {itemize}
	\item Ethernet 802.3 presque jamais utilisé en pratique
	\item Comment distinguer ?
	    \begin {itemize}
		\item Ethernet II si Type > 1500
		\item Ethernet 802.3 si Longueur $\leq$ 1500
	    \end {itemize}
	    \implique suppose que l'IEEE n'attribue pas de type $\leq$ 1500
	    (vrai)
    \end {itemize}
\end {frame}

\begin {frame} {Ethernet}
    Préambule~:
    \begin {itemize}
	\item avant la trame elle-même
	\item 7 octets à 01010101
	\item 1 octet à 01010111 (Start frame delimiter)
	\item synchronisation des stations
    \end {itemize}

    Inter Frame Gap~:
    \begin {itemize}
	\item préparer la réception de la trame suivante
	\item 9,6 $\mu$s (i.e. 12 octets à 10 Mb/s)
	\item 0,96 $\mu$s (i.e. 12 octets à 100 Mb/s)
	\item etc.
    \end {itemize}

\end {frame}

\begin {frame} {Ethernet -- Longueur de trame}
    Longueur maximum : limitée arbitrairement à 1500 octets
    
    \begin {itemize}
	\item trame longue \implique délai d'attente pour les autres
	    stations

	\item trame longue \implique probabilité de corruption d'un bit

	\item trame longue \implique mémoire dans la carte réseau
	    \\
	    \implique raison réelle de la limitation

    \end {itemize}

\end {frame}

\begin {frame} {Ethernet -- Longueur de trame}
    Longueur minimum = 46 octets, pour que les trames (hors
    préambule) fassent plus de 64 octets. Pourquoi ?

    \begin {itemize}
	\item la collision doit être détectée par l'émetteur, donc
	    la collision doit arriver avant la fin de l'émission

	\item sur 2,5 km via 4 répéteurs, le signal met environ 51,2
	    $\mu$s pour faire un aller et retour d'un bout à l'autre

	\item à 10 Mb/s ($10^7$ b/s), un bit est émis en $10^{-7}$ s =
	    0,1 $\mu$s

	\item l'émetteur doit donc envoyer au minimum 51,2 / 0,1 =
	    512 bits = 64 octets pour émettre pendant 51,2 $\mu$s

    \end {itemize}

    Si moins de 46 octets de données : bourrage

\end {frame}

\begin {frame} {Ethernet -- Adresses}
    \begin {center}
	\fig {mac48} {.95}
    \end {center}

    OUI = Organizationally Unique Identifier \\
    \url{http://standards-oui.ieee.org/oui/oui.txt}
\end {frame}

\begin {frame} {Ethernet -- Adresses}
    \begin {itemize}
	\item premier bit = 0 : adresse ordinaire
	    \begin {itemize}
		\item 3 octets = OUI = numéro d'entité (ex: fabricant de
		    cartes Ethernet), alloué par l'IEEE
		\item 3 octets = alloués par le fabricant
	    \end {itemize}
	    \implique adresse d'interface Ethernet \textbf {unique}
	\item premier bit = 1 : adresse de groupe
	    \begin {itemize}
		\item cas particulier : tous les bits à 1 = adresse de
		    \textbf {diffusion générale} (ou \textit {broadcast})
		\item $2^{24}-1$ autres cas : adresse \textbf {diffusion restreinte}
		    (ou \textit {multicast})
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Ethernet -- Codage}
    Ethernet classique utilise le codage Manchester~:

    \begin {center}
	\fig {codage} {.7}
    \end {center}

    Envoi du préambule = signal de 10 MHz pendant 6,4 $\mu$s \\
    \implique synchronisation de l'horloge du destinataire
\end {frame}

\begin {frame} {Ethernet -- Retransmissions}
    En cas de collision :

    \begin {itemize}
	\item la station qui détecte la collision envoie un signal
	    de brouillage (48 bits) pour avertir les autres stations

	\item retransmission : modèle CSMA 1-persistant avec attente
	    aléatoire exponentielle :

	    \begin {itemize}
		\item le temps est divisé en slots de taille 51,2 $\mu$s
		    (aller-et-retour sur le chemin le plus long)
		\item après la $i^e$ collision, une station doit attendre
		    un nombre aléatoire de slots $\in [0, 2^i-1]$
		\item lorsque $i > 10$, le nombre de slots maximum est
		    plafonné à $2^{10}-1 = 1023$
		\item lorsque $i > 16$, l'émission est abandonnée
	    \end {itemize}

	    \implique délai total borné à
		$\sum_{i=1}^{10}(2^i-1) + 6(2^{10}-1)$
		 = $2^{11} + 6\times 2^{10} - 17$
		 = 8175 slots de 51,2 $\mu$s
		 $\simeq$ 0,4 s
    \end {itemize}
\end {frame}

\begin {frame} {Avantages et inconvénients d'Ethernet}
    Avantages~:
    \begin {itemize}
	\item simple
	\item pas de configuration
	\item robuste (aux interférences)
    \end {itemize}

    \medskip
    Inconvénients~:
    \begin {itemize}
	\item les collisions augmentent avec la charge
	\item envoi non déterministe (possibilité de famine)
	\item pas de priorité (toutes les stations sont égales)
	\item taille minimum = 64 octets (overhead)
    \end {itemize}
\end {frame}

\begin {frame} {Support physique -- 10Base-T}

    \begin {itemize}
	\item 10 Mb/s
	\item câble point à point \implique topologie en étoile
	\item paire torsadée non blindée, catégorie 3
	\item connecteur RJ-45
	\item connexion via des concentrateurs (\textit {hubs}) Ethernet
	\item distance max = 100 m (90 m + 10 m de jarretières)
    \end {itemize}

    Variante 10Base-F : utilisation de la fibre optique

\end {frame}

\begin {frame} {Support physique -- Câbles}
    Paire torsadée~: deux fils enroulés en hélice l'un autour de l'autre

    \begin {itemize}
	\item distance constante entre les fils
	\item diminue la diaphonie (interférence entre les fils)
    \end {itemize}

    Câbles multi-paires (couramment 4 paires).

    \medskip

    Blindage de métal entre paires, ou autour du câble~:

    \ctableau {\fC} {|lcc|} {
	Nom & Blindage entre paires & Blindage entre paires et câble \\ \hline
	UTP & non & non \\
	STP & oui & non \\
    }
    U = Unshielded, S = Shielded, TP = Twisted Pair

    Note : il existe d'autres types de câbles.
\end {frame}

\begin {frame} {Support physique -- Câbles}
    Catégorie~:

    \begin {itemize}
	\item Catégorie 3 (basé sur UTP) : téléphone, 10Base-T,
	    100Base-T4, etc.

	\item Catégorie 5 (basé sur UTP) : 100Base-TX, 1000Base-T,
	    (très courant)

	\item Catégorie 5e (basé sur UTP) : idem Cat5, avec tests de
	    certification plus sévères

	\item Catégorie 6 (basé sur UTP) : 10GBase-T

	\item Classe F (appelé aussi Catégorie 7), basé sur SFTP : 1000Base-TX

    \end {itemize}

    SFTP = câble écranté
\end {frame}

\begin {frame} {Support physique -- Câbles}
    Avec Ethernet 10Base-T~:

    \begin {center}
	\fig {10baset} {.9}
    \end {center}

%    Minimisation des interférences avec le codage Manchester~:
%    \begin {itemize}
%	\item -2,5 V et +2,5 V
%	    \implique différence de potentiel = 5~V, 
%	\item 0 V et 0 V \implique différence de potentiel = 0~V,
%	\item sur une période longue
%    \end {itemize}

    Croisement des paires émission/réception~:
    \begin {itemize}
	\item hub $\leftrightarrow$ station~: pas de
	    croisement
	\item station $\leftrightarrow$ station ou hub $\leftrightarrow$ hub :
	    croisement nécessaire
    \end {itemize}

\end {frame}

\begin {frame} {Support physique -- Concentrateur}

    Un concentrateur (\textit {hub\/})~:

    \begin {itemize}
	\item retransmet une trame reçue, sans la stocker, sur tous
	    les autres ports
	    \\
	    \implique fonctionne en \textit {half-duplex}
	\item reconnaît les préambules et détecte les collisions
	\item diffuse le signal de brouillage en cas de collision
    \end {itemize}

    \medskip

    Mais~:
    \begin {itemize}
	\item impact sur la sécurité : espionnage du trafic
	\item autrefois très utilisé (prix)
	\item aujourd'hui abandonné au profit des commutateurs
    \end {itemize}
\end {frame}

\begin {frame} {FastEthernet (100Base-TX)}

    \begin {itemize}
	\item 100 Mb/s
	\item utilisation de commutateurs
	\item support = câble catégorie 5, avec utilisation de 2 paires : \\
	    \begin {itemize}
		\item une paire commutateur $\rightarrow$ station
		\item une paire station $\rightarrow$ commutateur
	    \end {itemize}
	    \implique liaison full-duplex
    \end {itemize}

    \medskip
    100Base-FX~: deux paires de fibres multimodes (full duplex, max 2~km)

\end {frame}

\begin {frame} {Support physique -- Codage 4B/5B}

    Problème du codage Manchester : nécessite 100 \% de bande passante
    supplémentaire \\
    100Base-TX : codage 4B/5B avec NRZI
    % \implique tension moyenne = 0 V \implique moins d'interférences

    \begin {center}
	\fig {codage} {.7}
    \end {center}

\end {frame}

\begin {frame} {Support physique -- Codage 4B/5B}

    Codage de 4 bits sur 5~: jamais plus de 3 bits consécutifs à 0
    \\
    \implique évite une désynchronisation d'horloge

    \begin {center}
    \begin {tabular} {l@{~$\rightarrow$~}l}
	0000 & 11110 \\
	0001 & 01001 \\
	0010 & 10100 \\
	\multicolumn {2} {l} {etc} \\
    \end {tabular}
    \end {center}

    4B5B entraîne un overhead de 25 \% seulement (au lieu de 100~\% avec
    Manchester)

\end {frame}

\begin {frame} {FastEthernet -- Compatibilité}

    Mixage avec stations ou hubs en 10Base-T

    Autonégociation

\end {frame}

\begin {frame} {GigabitEthernet}

    1000Base-T~:
    \begin {itemize}
	\item 1 Gb/s
	\item 200 m
	\item support = câble catégorie 5e ou 6
	\item autonégociation
	\item configuration automatique de MDI/MDI-X (Medium Dependant
	    Interface crossover)
    \end {itemize}

    Variantes sur fibre optique~:
    \fB
    \begin {itemize}
	\item 1000Base-SX : multimodes, max 550~m (sur 50 $\mu$m) ou 220~m (sur 62,5 $\mu$m)
	\item 1000Base-LX : monomode, max 5~km (moins sur multimodes)
	\item 1000Base-LX10 (1000Base-LH) : monomode, max 10 km
	\item autres versions non standard (1000Base-EX, 1000Base-ZX, etc.)
    \end {itemize}

\end {frame}

\begin {frame} {GigabitEthernet}

    Compatibilité avec les hubs~:

    \begin {itemize}
	\item CSMA/CD
	\item 1 Gb/s \implique une trame de 64 octets est envoyée
	    en 0,512~$\mu$s \\
	    \implique trop rapide pour détecter une collision
    \end {itemize}

    D'où~:
    \begin {itemize}
	\item longueur minimum de trame = 512 octets \\
	    \implique bourrage par le matériel (sans intervention du logiciel)
	\item mode «~rafale~» pour l'émission de plusieurs petites trames
	    en une seule
    \end {itemize}

\end {frame}

\begin {frame} {GigabitEthernet}

    Extensions (non standards)~:

    \begin {itemize}
	\item contrôle de flux (1~Gb/s \implique 1 bit/ns)

	    Trame «~pause~» : attendre $n$ trames de 512~ns avant
	    d'envoyer une nouvelle trame ($n < 2^{16}-1$)

	\item Jumbo frames

	    Longueur de trame jusqu'à 9000 octets.
    \end {itemize}

\end {frame}

\begin {frame} {10 Gigabit Ethernet}

    \begin {itemize}
	\item jusqu'à 100 m en cuivre (Catégorie 6a)
	\item jusqu'à 40 km en fibre optique monomode
	\item plus de support de CSMA/CD (pas de compatibilité avec les hubs)
    \end {itemize}

\end {frame}

\begin {frame} {Commutation}

    Commutateur = ne transfère que les trames utiles

    \begin {center}
	\fig {commut} {.6}
    \end {center}

    Apprentissage des adresses

    \begin {itemize}
	\item inondation initiale
	\item écoute de l'adresse source
	\item expiration des entrées
	\item commutateurs en cascade
    \end {itemize}
\end {frame}

\begin {frame} {Commutation}

    Problème de sécurité~: \textit {port stealing}

    \begin {center}
	\fig {portsteal} {.7}
    \end {center}

    Le pirate émet une trame de niveau 2 contenant A comme adresse source,
    et sa propre adresse comme adresse de destination
    \\
    \implique le commutateur actualise sa table de \textit {forwarding}
    \\
    \implique la trame n'est pas propagée \implique la trame n'est pas
    détectable
    \\
    \implique le trafic à destination de A est capturé par P, et en
    silence~!

\end {frame}

\begin {frame} {Commutation}
    Une fois le port volé, il est possible de le rendre à A~:

    \begin {itemize}
	\item le pirate envoie une requête ARP en demandant l'adresse
	    de A
	\item la requête est diffusée, donc A répond
	\item la réponse de A provoque l'actualisation de la table de
	    \textit {forwarding} du commutateur

    \end {itemize}

    Exemple de protection~: commande \texttt {switchport port-security} sur
	les commutateurs Cisco

    \begin {itemize}
	\item configuration statique d'une ou plusieurs adresses MAC
	\item apprentissage dynamique des adresss MAC, avec interdiction
	    de changement de port
    \end {itemize}

\end {frame}

\begin {frame} {Aggrégation de liens -- 802.3ad/802.1AX}
    Besoin d'aggréger des liens pour~:

    \begin {itemize}
	\item augmenter la bande passante
	\item offrir de la redondance
    \end {itemize}

    \begin {center}
	\fig {8023ad} {.2}
    \end {center}

    \implique aggrégation de liens opérant à la même vitesse

    \medskip

    Protocoles~:
    \begin {itemize}
	\item EtherChannel (Kalpana, racheté par Cisco) : PAgP
	    (Port Aggregation Protocol) ou LACP
	\item 802.3ad : LACP (Link Aggregation Control Protocol) \\
	    802.3ad $\rightarrow$ 802.1AX (i.e. indépendant d'Ethernet)
    \end {itemize}

\end {frame}

\begin {frame} {Aggrégation de liens -- 802.3ad/802.1AX}
    Fonctionnement~:

    \begin {itemize}
	\item création d'un port «~virtuel~» pour l'aggrégation \\
	    \implique les ports physiques aggrégés ne sont plus
	    utilisés dans les tables de commutation
	\item partage statique en fonction d'une table de hâchage \\
	    (en fonction des adresses MAC, IP, numéros de ports, etc.) \\
	    \implique pas de stratégie dynamique de type «~\textit
		{round-robin}~» \\
	    \implique pas de déséquencement des trames
	\item protocole (LACP) : maintient à jour la liste des ports
	    physiques (après configuration initiale)

    \end {itemize}

\end {frame}

\begin {frame} {Spanning Tree -- arbre recouvrant -- 802.1D}
    Redondance de niveau 2 : utiliser un réseau maillé et transmettre
    les trames sans provoquer de boucle

    \medskip

    \implique Constitution d'un arbre recouvrant le graphe avec STP \\
    (Spanning Tree Protocol)

    \begin {center}
	\fig {stp} {.6}
    \end {center}

    \implique blocage de certains ports pour éviter les boucles

\end {frame}


\begin {frame} {Spanning Tree  -- arbre recouvrant -- 802.1D}
    Fonctionnement~:

    \begin {enumerate}
	\item élection du commutateur racine : plus petite valeur de
	    \textit {bridge-id} = <priorité, adresse MAC> \\
	    (prio = 0x8000, configurable)

	\item chaque commutateur détermine le port racine (RP)~: distance
	    (coût) minimum vers le commutateur racine

	\item sur chaque segment, élection du port désigné (DP)~:
	    le port du commutateur le plus direct vers la racine

	\item tout autre port (non DP ou non RP) est bloqué

    \end {enumerate}
\end {frame}


\begin {frame} {Spanning Tree  -- arbre recouvrant -- 802.1D}

    Paquets du protocole STP = Bridge Protocol Data Units~:
    \begin {itemize}
	\item configuration : calcul de l'arbre
	\item notification de changement de topologie
	\item acquittement de changement de topologie
    \end {itemize}

    \bigskip
    BPDU envoyés à l'adresse multicast 01:80:C2:00:00:00 \\
    \implique seuls les ponts les reçoivent
\end {frame}

\begin {frame} {Spanning Tree  -- arbre recouvrant -- 802.1D}
    Fonctionnement~:

    \medskip

    \begin {minipage} {.25\linewidth}
	\fig {stp-topo} {}
    \end {minipage}%
    \begin {minipage} {.75\linewidth}
    \begin{flushright}
    \fD
    \renewcommand {\arraystretch} {1.1}
    \begin {tabular} {|c|c|c|c|c|l|} \hline
	étape & src & port & \multicolumn {2} {c|} {BPDU} & action \\
	      &     & dest & RB & dist & \\ \hline
	1     & B   & 3    & B  &   0  & A ignore RB=B/0 via 1\\ \cline {3-6}
	      &     & 4    & B  &   0  & C apprend RB=B/0 via 5 \\ \hline
	2     & C   & 6    & B  &   1  & A ignore RB=B/1 via 2 \\ \hline
	3     & A   & 1    & A  &   0  & B apprend RB=A/0 via 3 \\ \cline {3-6}
	      &     & 2    & A  &   0  & C apprend RB=A/0 via 6 \\ \hline
	4     & B   & 4    & A  &   1  & C ignore RB=A/1 via 5 \\ \hline
	5     & C   & 5    & A  &   1  & B ignore RB=A/1 via 3 \\ \hline
    \end {tabular}
    \end{flushright}
    \end {minipage}

    \bigskip

    \implique A est élu racine (\textit {Root Bridge\/}) car il a le \textit
    {bridge-id} le plus bas. \\
    \implique le port racine (\textit {Root Port\/}) de B = 3, de C = 6 \\
    \implique le port désigné (\textit {Designated Port\/}) sur le
	segment AB est A1 (resp. B4 pour BC, resp. A2 pour AC) \\
    \implique le port C5 est bloqué

\end {frame}

\begin {frame} {Spanning Tree  -- arbre recouvrant -- 802.1D}
    \begin {minipage} {0.2\linewidth}
	\fig {stp-auto} {}
    \end {minipage}%
    \begin {minipage} {0.8\linewidth}
    États d'un port~:
    \fB
    \begin {itemize}
	\item Block~: état initial, aucun trafic ne passe, seuls les
	    BPDU sont écoutés
	\item Listen~: état transitoire, seuls les BPDU sont écoutés
	\item Learn~: état transitoire, trafic écouté (mais non
	    transmis) pour constituer la table de commutation
	\item Forward~: état normal de commutation
	\item Disable~: port non pris en compte dans le STP

    \end {itemize}
    \end {minipage}

    \bigskip

    Temps Block $\rightarrow$ Forward : 30 à 50 s
\end {frame}


\begin {frame} {Spanning Tree  -- arbre recouvrant -- 802.1D}
    802.1D : temps de convergence important (30 à 50 secondes)

    \implique Rapid STP (802.1w) en 2001~:
    \begin {itemize}
	\item acquittements \implique éviter d'attendre un délai
	\item possibilité de configurer des ports «~edge~»
	\item convergence~: 6 secondes
	\item messages périodiques «~hello~»~: 2 secondes
	\item expiration rapide~: après 3 «~hello~» non reçus
    \end {itemize}
\end {frame}


\begin {frame} {Réseaux virtuels - 802.1Q}
    Besoin~: gérer des réseaux indépendants en utilisant une
    infrastructure commune

    \begin {center}
	\fig {vlan-prc} {.8}
    \end {center}

    \begin {itemize}
	\item Réseau virtuel 1~: B, C, E, F, H
	\item Réseau virtuel 2~: A, D, G, I, J
    \end {itemize}
\end {frame}


\begin {frame} {Réseaux virtuels - 802.1Q}
    Définition des VLAN sur le commutateur. En principe~:

    \begin {itemize}
	\item les ports vers les stations sont configurés dans un VLAN
	    donné
	    \\
	    \implique protocole Ethernet standard
	\item les ports vers d'autres commutateurs doivent faire
	    transiter plusieurs VLAN
	    \\
	    \implique ajout au protocole Ethernet
    \end {itemize}

    Dans la pratique~: 
    \begin {itemize}
	\item les stations peuvent être des serveurs hébergeant
	    plusieurs machines virtuelles dans des VLAN distincts
	\item un commutateur distant peut ne pas être compatible
	    avec 802.1Q
    \end {itemize}
\end {frame}


\begin {frame} {Réseaux virtuels - 802.1Q}
    802.1Q (1988) \implique modification de la trame Ethernet~:

    \begin {center}
	\fig {8021q} {.7}
    \end {center}

    En-tête 802.1Q = tag~:
    \ctableau {\fC} {|cl|} {
	16 bits & identificateur de protocole (0x8100) \\
	3 bits & priorité 802.1p \\
	1 bit & trame jetable en cas de congestion \\
	12 bits & identificateur de VLAN \\
    }
\end {frame}


\begin {frame} {Réseaux virtuels - 802.1Q}
    Transport de VLAN sur des réseaux d'opérateurs \\
    \implique 
    extension~: «~QinQ~» (802.1ad, 1998)
    
    \begin {itemize}
	\item double en-tête 802.1Q
	\item type = 0x88a8 (anciennement 0x9100)
    \end {itemize}

\end {frame}


\begin {frame} {Réseaux virtuels - 802.1Q}
    Modifications à Ethernet~:

    \begin {itemize}
	\item protocole Ethernet
	\item étanchéïté des réseaux \implique
	    commutateur doit avoir une table de commutation
	    par VLAN
	\item Spanning Tree par VLAN
    \end {itemize}

\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Partage du canal : CSMA/CA et WiFi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {CSMA/CA et WiFi}

\begin {frame} {Problématique de la propagation radio}
    Médium partagé, mais...

    \begin {enumerate}
	\item ... une station ne peut recevoir et émettre en même temps
	    \\
	    \implique transmission \textit {half-duplex}
	    \\
	    \implique pas de détection de collision

	\item la portée n'est pas uniforme
	    \begin {itemize}
		\item problème de la station cachée
		\item problème de la station exposée
	    \end {itemize}

	    \implique le partage du médium est inégal
	    \\
	    \implique impossible d'écouter toutes les stations
    \end {enumerate}

\end {frame}

\begin {frame} {CSMA/CA -- Principes}
    CA = Collision Avoidance \\
    \implique
    éviter \textit {autant que possible} les collisions

    \bigskip

    CSMA/CA est un mécanisme général (comme CSMA/CD) pour partager
    un canal~:

    \begin {itemize}
	\item bus~: LocalTalk d'Apple
	\item sans-fil~: 802.11 (WiFi) ou 802.15.4 (réseaux de capteurs)
    \end {itemize}

\end {frame}

\begin {frame} {CSMA/CA -- Algorithme général}
    \begin {enumerate}
	\item une station souhaitant émettre écoute le canal
	\item si le canal n'est pas libre, la station attend
	    en écoutant
	\item lorsque le canal est libre, la station attend un
	    délai convenu IFS (\textit {Inter-Frame Spacing}) \\
	    \implique la durée du délai est représentative de la priorité
	\item la station choisit alors un délai d'attente aléatoire
	    $\in [0,cw]$ ($cw$ = \textit {contention window}, initialement
	    à 1) \\
	    si le canal devient occupé, le délai n'est plus
	    décompté pendant la durée d'occupation
	\item au bout du délai, si le canal n'est pas occupé, la station
	    émet les données
	\item si l'acquittement n'est pas reçu, $cw$ est doublé
	    et on recommence au début
    \end {enumerate}
\end {frame}

\begin {frame} {CSMA/CA -- Algorithme général}
    \begin {center}
	\fig {csma-ca} {.9}
    \end {center}
\end {frame}

\begin {frame} {CSMA/CA -- Algorithme général}
    Ajustements pour tenir compte de la réalité~:

    \ctableau {\fD} {|cp{.25\linewidth}p{.50\linewidth}|} {
	DIFS & Distributed Coordination Function IFS &
		temps d'attente initial \\
	SIFS & Short IFS & temps entre réception des données
		et émission de l'acquittement (basculement du tuner)
		\\
	EIFS & Extended IFS & remplace DIFS  si la précédente
		trame émise était en collision
		\\
    }

    SIFS < DIFS < EIFS

    \medskip

    De plus, chaque trame contient la durée de l'échange \\
    \implique ex~: l'en-tête des données comprend aussi les durées
    de SIFS et de l'acquittement

\end {frame}


\begin {frame} {CSMA/CA -- Communications radio}
    Problématique des communications radio~: la station cachée

    \begin {center}
	\fig {sta-cachee} {.6}
    \end {center}

    \begin {itemize}
	\item A ne peut détecter la collision provoquée
	    par la transmission D $\rightarrow$ C
	\item le signal reçu par B (A $\rightarrow$ B) est perturbé
	    par la communication D $\rightarrow$ C
    \end {itemize}

\end {frame}


\begin {frame} {CSMA/CA -- Communications radio}
    Problématique des communications radio~: la station exposée

    \begin {center}
	\fig {sta-exposee} {.3}
    \end {center}

    \begin {itemize}
	\item B émet vers A~:
	    \begin {itemize}
		\item C reçoit le signal (par exemple parce qu'il
		    est en hauteur)
		\item D ne reçoit pas le signal
	    \end {itemize}
	\item C ne peut donc pas émettre vers D
	    \begin {itemize}
		\item bien que D ne soit pas perturbé par l'émission de B
	    \end {itemize}
    \end {itemize}

\end {frame}


\begin {frame} {CSMA/CA -- Communications radio}
    Ces deux problèmes viennent de ce que l'émetteur choisit d'émettre
    en fonction de ce que l'\textit {émetteur} sait, et non de ce que
    le \textit {récepteur} peut recevoir

    Solution (parmi d'autres)~:

    \begin {enumerate}
	\item A envoie un message RTS (\textit {Request To Send}) à B
	\item si B reçoit RTS, il renvoie un message CTS (\textit {Clear
	    To Send}) à A
	\item A envoie alors la trame de données à B

    \end {enumerate}

    Mécanisme optionnel sur 802.11, rarement utilisé en pratique
    (coût trop élevé compte-tenu de la rareté des stations
    cachées/exposées)

\end {frame}

\begin {frame} {802.11}
    WiFi = nom « commercial » des normes IEEE 802.11*

    Deux modes d'utilisation~:

    \begin {itemize}
	\item mode «~ad-hoc~»~: coordination des stations entre elles
	    (sans point d'accès)
	\item mode «~infrastructure~»~: repose sur un point d'accès
	    \\
	    \implique le plus courant
    \end {itemize}

\end {frame}

\begin {frame} {802.11}

    \ctableau {\fD} {|ccccccc|} {
	\rcb \textbf {Norme} & \textbf {Date} & \textbf {Fréquence}
	    & \multicolumn {2} {c} {\textbf {Bande passante}}
	    & \multicolumn {2} {c|} {\textbf {Portée}} \\
	 & & & \textbf {max} & \textbf {typique} &
	 	\textbf {int} & \textbf {ext} \\ \hline
	802.11a & 1999 & 5 GHz & 54 Mb/s & 22 Mb/s & 25 m & 75 m \\
	802.11b & 1999 & 2,4 GHz & 11 Mb/s & 5-6 Mb/s & 35 m & 100 m \\
	802.11g & 2003 & 2,4 GHz & 54 Mb/s & 22 Mb/s & 25 m & 75 m \\
	802.11n & 2009 & 2,4/5 GHz & 540 Mb/s & 200 Mb/s & 50 m & 125 m \\
	802.11ac & 2014 & 5 GHz & 6 Gb/s & - & 20 m & 50 m \\
    }

    {\fC Note : pas de notion de débit typique avec 802.11ac
    car cela dépend de l'encombrement du spectre et de la distance.}

\end {frame}

\begin {frame} {802.11 -- Canaux}

    Décomposition du spectre 2,4 GHz en canaux (802.11b)~:

    \begin {center}
	\fig {80211-chan2} {.9}
    \end {center}

    \begin {itemize}
	\item canal 14 autorisé au Japon seulement
	\item interférences possibles entre canaux \\
	    \implique canaux 1, 6 et 11, voire 14
    \end {itemize}
\end {frame}

\begin {frame} {802.11 -- Canaux}

    Décomposition du spectre 5 GHz en canaux (802.11a)~:

    \begin {center}
	\fig {80211-chan5} {.9}
    \end {center}

    \begin {itemize}
	\item canaux décomposés en 52 sous-canaux de 300~kHz
	\item interférences possibles entre canaux \\
	    \implique canaux 34, 36, 42, etc.
	\item 802.11a~: mécanisme de choix dynamique du canal
    \end {itemize}
\end {frame}

\begin {frame} {802.11}
    \begin {center}
	\fig {80211-fmt} {.6}
    \end {center}

    \begin {itemize}
	\item Contrôle de trame~:
	    \begin {itemize}
		\item type de trame (données, acquittement, RTS, CTS, etc.)
		\item retransmission ou non
		\item trame de/vers le réseau extérieur
		\item trame chiffrée ou non
		\item ...
	    \end {itemize}
	\item Durée~: de l'échange complet (ex: y compris SIFS et
	    acquittement) en $\mu$s

	\item Adresse 3~: destination réelle, si passage par un point d'accès
	\item Séquence~: numéro de séquence de la trame

    \end {itemize}
\end {frame}


\begin {frame} {802.11}
    Utilisation de la troisième adresse (en mode «~infrastructure~»)~:

    \begin {center}
	\fig {80211-adr3} {.9}
    \end {center}

\end {frame}


\begin {frame} {802.11}
    Gestion des duplications~:

    \begin {itemize}
	\item chaque trame comporte~:
	    \begin {itemize}
		\item un numéro de séquence
		\item un bit «~retry~» (dans le champ «~contrôle~»)
	    \end {itemize}
	\item le récepteur mémorise, pour chaque adresse source,
	    le numéro de séquence de la dernière trame reçue

	    \implique dupliqué si retry = 1 et numéro de séquence
		identique \\
	    \implique ignoré par le récepteur

    \end {itemize}

\end {frame}

\begin {frame} {802.11}

    Les différents types de trames~:
    \begin {center}
	\fig {80211-ctl} {.6}
    \end {center}

\end {frame}

\begin {frame} {802.11}
    En mode «~infrastructure~»~: message «~Beacon~» =
    annonce périodique (toutes les 100 ms) par le point d'accès~:
    \begin {itemize}
	\item capacité du point d'accès (débits supportés)
	\item SSID (Service Set IDentity) : chaîne de caractères terminée
	    par un octet nul
	\item paramètres radio
    \end {itemize}

    Une station peut également interroger les points d'accès~:
    \begin {itemize}
	\item trame «~probe request~»~: la station envoie le SSID
	    souhaité et sa liste de capacités
	\item trame «~probe response~»~: analogue au «~beacon~»
    \end {itemize}
\end {frame}

\begin {frame} {802.11 -- Association et authentification}

    \begin {center}
	\fig {80211-prob} {.8}
    \end {center}
\end {frame}

\begin {frame} {802.11 -- Association et authentification}
    \begin {center}
	\fig {80211-assoc} {.6}
    \end {center}

    Authentification~:
    \begin {itemize}
	\item open-system
	\item shared-key~: repose sur un challenge WEP
    \end {itemize}

    Association~: le point d'accès renvoie un identificateur
    d'association dans la trame de réponse d'association, et
    commence à répondre aux requêtes sur le réseau filaire pour
    l'adresse MAC de la station.

\end {frame}

%\begin {frame} {802.11}
%
%    \Huge\bf
%
%    Veille
%
%    Fragments
%
%
%\end {frame}

> nslookup bambi.lptl.jussieu.fr	# requête de type A par défaut
Server:		127.0.0.53		# serveur DNS mandataire interrogé
Address:	127.0.0.53

Non-authoritative answer:		# réponse venant du cache du mandataire
Name:	bambi.lptl.jussieu.fr
Address: 134.157.8.33

> nslookup -norecurse -q=ns unistra.fr d.nic.fr  # requête de type NS envoyée au serveur de fr
Server:		d.nic.fr		# serveur contacté
Address:	2001:678:c::1		# son adresse (IPv6 seulement)

Non-authoritative answer:
*** Cant find unistra.fr: No answer	# le serveur ne connait pas la réponse...

Authoritative answers can be found from: # ... mais peut nous indiquer où la demander
unistra.fr	nameserver = ns1.u-strasbg.fr.
unistra.fr	nameserver = shiva.jussieu.fr.
unistra.fr	nameserver = ns2.u-strasbg.fr.
ns2.u-strasbg.fr	has AAAA address 2001:660:2402::3
ns1.u-strasbg.fr	has AAAA address 2001:660:2402::1
shiva.jussieu.fr	internet address = 134.157.0.129
ns2.u-strasbg.fr	internet address = 130.79.200.3
ns1.u-strasbg.fr	internet address = 130.79.200.1


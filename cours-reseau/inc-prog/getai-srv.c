#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <syslog.h>

#define	MAXLEN	1024
#define	SERVICE	"9000"		// ou un nom dans \texttt{/etc/services}
#define	MAXSOCK	32

void raler_log (char *msg)
{
    syslog (LOG_ERR, "%s: %m", msg) ;
    exit (1) ;
}

void usage (char *argv0)
{
    fprintf (stderr, "usage: %s [port]\n", argv0) ;
    exit (1) ;
}

void serveur (int in)
{
    int r, n = 0 ;
    char buf [MAXLEN] ;

    while ((r = read (in, buf, MAXLEN)) > 0)
	n += r ;
    syslog (LOG_ERR, "nb d'octets lus = %d\n", n) ;
}

void demon (char *serv)
{
  int s [MAXSOCK], sd, ns, r, opt = 1 ;
  struct addrinfo indic, *res, *res0 ;
  char *cause ;

  memset (&indic, 0, sizeof indic) ;
  indic.ai_family = PF_UNSPEC ;
  indic.ai_socktype = SOCK_STREAM ;
  indic.ai_flags = AI_PASSIVE ;
  if ((r = getaddrinfo (NULL, serv,  &indic, &res0)) != 0) {
    fprintf (stderr, "getaddrinfo: %s\n", gai_strerror (r)) ;
    exit (1) ;
  }

  ns = 0 ;
  for (res = res0; res && ns < MAXSOCK; res = res->ai_next) {
    s[ns]=socket(res->ai_family,res->ai_socktype,res->ai_protocol);
    if (s [ns] == -1) cause = "socket" ;
    else {
      setsockopt(s[ns],SOL_SOCKET,SO_REUSEADDR,&opt,sizeof opt) ;
      r = bind (s [ns], res->ai_addr, res->ai_addrlen) ;
      if (r == -1) {
	cause = "bind" ;
	close (s [ns]) ;
      }
      else {
	listen (s [ns], 5) ;
	ns++ ;
      }
    }
  }
  if (ns == 0) raler_log (cause) ;
  freeaddrinfo (res0) ;

  for (;;) {
    fd_set readfds ;
    int i, max = 0 ;

    FD_ZERO (&readfds) ;
    for (i = 0 ; i < ns ; i++) {
      FD_SET (s [i], &readfds) ;
      if (s [i] > max) max = s [i] ;
    }
    if (select (max+1, &readfds, NULL, NULL, NULL) == -1)
      raler_log ("select") ;

    for (i = 0 ; i < ns ; i++) {
      struct sockaddr_storage sonadr ;
      socklen_t salong ;
      if (FD_ISSET (i, &readfds)) {
	salong = sizeof sonadr ;
	sd = accept (s[i],(struct sockaddr *)&sonadr,&salong) ;
	if (fork () == 0) {
	  serveur (sd) ;
	  exit (0) ;
	}
	close (sd) ;
      }
    }
  }
}

int main (int argc, char *argv [])
{
    char *serv ;

    switch (argc)
    {
	case 1 : serv = SERVICE ; break ;
	case 2 : serv = argv [1] ; break ;
	default : usage (argv [0]) ; break ;
    }

    switch (fork ())
    {
	case -1 :
	    perror ("cannot fork") ;
	    exit (1) ;
	case 0 :		// le démon proprement dit
	    setsid () ; chdir ("/") ; umask (0) ;
	    close (0) ; close (1) ; close (2) ;
	    openlog ("exemple", LOG_PID | LOG_CONS, LOG_DAEMON) ;
	    demon (serv) ;
	    exit (1) ;
	default :
	    exit (0) ;
    }
}

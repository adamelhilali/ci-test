main ()
{
    serveur_tcp (0, 1) ;	// entrée et sortie standard
}

void serveur_tcp (int sock_in, int sock_out)
{
    while (read (sock_in, ...) > 0)
    {
        // traiter l'envoi et préparer la réponse
        write (sock_out, ...) ;
    }
}

int s = -1, r ; char *cause ;
struct addrinfo indic, *res, *res0 ;

memset (&indic, 0, sizeof indic) ;
indic.ai_family = PF_UNSPEC ;
indic.ai_socktype = SOCK_STREAM ;
if (getaddrinfo (host, serv,  &indic, &res0) != 0) {
    fprintf (stderr, "getaddrinfo: %s\n", gai_strerror (r)) ;
    exit (1) ;
}
for (res = res0 ; res != NULL ; res = res->ai_next) {
    s = socket(res->ai_family,res->ai_socktype,res->ai_protocol);
    if (s == -1) cause = "socket" ;
    else {
	if (connect (s, res->ai_addr, res->ai_addrlen) == -1) {
	    cause = "connect" ;
	    close (s) ;
	    s = -1 ;
	}
	else break ;
    }
}
if (s == -1) perror (cause) ;
freeaddrinfo (res0) ;

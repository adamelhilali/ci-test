struct  protoent {
  char *p_name;     // nom officiel
  char **p_aliases; // liste des synonymes
  int  p_proto;     // numéro de protocole
}

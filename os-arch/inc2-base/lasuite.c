void therest (void)
{
    int *code = 102 ;		// address of code stored by hardware at interrupt or exception

    switch (*code) {
	case 0 : process_interrupt () ; break ;
	case 1 : process_div_by_zero () ; break ;
	case 2 : process_invalid_instr () ; break ;
	case 3 : process_privileged_instr () ; break ;
	case 4 : process_trap () ; break ;
	default : panic ("invalid exception") ; break ;	// always anticipate theimpossible
    }
}

calculation:
    // calculations cannot be 
    // performed without 
    // backup in the stack 
    ld   [%sp+1],%a
    mul  [%sp+2],%a
    push %a
    ld   [%sp+4],%a
    mul  [%sp+5],%a
    add  [%sp],%a
    add  1,%sp
    rtn

sum_except:
    ld   [%sp+1],%a
    cmp  [%sp+3],%a
    jeq  a1
    add  [%sp+2],%a
    jmp  a2
a1: ld   [%sp+2],%a
// end of if
a2: mul  2,%a
    rtn

sum_even_except:
    push %b
    // start=sp+2, end=sp+3, except=sp+4
    ld   [%sp+2],%b // B $\leftarrow$ i
    ld   0,%a	    // A $\leftarrow$ s
    jmp  b2
// start of loop 
b1: cmp  [%sp+4],%b // except == i?
    jeq  b3         // jump if equal 
    add  %b,%a      // s += i
b3: add  2,%b       // i += 2
b2: cmp  [%sp+3],%b // compare end and i
    jge  b1         // jump if end $\geq$ i
    pop  %b
    rtn

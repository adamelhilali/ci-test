# \texttt{run} : executes the \texttt{fact.s} file from the \texttt{main} label to the \texttt{reset} instruction
> ./z33-cli run fact.s main
Reading program path="fact.s"
Executing instruction "push 5"
...
Executing instruction "reset"
End of program registers=%a = 120 | %b = 0 | %pc = 103 | %sp = 9999 | %sr = ZERO

# \texttt{run} with \texttt{-i} option : starts execution in \frquote{interactive} mode
> ./z33-cli run -i fact.s main
Running in interactive mode. Type "help" to list available commands.
>> registers			# displays register contents 
Registers: %a = 0 | %b = 0 | %pc = 100 | %sp = 10000 | %sr = (empty)
>> step				# executes the following instruction 
Executing instruction "push 5"
>> break factorial		# sets a breakpoint at the \frquote{factorial} address (here at adress 200)
Setting a breakpoint address=200
>> continue			# starts execution up to the \texttt{reset} instruction or a breakpoint 
Stopped at a breakpoint address=200
>> registers
...

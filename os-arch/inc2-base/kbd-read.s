#define KBD_REG_STATUS		20
#define KBD_REG_DATA		21

#define KBD_STA_READY           0x01		// see controller's doc

read_keyboard:
// wait for a key to be pressed : loop until bit Ready $\neq$ 1
	in   [KBD_REG_STATUS],%a		// a $\leftarrow$ controller status
	and  KBD_STA_READY,%a			// a $\leftarrow$ a $\wedge$ 0x01
	cmp  0,%a				// compare 0 to a
	jeq  read_keyboard			// loop if a = 0 (nothing to read)
// read the key 
	in   [KBD_REG_DATA],%a			// a $\leftarrow$ key code 
	rtn

> cc -c example.c               # generates example.o
> nm example.o
0000000000000000 T main		# T : symbol defined in the "text" area 
                 U printf	# U : used symbol, but is not defined here 
0000000000000004 C x		# C : "common" (can be defined several times)

> cc -static example.o		# generates a.out (and assigns addresses)
> nm a.out | egrep ' (main|printf|x)$'
0000000000401ce5 T main         # the address is now assigned 
0000000000410ba0 T printf       # same for printf, function included in the file
00000000004c3300 B x		# symbol placed in the BSS ("data" segment) $\Rightarrow$ zone initialized at 0

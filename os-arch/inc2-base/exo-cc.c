int compute (int a, int b, int c, int d) {
    return (a*b) + (c*d) ;
}

int sum_except (int x, int y, int except) {
    int s = 0 ;
    if (x != except)
	s = x+y ;
    else s = y ;
    return s*2 ;
}

int sum_even_except (int start, int end, int except) {
    int s = 0 ;
    for (int i = start ; i <= end ; i += 2)
	if (i != except)
	    s += i ;
    return s ;
}

#define KBD_REG_CTRL		KBD_REG_STATUS	// same port number
#define	KBD_CTRL_LED		0x01		// codes extracted from the doc
#define	KBD_DATA_LED_ON		0x08		// light a keyboard LED
#define	KBD_LED_NUMLOCK		1		// keyboard LED numbers 
#define	KBD_LED_CAPSLOCK	2

#define	CAPS_ON	  KBD_DATA_LED_ON|KBD_LED_CAPSLOCK // value calculated by assembler

// turn on the "Caps Lock" LED
light_caps_lock:
	out CAPS_ON,[KBD_REG_DATA]		// LED number + light 
	out KBD_CTRL_LED,[KBD_REG_CTRL]		// send command to light LED
	rtn

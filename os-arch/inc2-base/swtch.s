    // void swtch (struct proc *elected)
    swtch:
	// save the current registers in the old stack, except:
	// - PC : already saved, it is the return address of swtch 
	// - A : not needed because it is the return value (when there is one)
250	push %b
251	push %sr
	// save sp in the old process descriptor 
252	ld   [1000],%a		// a $\leftarrow$ curproc
253	st   %sp,[%a+5]		// curproc->kstack $\leftarrow$ sp

	// change curproc : curproc $\leftarrow$ argument 1 = \texttt{elected}
254	ld   [%sp+4],%a		// a $\leftarrow$ elected 
255	st   %a,[1000]		// curproc $\leftarrow$ a = elected 

	// change of kernel stack 
256	ld   [%a+5],%sp		// sp $\leftarrow$ elected->kstack $\Rightarrow$ kernel stack of the new process

	// restore new process registers
257	pop  %sr
258	pop  %b

259	rtn			// back to swtch caller for new process 

int sys_read (void)			// function called by process\_trap
{
    char *buf ; int fd, n ;		// read arguments

    // recovering arguments
    ...

    // loop on n (as long as there are bytes to read)
    while (n > 0) {
	...
	if (no_data_to_read) {		// ouch ouch ouch...
	    curproc->state = WAITING ;   // I am not "ready to run" anymore 
	    struct proc *elected ;		// new process
	    elected = process_ready () ;	// choose a process that is "ready to run" 
	    swtch (elected) ;		// \textbf{change to the selected process}
	    // when we get here, we are back on the processor 
	}
    }

    // rest of primitive code
    ...
}

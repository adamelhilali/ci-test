f:  sub  1,%sp		// leave room for y

    ld   [%sp+2],%a	// a $\leftarrow$ argument x
    add  2,%a		// a $\leftarrow$ a+2
    push %a		// stack x+2 (v)
    push 10		// stack 10 (u)
    call g
    add  2,%sp		// unpack the 2 arguments
    st   %a,[%sp]	// y $\leftarrow$ result of g (in a)

    ld   [%sp],%a	// a $\leftarrow$ y (could be optimized)
    add  20,%a		// a $\leftarrow$ y+20

    add  1,%sp		// end of existence for y 
    rtn			// result in a

g:  ld   [%sp+1],%a	// a $\leftarrow$ u
    add  1,%a           // a $\leftarrow$ u+1
    push %b		// we'll use b and save it 
    ld   [%sp+3],%b	// b $\leftarrow$ v
    add  2,%b           // b $\leftarrow$ v+2
    mul  %b,%a		// a $\leftarrow$ a*b = (u+1)*(v+2)
    pop  %b		// restore b
    rtn			// result in a 

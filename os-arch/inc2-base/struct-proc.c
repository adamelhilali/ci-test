struct proc {
    unsigned int a ;			// saving a
    unsigned int b ;			// saving b
    unsigned int pc ;			// saving pc
    unsigned int sr ;			// saving sr
    unsigned int sp ;			// saving sp
    unsigned int *kstack ;		// kernel stack top address 
    ...
} ;

struct proc proc [NPROC] ;		// array of all process descriptors 

struct proc *curproc ;			// pointer to the current process 

int sys_open (void)			// function called by process\_trap
{
    int *sp ;
    char *path ; int mode, permissions ;	// open arguments

    sp = curproc->sp ;			// saved SP register (user stack) value 
    path = *sp ;			// first argument (yes, we take liberties with types...)
    mode = *(sp+1) ;			// second argument
    permissions = *(sp+2) ;			// third argument

    // rest of primitive code
    ...
}

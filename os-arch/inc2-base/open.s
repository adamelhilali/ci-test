myfct:	sub  1,%sp		// space for fd
	push 438		// 438 = 0666
	push 577		// 577 =  01101 = O\_WRONLY|O\_CREAT|O\_TRUNC
	push 2200		// address of "foo" string in memory
	ld   5,%a		// 5 = "open" primitive number
	trap			// \textbf{exception} \implique kernel call 
	add  3,%sp		// unstack the 3 arguments
	st   %a,[%sp]		// fd $\leftarrow$ return of open
	ld   [%sp],%a		// return value $\leftarrow$ fd (optimizable)
	add  1,%sp		// free the fd location
	rtn



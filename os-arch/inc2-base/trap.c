int (*primtab [NPRIM]) (void) = {	// array of pointers to functions returning an integer 
    [1] = sys_fork,			// primitive number 1 : fork
    [2] = sys_exit,			// primitive number 2 : exit
    ...
    [5] = sys_open,			// primitive number 5 : open
    ...
} ;
struct proc *curproc ;			// current process descriptor (adr 1000 variable)

void process_trap (void)		// called by the interrupts and exceptions handling code
{
    int number, return ;

    number = curproc->a ;		// primitive number $\leftarrow$ saved A register value
    return = (*primtab [number]) () ;	// primitive call
    curproc->a = return ;		// saved A value $\leftarrow$ primitive return
}

	interrupt:			// label placed at address 200
	// save b in the temporary space (tmp) located from address 100
200		st   %b,[103]		// tmp[3] $\leftarrow$ b
	// find the address of the current process descriptor:
	// we assume that the variable at address 1000 (= curproc) contains its address
201		ld   [1000],%b		// b $\leftarrow$ contenu de la case 1000 (= curproc)
202		st   %a,[%b]		// *curproc = curproc[0] $\leftarrow$ a
	// saving b
203		ld   [103],%a		// restore the b value saved in 200
204		st   %a,[%b+1]		// curproc[1] $\leftarrow$ tmp[3] (= b)
	// saving pc
205		ld   [100],%a
206		st   %a,[%b+2]		// curproc[2] $\leftarrow$ tmp[0] (= pc)
	// saving sr
207		ld   [101],%a
208		st   %a,[%b+3]		// curproc[3] $\leftarrow$ tmp[1] (= sr)
	// saving sp
209		st   %sp,[%b+4]		// curproc[4] $\leftarrow$ sp

	// we assume that the process descriptor contains the address of the process kernel stack
210		ld   [%b+5],%sp		// sp $\leftarrow$ curproc[5]

	// call the function to carry out further processing 
211		call therest 

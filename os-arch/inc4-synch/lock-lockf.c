void locking (int fd) {
  if (lockf (fd, F_LOCK, 0) == -1)
    grumble (...) ;
}

void unlocking (int fd) {
  if (lockf (fd, F_ULOCK, 0) == -1)
    grumble (...) ;
}

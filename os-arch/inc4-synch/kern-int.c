// naive version (and wrong)
void start_reading (struct desc_es *d) {
    // insert at tail of list (circular, list points to tail)
    d->next = (list == NULL) ? d : list->next ;
    list = d ;

    // if there are no pending requests, start ours
    if (d->next == d) {
	start_request (d) ;
    }

    // put the current process (or thread) on hold
    d->proc = curproc ;
    put_process_on_hold () ;
}

void process_int_disque (void) {
    struct desc_es *d ;

    // extract the oldest request, i.e. the one at the top of the list
    d = list->next ;		// top of the list because circular
    list = (d == list) ? NULL : list->next ;

    // set the corresponding process to the \frquote{ready to run} state
    wake_process (d->proc) ;

    // if there are still pending requests, start the oldest one
    if (list != NULL) {
	start_request (list->next) ;
    }
}

// corrected version
void start_reading (struct desc_es *d) {
    mask_interrupts () ;

    ...		// previous function version

    unmask_interrupts () ;
}

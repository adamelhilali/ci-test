condition c ;
mutex m ;
queue *q ;

void *thread (void *arg) {
  lock (&m) ;
  while (empty_queue(q)) {  // \alert{$\leftarrow$ loop}
    cwait (&c, &m) ;
  }
  d = extract_queue (q) ;
  unlock (&m) ;
  process (d) ;
}

void *thread_produce (void *arg) {
  d = read_data (...) ;
  lock (&m) ;	// if need of critical section
  add_queue (q, d) ;
  csignal (&c) ;
  unlock (&m) ;
}

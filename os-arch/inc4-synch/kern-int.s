start_request:
    ld   [%sp+1],%a	// the query descriptor
    ...			// send sector number to be read
    out  ...,[50]	// start reading
    rtn

#define FLAG_IE	0x100    // bit 8 in SR

mask_interrupts:	 // remove the IE bit from SR
    and  ~FLAG_IE,%sr	 // sr $\leftarrow$ sr AND all bits set to 1 except IE
    rtn

unmask_interrupts: // add the IE bit to SR
    or   FLAG_IE,%sr	 // sr $\leftarrow$ sr OR IE bit at 1
    rtn

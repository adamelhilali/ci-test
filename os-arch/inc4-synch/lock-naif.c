void locking (int *lock) {
  while (*lock != 0)
    ;                    // active wait
  *lock = 1 ;
}
void unlocking (int *lock) {
  *lock = 0 ;
}

condition c ;
queue *q ;

void *thread (void *arg) {
  if (empty_queue(q)) {		// if no data is already present 
    cwait (&c) ;		// wait for data 
  }
  d = extract_queue (q) ;	// extract the first available data from the queue
  process (d) ;			// process extracted data 
}

void *thread_produce (void *arg) {
  d = read_data (...) ;		// read data somewhere 
  add_queue (q, d) ;		// communicate the data to the processing thread
  csignal (&c) ;		// we added, we can wake it up 
}

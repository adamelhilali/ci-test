struct {
  struct proc proc [NPROC] ;	// process table
  int lock ;			// table lock
} tproc ;			// table used in concurrency

struct proc *look_for_free_proc (void) {
  locking (&tproc.lock) ;
  for (int i = 0 ; i < NPROC ; i++) {
    if (tproc.proc [i].state == FREE) {	// if two processors arrive at the same input simultaneously
      tproc.proc [i].state = CREATION ;	// they would allocate it twice \implique required critical section
      unlocking (&tproc.lock) ;
      return &tproc.proc [i] ;
    }
  }
  unlocking (&tproc.lock) ;
  return NULL ;
}

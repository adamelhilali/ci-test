condition c ;
mutex m ;
queue *q ;

void *thread (void *arg) {
  lock (&m) ;
  if (empty_queue(q)) {
     cwait (&c, &m) ;  // \alert{$\leftarrow$ here}
  }
  d = extract_queue (q) ;
  unlock (&m) ;
  process (d) ;
}

void *thread_produce (void *arg) {
  d = read_data (...) ;
  lock (&m) ;	// if need of critical section
  add_queue (q, d) ;
  csignal (&c) ;
  unlock (&m) ;
}

// do not do: everything is destructured 
int counter ;
pthread_mutex_t m ;

void *fct_thread (void *arg) {
    ...
    pthread_mutex_lock (&m) ;
    counter += y ;
    pthread_mutex_unlock (&m) ;
    ...
}

// better: the x counter is an object containing its own 
// lock, accesses via atomic functions (methods)

struct counter {
    int counter ;
    pthread_mutex_t m ;
} x ;

void add (struct counter *c, int y) {
    pthread_mutex_lock (&c->m) ;
    c->counter += y ;
    pthread_mutex_unlock (&c->m) ;
}

void *fct_thread (void *arg) {
    ...
    add (&x, y) ;
    ...
}

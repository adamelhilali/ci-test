locking:			// stack argument
	ld   [%sp+1],%a		// a $\leftarrow$ lock address
	push %b			// we will use b, save it first
loop:
	ld   [%a],%b		// b $\leftarrow$ *lock
        cmp  0,%b		// tests b value
	jne  loop		// loop if b $\neq$ 0
        ld   1,%b
	st   %b,[%a]		// *lock $\leftarrow$ 1
	pop  %b			// restore b
        rtn

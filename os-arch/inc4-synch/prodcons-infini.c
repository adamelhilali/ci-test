init () {
    ip = ic = 0 ;
    create_semaphore (Sip, 1) ;
    create_semaphore (Sic, 1) ;
    create_semaphore (Swritten, 0) ;
}
produce (val) {
    P (Sip) ;
    buffer [ip] = val ; ip = ip + 1 ;
    V (Sip) ;
    V (Swritten) ;
}
consume () {
    P (Swritten) ;
    P (Sic) ;
    val = buffer [ic] ; ic = ic + 1 ;
    V (Sic) ;
    return val ;
}

void locking (void) {
  sigset_t s ;

  sigemptyset (&s) ;
  sigaddset (&s, SIGUSR1) ;
  sigprocmask (SIG_BLOCK, &s, NULL) ;	// pthread\_sigmask for threads
}

void unlocking (void) {
  sigset_t s ;

  sigemptyset (&s) ;
  sigaddset (&s, SIGUSR1) ;
  sigprocmask (SIG_UNBLOCK, &s, NULL) ;
}

init () {
    ip = ic = 0 ;
    create_semaphore (Sip, 1) ;
    create_semaphore (Sic, 1) ;
    create_semaphore (Swritten, 0) ;
    create_semaphore (Sfree, N) ;
}
produce (val) {
    P (Sfree) ;
    P (Sip) ;
    buffer [ip] = val ; ip = (ip + 1) % N ;
    V (Sip) ;
    V (Swritten) ;
}
consume () {
    P (Swritten) ;
    P (Sic) ;
    val = buffer [ic] ; ic = (ic + 1) % N ;
    V (Sic) ;
    V (Sfree) ;
    return val ;
}

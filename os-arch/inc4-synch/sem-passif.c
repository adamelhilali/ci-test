P (S) {
    S.counter-- ;
    if (S.counter < 0) {
	add_fifo (S.list, current_proc) ;
	wait () ;
    }
}

V (S) {
    S.counter++ ;
    if (S.counter <= 0) {
	proc = extract_fifo (S.list) ;
	wake (proc) ;
    }
}

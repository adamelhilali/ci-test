#include <stdio.h>
#include <pthread.h>

pthread_cond_t  c ;		// POSIX condition
pthread_mutex_t m ;		// mutex associated with the condition
queue *q ;			// a queue

void *thread (void *arg) {
  pthread_mutex_lock (&m) ;	// start of critical section
  while (empty_queue(q))		// while : may wake up for other reasons
    pthread_cond_wait (&c, &m) ;// releases the critical section while waiting
  int d = extract_queue (q) ;
  pthread_mutex_unlock (&m) ;	// end of critical section
  process (d);
}

void *thread_produce (void *arg) {
  int d = read_data () ;
  pthread_mutex_lock (&m) ;	// start of critical section
  add_queue (q, d) ;
  pthread_cond_signal (&c) ;	// wakes up a thread if necessary
  pthread_mutex_unlock (&m) ;	// end of critical section
}

void *poireauteur (void *arg) {
    printf ("j'attends\n") ;
    attendre () ;
    printf ("je suis reveille\n") ;
}

void *reveilleur (void *arg) {
    printf ("je reveille\n") ;
    reveiller () ;
}

main () {
    pthread_t tp, tr ;
    pthread_mutex_init (&m, NULL) ;
    pthread_cond_init (&c, NULL) ;
    pthread_create (&tp, NULL, poireauteur, NULL) ;
    sleep (5) ;
    pthread_create (&tr, NULL, reveilleur, NULL) ;
    sleep (5) ;
}

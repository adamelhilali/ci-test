void locking (char *lock) {
  int fd ;

  do {
    fd = open (lock, O_CREAT | O_WRONLY | O_EXCL, 0666) ;
    if (fd == -1 && errno == EEXIST)
      sleep (1) ;        // slowed active waiting 
  } while (fd == -1 && errno == EEXIST) ;
  if (fd == -1)
    grumble (...) ;
  close (fd) ;
}

void unlocking (char *lock) {
  unlink (lock) ;
}

void process (void) {
    if (jobnumb == 0)
	cwait (&c) ;
    jobnumb-- ;
    process_job () ;
}

void add (void) {
    add_job () ;
    jobnumb++ ;
    csignal (&c) ;
}

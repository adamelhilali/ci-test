P (sem) {
    sem.counter-- ;		// resource consumption
    if (sem.counter < 0)	// no more avaible resource
	wait () ;		// put activity on hold  
}

V (sem) {
    sem.counter++ ;		// resource reslease
    if (sem.counter <= 0)
	wake () ;		// wake one of the waiting activities 
}

int count ;

void f (int signo) {
  count = count + 50 ;
}

main () {
  struct sigaction s ;
  ...
  s.sa_handler = f ;
  sigaction (SIGINT, &s, NULL) ;
  count = count + 10 ;
}

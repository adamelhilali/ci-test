init () {
    create_semaphore (Swriter, 1) ;
    create_semaphore (Smutex, 1) ;
    nread = 0 ;
}
writer () {
    P (Swriter) ;
    // writing  
    V (Swriter) ;
}
reader () {
    P (Smutex) ;
    nread++ ;
    if (nread == 1) {
	P (Swriter) ;
    }
    V (Smutex) ;
    // reading  
    P (Smutex) ;
    nread-- ;
    if (nread == 0) {
	V (Swriter) ;
    }
    V (Smutex) ;
}

// before 
if (x < 0)		// x is a variable shared by several threads
    y = x ;		// x may have been modified between test and assign 

// after 
pthread_mutex_lock (&lock) ;
if (x < 0)		// modifications to x must also...
    y = x ;		// ... be carried out with mutual exclusion 
pthread_mutex_unlock (&lock) ;

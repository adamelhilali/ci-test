// before 
count = count + 10 ;	// variable shared by several threads

// after 
pthread_mutex_lock (&lock) ;	// lock also needs to be shared 
count = count + 10 ;	// mutual exclusion access 
pthread_mutex_unlock (&lock) ;

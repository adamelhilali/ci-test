P (S) {
    spin_lock (S.lock1) ;
    while (S.counter <= 0)
	;		// active waiting
    spin_lock (S.lock2) ;
    S.counter-- ;
    spin_unlock (S.lock2) ;
    spin_unlock (S.lock1) ;
}

V (S) {
    spin_lock (S.lock2) ;
    S.counter++ ;
    spin_unlock (S.lock2) ;
}

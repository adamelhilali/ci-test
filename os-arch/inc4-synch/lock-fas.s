locking:			// stack argument 
	ld   [%sp+1],%a		// a $\leftarrow$ lock address
	push %b			// we will use b, save it first 
loop:
	fas  [%a],%b		// b $\leftarrow$ *lock, then *lock $\leftarrow$ 1
        cmp  0,%b		// tests b value 
	jne  boucle		// loop if b $\neq$ 0
	pop  %b			// restore b
        rtn

int fetch_and_set (int *address) {
  int old ;
  old = *address ;
  *address = 1 ;
  return old ;
}

int compare_and_swap (int *address, int ok, int val) {
  int old ;
  old = *address ;
  if (old == ok)
    *address = val ;
  return old ;
}

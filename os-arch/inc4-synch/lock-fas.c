void locking (int *lock) {
  while (fetch_and_set (lock) != 0)
    ;       // active waiting
}
void unlocking (int *lock) {
  *lock = 0 ;
}

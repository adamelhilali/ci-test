void sleep (void *wchan) {
  curproc->state = WAITING ;
  curproc->wchan = wchan ;
  swtch (...) ;			// switch over now!
  // we get here after another process has done wakeup(...)
}

void wakeup (void *wchan) {	// use a critical section if multiprocessor
  for (int i = 0 ; i < NPROC ; i++) {
    if (proc[i].state == WAITING && proc[i].wchan == wchan) {
      proc[i].state = READY_TO_RUN ;
      commute = 1 ;		// request switching back to user mode
    }
  }
}

void read_in_pipe (struct inode *ip) {
  while (empty_pipe (ip)) {	// ``while'' because on wake up, another process may already
    sleep (ip) ;		// read everything in the pipe
  }
  // write in the pipe
}

void write_in_pipe (struct inode *ip) {
  // write in the pipe (making sure it is not full !)
  wakeup (ip) ;			// we have written inside, so the pipe is no longer empty
}

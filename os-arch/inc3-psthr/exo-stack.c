// global variable
int *p ;

void thread1 (...) {
    // x is in thread1 stack
    int x = 5 ;
    p = &x ;
    sleep (5) ;
    printf ("%d\n", x) ;
}

void thread2 (...) {
    sleep (2) ;
    *p = 2 ;
}

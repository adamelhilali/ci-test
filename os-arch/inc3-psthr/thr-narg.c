struct thing {
  int a ; float b ; char *c ;	// example with 3 arguments
} ;

int main (...) {
  struct thing m = { ... } ;	// variable m declared in main thread 
  ...
  pthread_create (..., f, &m) ;	// no need of cast because the 4th arg is of type void *
}

void *f (void *arg) {
  struct thing *m = arg ;	// no need of cast because arg is of type void *
  ...
}

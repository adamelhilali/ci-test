int nticks ;				// global variable : number of elapsed clock ticks
int switch ;				// global variable : switch over (or not)

void process_int_horloge (void)		// called at each clock interrupt
{
  curproc->cpu_use++ ; 			// increase current process time 
  nticks++ ;
  if (ntops >= 50) {			// if 50 clock ticks, or 1 sec
    ntops = 0 ;

    for (int i = 0 ; i < nproc ; i++)	// divide by 2 all priorities
      proc[i].cpu_use /= 2 ;

    // minimum priority = my priority to get started
    int min_prio = curproc->cpu_use + curproc->nice ;
    switch = -1 ;			// no need to switch 

    for (int i = 0 ; i < nproc ; i++) {	// compute priorities for all processes 
      if (&proc[i] != curproc) {
	// computing the priority of process i
	int prio_i = proc[i].cpu_use + proc[i].nice ;

	// take minimum priority
	if (prio_i < min_prio) {
	  switch = i;			// switch to elected number i 
	  min_prio = prio_i ;		// on next interrupt/exception return 
	}
      }
    }
  }
}

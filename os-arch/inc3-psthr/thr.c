#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

#define	MAXSTR	1000

int nthread ;

void *f (void *arg)
{
    int *pi = arg ;		// \texttt{void*} is compatible with \texttt{int*}
    int *r ;
    
    printf ("I am the thread %d (on %d), tid =%ld\n",
    		*pi, nthread, pthread_self ()) ;

    r = malloc (sizeof (int)) ;
    *r = *pi * 2 ;

    pthread_exit (r) ;		// $\Leftrightarrow$ return r
}

void grumble (char *msg)
{
    char buf [MAXSTR] ;

    strerror_r (errno, buf, sizeof buf) ;
    fprintf (stderr, "%s: %s\n", msg, buf) ;
    exit (1) ;
}

int main (int argc, char *argv [])
{
    pthread_t *tid ;      int i, *ti ;

    nthread = atoi (argv [1]) ;
    tid = calloc (nthread, sizeof (pthread_t)) ;
    ti = calloc (nthread, sizeof (int)) ;
    for (i = 0 ; i < nthread ; i++) {
	ti [i] = i ;
	if ((errno=pthread_create(&tid[i], NULL, f, &ti[i])) > 0)
	    grumble ("pthread_create") ;
    }
    for (i = 0 ; i < nthread ; i++) {
	void *r ; int *pi ;

	if ((errno = pthread_join (tid [i], &r)) > 0)
	    grumble ("pthread_join") ;
	pi = r ;		// \texttt{void*} is compatible with \texttt{int*}
	printf ("Thread return %d = %d\n", i, *pi) ;
	free (r) ;
    }
    free (tid) ; free (ti) ;
    return 0 ;
}

pthread_attr_t pa ;

errno = pthread_attr_init (&pa) ;
if (errno > 0)
    grumble ("pthread_attr_init") ;

errno = pthread_attr_setstacksize (&pa, 2 * PAGE_SIZE) ;
if (errno > 0)
    grumble ("pthread_attr_setstacksize") ;

errno = pthread_create (..., &pa, ...) ;
if (errno > 0)
    grumble ("pthread_create") ;

pthread_attr_destroy (&pa) ;

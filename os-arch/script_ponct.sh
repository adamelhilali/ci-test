#!/bin/sh

fichiers=$(ls sl*.tex)

for fichier in $fichiers
do
    echo "Modification de $fichier"
    sed -i '
    s/[[:space:]]*\([:?!]\)/\1/g;
    s/[[:space:]]*~\([:?!]\)/\1/g;
    s/«[[:space:]]*/``/g;
    s/[[:space:]]*»/'\'''\''/g;
    s/``~/``/g;
    s/~'\'''\''/'\'''\''/g;
    s/\\frquote{\([^}]*\)}/``\1'\'''\''/g;
    ' "$fichier"

    echo "Modifications de $fichier effectuées"
done

echo "Tout les fichiers ont été modifiés"

narg=$#
if [ $narg != 2 ] && [ ! -f "$1" ]	# pas d'utilisation de \texttt{-a}
then
    echo "usage: $0 fichier" >&2	# redirection de 1 vers 2
    exit 1
fi

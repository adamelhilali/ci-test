for (int i = 1 ; i < N ; i++) {
  a[i] = f(a[i-1],b[i-1],c[i-1]);
}
for (int i = 1 ; i < N ; i++) {
  b[i] = g(a[i-1],b[i-1],c[i-1]);
}
for (int i = 1 ; i < N ; i++) {
  c[i] = h(a[i-1],b[i-1],c[i-1]);
}

pthread_barrier_t bar ;			// à initialiser (pthread\_barrier\_init)
for (int i = 1 ; i < N ; i++) {
  pthread_barrier_wait (&bar);		// attente des deux autres threads
  a[i] = f(a[i-1],b[i-1],c[i-1]);
}
for (int i = 1 ; i < N ; i++) {
  pthread_barrier_wait (&bar);		// attente des deux autres threads
  b[i] = g(a[i-1],b[i-1],c[i-1]);
}
for (int i = 1 ; i < N ; i++) {
  pthread_barrier_wait (&bar);		// attente des deux autres threads
  c[i] = h(a[i-1],b[i-1],c[i-1]);
}

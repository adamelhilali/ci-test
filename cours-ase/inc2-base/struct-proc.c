struct proc {
    unsigned int a ;			// sauvegarde de a
    unsigned int b ;			// sauvegarde de b
    unsigned int pc ;			// sauvegarde de pc
    unsigned int sr ;			// sauvegarde de sr
    unsigned int sp ;			// sauvegarde de sp
    unsigned int *kstack ;		// adresse du sommet de pile noyau
    ...
} ;

struct proc proc [NPROC] ;		// tableau de tous les descripteurs de processus

struct proc *curproc ;			// pointeur vers le processus courant

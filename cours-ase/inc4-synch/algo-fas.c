int fetch_and_set (int *adresse) {
  int vieux ;
  vieux = *adresse ;
  *adresse = 1 ;
  return vieux ;
}

int compare_and_swap (int *adresse, int ok, int val) {
  int vieux ;
  vieux = *adresse ;
  if (vieux == ok)
    *adresse = val ;
  return vieux ;
}

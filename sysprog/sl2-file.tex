\def\inc{inc2-file}

\titreA {Files}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Accès aux fichiers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Files access}

\begin {frame} {Files access}
    A file:
    \begin {itemize}
	\item has a name (actually, several...)
	\item is accessible via a path (absolute, relative)
	\item has attributes:
	    \begin {itemize}
		\item type
		\item owner, group, permissions
		\item size
		\item dates
		    \begin {itemize}
			\item of last data modification
			\item last attribute modification date
			\item last access date
		    \end {itemize}
		\item data location on disk
	    \end {itemize}
	\item 2 file types:
	    \begin {itemize}
		\item ``regular'' files
		\item directories
		\item ... in reality, there are others (later...)
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Files access}
    A file has a simple structure: a linear sequence of bytes
    \begin {center}
	\fig{str-fich}{.6}
    \end {center}

    \begin {itemize}
	\item Unix innovation
	    \begin {itemize}
		\item in earlier systems: files had
		    a type (text, database, etc.)
		\item no type \implique kernel simplification
	    \end {itemize}

	\item the structure depends on the application accessing the file

	    \begin {itemize}
		\item text: sequence of characters separated by
		    \code {\textbackslash n}
		    (byte 10)
		\item executable binary: contains a header that
		    describes the various parts (code, data,
		    debug info, etc.)
		\item LibreOffice document: see LibreOffice application
		\item etc.
	    \end {itemize}

	\item notion of ``\textit {magic number}''
	    \begin {itemize}
		\item sequence of bytes at the beginning of a file to identify it
		\item e.g.: \code {\#!} (script), \code {\%PDF} (PDF file),
		    \code {0xffd8} (JPEG), etc.
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Files access}
    \begin{termblock}{Command \code{file}}
	The \code{file} command uses various heuristics to
	``guess'' file usage:
	\begin{itemize}
	    \item file type: regular file, directory,
		etc. (see below)
	    \item \textit{magic number}
	    \item character set recognition (ISO, UTF-8, etc.)

	\end{itemize}
	\lst{file.sh}{\fD}{language=sh}
    \end{termblock}
\end {frame}

\begin {frame} {Files access}

    \begin {itemize}
	\item filename: no meaning for the kernel
	    \begin {itemize}
		\item I can name my executable \code {foo.bar.baz}
		    if I feel like it

		\item I can name a text file \code {foo.xls}
		    if I feel like it

		\item some applications require a suffix
		    \begin {itemize}
			\item e.g.: the ``C compiler'' application
			    assumes that C sources end in ``\code {.c}''
			\item this is not the general case
		    \end {itemize}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {File opening}

    \prototype {
	\code {int open (const char *path, int flags)} \\
	\code {int open (const char *path, int flags, mode\_t mode)}
    }

    \begin {itemize}
	\item 2 prototypes for this primitive (exception that confirms...)
	    \begin {itemize}
		\item first form: ``simple'' opening
		\item second form: open with \alert{creation}
		of the file
		    \\
		    \implique \code {mode} is the initial file permission
		    (with application of the creation mask, see
		    later)
	    \end {itemize}

	\item result: opening descriptor (or -1)

	    \begin {itemize}
		\item used by other primitives
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {File opening}
    \begin {itemize}
	\item \code {flags}: opening mode
	    \begin {center}
		\vspace* {-2ex}
		\fig{flags-open}{.5}

		{\fD Purely imaginary bits position...}
	    \end {center}

	\item Examples:

	    \begin {itemize}
		\item \code {open ("foo", O\_RDONLY)}

		    Open read-only (file must exist)

		\item \code {open ("foo", O\_WRONLY | O\_CREAT | O\_TRUNC, 0666)}

		    Create a new file (or reset an existing file to 0)
		    and open in write-only mode

		\item \code {open ("foo", O\_RDWR | O\_CREAT | O\_APPEND, 0666)}

		    Create a new file (if it did not already
		    existed) and open in read/write mode with
		    add at end

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {File opening}
    \begin {itemize}
	\item What permissions to set when creating?
	    \begin {itemize}
		\item basic rule: \code {mode = 0666}

		    \begin {itemize}
			\item if no particular security concerns
			\item if not executable (unless you wrote
			    a link editor)
		    \end {itemize}
		\item let the file creation mask do the work
		    (see later)
	    \end {itemize}

	\item Warning: if \code {mode} not supplied, \code {open} will
	    take what is on the stack at the expected location \\
	    \implique probably anything

    \end {itemize}
\end {frame}

\begin {frame} {File opening}
    Three default openings:
    \ctableau {\fC} {|lll|} {
	\code{0} & \code{STDIN\_FILENO} & standard input\\
	\code{1} & \code{STDOUT\_FILENO} & standard output \\
	\code{2} & \code{STDERR\_FILENO} & standard error output \\
    }

    \medskip
    These descriptors are opened by the Shell
    \begin {itemize}
	\item By default: the terminal
	\item Redirection from/to a file
	    \\
	    \implique never assume that standard input is necessarily
	    the keyboard (or the screen for the standard output)
	\item The POSIX constants (\code{STDIN\_FILENO}...) are
	    not widespread
	    \\
	    POSIX defines numerical values \implique
	    practical to use
    \end {itemize}
\end {frame}

\begin {frame} {File closing}
    Remember to close files after use
    \prototype {
	\code {int close (int fd)}
    }

    \begin {itemize}
	\item automatic closing at process end
	\item best practice: close as soon as possible
	\item later (pipes): to close as soon as possible is \textbf
	    {crucial}...
	\item you might as well get used to doing it now!

    \end {itemize}
\end {frame}

\begin {frame} {Files access}
    Once the file is open, one read and write:

    \prototype {
	\code {ssize\_t read (int fd, void *buf, size\_t cnt)} \\
	\code {ssize\_t write (int fd, const void *buf, size\_t cnt)}
    }

    \begin {itemize}
	\item returns the number of bytes transferred (or -1)
	    \begin {itemize}
		\item 0 at end of file
	    \end {itemize}
	\item \code {fd}: opening descriptor (returned by \code {open})
	\item \code {buf}: where the kernel writes (for \code {read})
	    or reads (for \code {write}) the data to be transferred
	\item \code {cnt}: number of bytes to be transferred
	    \begin {itemize}
		\item number of bytes transferred: not necessarily the number requested
		\item example: \code {read (fd, buf, 500)} whereas
		the file is only 10 bytes long
		    \implique return = 10
		\item example: \code {write (fd, buf, 500)} whereas
		    the disk is 10 bytes from saturation
		    \implique return = 10
	    \end {itemize}
    \end {itemize}

\end {frame}

\begin {frame} {Files access}
    Random access:
    \begin {center}
	\fB
	\code {off\_t lseek (int fd, off\_t offset, int whence)}
    \end {center}

    \begin {itemize}
	\item Each file opening has an \textit {offset} \\
	    \begin {itemize}
		\item current position in file ($\geq$ 0)
		\item advanced automatically with \code {read} and
		    \code {write}

	    \end {itemize}

	\item \code {lseek} allows you to modify the offset according to \code {whence}

	    \ctableau {\fC} {|ll|} {
		\code {SEEK\_SET}
		    & move to absolute position \\
		\code {SEEK\_CUR}
		    & advance from current position \\
		\code {SEEK\_END}
		    & advance from end of file \\
	    }

	\item return code for \code {lseek}: offset after modification

    \end {itemize}
\end {frame}

\begin {frame} {Files access}
    Examples:
    \begin {itemize}
	\item \code {lseek (fd, 1000000, SEEK\_SET)}

	    Move to offset = 1 million bytes

	\item \code {lseek (fd, -20, SEEK\_CUR)}

	    Go back 20 bytes before current position

	\item \code {lseek (fd, 300000, SEEK\_END)}

	    Move 300$\thinspace$000 bytes after the current
		end of the file
	    \begin {itemize}
		\item \code {read} will return 0
		\item \code {write} will be able to write new bytes.
		    In that case:
		    \begin {itemize}
			\item the system leaves a “hole” in the
			    file
			\item in case of reading in the “hole”, everything
			    happens as if we had written
			    300$\thinspace$000 times byte 0
			\item the size of the file is not the space
			    occupied on the disk!

		    \end {itemize}
	    \end {itemize}

	\item \code {lseek (fd, 0, SEEK\_CUR)}

	    Where am I?

    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Primitives système et fonctions de bibliothèque
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {System primitives and library functions}

\begin {frame} {Primitives and library functions}
    Two sets of file access functions?

    \begin {itemize}
	\item system primitive: \code {open}, \code {close},
	    \code {read}, \code {write}, \code {lseek}

	\item library functions: \code {fopen}, \code {fclose},
	    \code {getc}, \code {scanf}, \code {fread}, \code {putc},
	    \code {printf}, \code {fwrite}, \code {fseek}, etc.

    \end {itemize}

    \medskip

    Functionality duplication?

    \medskip

    Why?
\end {frame}

\begin {frame} {Example with system primitives}
    \lst{lib-open.c}{\fD}{}
\end {frame}

\begin {frame} {Example with library functions}
    \lst{lib-fopen.c}{\fD}{}
\end {frame}

\begin {frame} {Primitives and library functions}
    Difference game

    \ctableau {\fD} {|p{.45\linewidth}p{.45\linewidth}|} {
	\multicolumn {1} {|c} {\textbf {system primitives}}
	    & \multicolumn {1} {c|} {\textbf {libraries functions}}
	    \\ \hline
	descriptor = \code {int} & descriptor = \code {FILE *}
	    \\
	less simple \code {read}/\code {write} parameters
	    & simple \code {getc}/\code {putc} use
	    \\
	only \code {read} or \code {write} &
	    possibility of using other functions
	    (\code {printf}, \code {puts}, etc.)
	    \\
	purpose = to secure data
	    & purpose = programming aid
	    \\
	lower-level interface
	    & library functions use
		system primitives (\implique high-level)
	    \\
	code in the kernel
	    & code added to program when editing links
	    \\
    }
\end {frame}

\begin {frame} {Efficiency}
    Which one are the most efficient?

    \begin {itemize}
	\item naive approach: library functions calling
	    ``equivalent'' system primitives, they are more
	    slower
	\item the answer is more complex...
    \end {itemize}
\end {frame}

\begin {frame} {Efficiency}

    Reminder: running a system primitive (example \code {read})

    \begin {itemize}
	\item special instruction (depending on the
	    processor: TRAP, SVC, INT, etc.)
	\item causes (among other things):
	    \begin {itemize}
		\item switching to privileged mode
		\item rerouting the program to a specific address
	    \end {itemize}
	\item checks:
	    \begin {itemize}
		\item is the opening descriptor open?
		\item is the buffer address valid?
		\item is the end-of-buffer address valid??
		\item is the number to be transferred valid?
	    \end {itemize}
	\item perform the action (input/output)
	\item copy data into process memory space
	\item return to the instruction following \code {read} 
    \end {itemize}
    Result: a lot of checks, \textbf {especially for a single byte}
\end {frame}

\begin {frame} {Bufferization}

    The library's input/output functions use
    ``\textbf {bufferization}''

    \begin {minipage} [c] {.4\linewidth}
	\medskip
	\fig{bufferisation}{}
    \end {minipage}%
    \begin {minipage} [c] {.6\linewidth}
	\begin {itemize}
	    \item buffer = array in memory
	    \item first call to \code {getc}: fill buffer
		\\
		\implique call for \code {read} \implique check
	    \item after: simple function call + read from memory
		\\
		\implique very efficient
	    \item size $n$ buffer
		\implique call \code {read} once on $n$

	    \item in practice, $n = 4096$ (for example)

	\end {itemize}
    \end {minipage}
\end {frame}

\begin {frame} {Bufferization}

    Summary:
    \begin {itemize}
	\item if only a few bytes are read, the library's input/output
	    functions are more efficient than system primitives

	    \begin {itemize}
		\item less checks
		\item more simple function calls
	    \end {itemize}

	\item if many bytes at a time are read, system primitives
	    are more efficient than library functions

	    \begin {itemize}
		\item no overhead due to overlay
		\item no time spent on superfluous buffering
	    \end {itemize}
    \end {itemize}

    \medskip

    From what read size are the system primitives
    less efficient than \code {getc}? \implique exercise

\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Attributs des fichiers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {File attributes}

\begin {frame} {File attributes}
    Each file is associated with attributes:

    \begin {itemize}
	\item type
	\item owner and group
	\item permissions
	\item size
	\item dates
	    \begin {itemize}
		\item of last data modification
		\item of last attribute modification
		\item of last access
	    \end {itemize}
	\item number of links (see later)
	\item device number, file number
	\item data location on disk
    \end {itemize}
    \medskip
    Name is not a file attribute (see later)
\end {frame}

\begin {frame} {Dates}
    \begin{termblock}{File dates}
	The \code{ls} command, with the \code{-l} option, displays the
	date of last data modification

	\medskip

	Among the (too) many \code{ls} options, there are
	\code{-c} and \code{-u}:

	\lst{ls-date.sh}{\fC}{language=sh}
    \end{termblock}
\end {frame}

\begin {frame} {Permissions}
    Permissions: 12 bits

    \begin {center}
	\fig{perm}{.6}
    \end {center}

    \begin {itemize}
	\item Usual 9 bits (3 bits for owner, group, other)
	\item ``sticky'' bit: for directories, prohibits the deletion
	    except for the owner of the file or directory
	    (useful for \code {/tmp} for example)
	\item ``set-user-id-on-exec'' and ``set-group-id-on-exec'' bits \\
	    \implique later (chapter on process management)
    \end {itemize}
\end {frame}

\begin {frame} {Viewing attributes}
    Retrieve all attributes in a single operation:

    \prototype {
	\code {int stat (const char *path, struct stat *stbuf)} \\
	\code {int fstat (int fd, struct stat *stbuf)}
    }

    \begin {itemize}
	\item the \code {stat} structure contains, in return, all the
	    attributes, including:

	    \ctableau {\fD} {|ll|} {
		\code {st\_mode}
		    & type and permissions \\
		\code {st\_uid}
		    & owner \\
		\code {st\_gid}
		    & group \\
		\code {st\_size}
		    & bytes size \\
		\code {st\_atime}
		    & date of last access \\
		\code {st\_mtime}
		    & date of last data modification \\
		\code {st\_ctime}
		    & date of last attributes modification \\
	    }
	\item this structure may hold other attributes
	\item ... but not all (data localization on disk
	    unnecessary outside the kernel \implique not returned)

    \end {itemize}
\end {frame}

\begin {frame} {Viewing attributes}
    \code {st\_mode}: type and permissions in the same field?

    \begin {center}
	\fig{st-mode}{.6}
    \end {center}

    \begin {itemize}
	\item 12 permissions bits (e.g.: \code {00752} = \code {rwxr-x-w-})
	    \begin {itemize}
		\item POSIX defines constants: \\
		    \code {S\_IRWXU | S\_IRGRP | S\_IXGRP | S\_IWOTH}
		\item ... but POSIX also defines numerical values
		    \\
		    (exceptional $\Leftarrow$ they are more convenient to use)
	    \end {itemize}
	\item 4 bits: type

	    \begin {minipage} [c] {.4\linewidth}
		\ctableau {\fD} {|ll|} {
		    1 & directory \\
		    2 & regular file \\
		    ... & ... \\
		}
	    \end {minipage}%
	    \begin {minipage} [c] {.6\linewidth}
		Example
		(valeurs imaginaires)
	    \end {minipage}
    \end {itemize}
\end {frame}

\begin {frame} {Viewing attributes}
    How to use \code {st\_mode}?

    \begin {itemize}
	\item Recover permissions: \code {stbuf.st\_mode \& 0777}
	\item Recover type: \code {stbuf.st\_mode \& 0xf000}
	    \begin {itemize}
		\item To test...
		    {
		    \fC
		    \begin {tabular} {ll}
			... if directory
			    & \code {if ((stbuf.st\_mode \& 0xf000) == 0x1000)}
			    \\
			... if file
			    & \code {if ((stbuf.st\_mode \& 0xf000) == 0x2000)}
			    \\
		    \end {tabular}
		    }
		\item Using POSIX constants
		    {
		    \fC
		    \begin {tabular} {ll}
			... if directory
			    & \code {if ((stbuf.st\_mode \& S\_IFMT) == S\_IFDIR)}
			    \\
			... if file
			    & \code {if ((stbuf.st\_mode \& S\_IFMT) == S\_IFREG)}
			    \\
		    \end {tabular}
		    }
		\item Even better... \\
		    {
		    \fC
		    \begin {tabular} {ll}
			... if directory
			    & \code {if (S\_ISDIR (stbuf.st\_mode))}
			    \\
			... if file
			    & \code {if (S\_ISREG (stbuf.st\_mode))}
			    \\
		    \end {tabular}
		    }
	    \end {itemize}
	    \medskip
	    Warning: do not use numerical values \\
	    \implique there is every chance that they will not be correct
		on your system
    \end {itemize}
\end {frame}

\begin {frame} {Viewing attributes}
    How to answer ``can I read/write/execute the file''?

    \begin {itemize}
	\item \code {stat} does not answer the question
	    \begin {itemize}
		\item testing permissions is not enough
		\item first of all, we need to know if we are the owner,
		    a member of the group, or ``other''
	    \end {itemize}

	\item solution:

	    \prototype {\code {int access (const char *path, int mode)}}

	\item \code {mode} parameter:

	    \ctableau {\fC} {|ll|} {
		\code {F\_OK} & tests if the file exists \\
		\code {X\_OK} & tests access at runtime \\
		\code {W\_OK} & tests write access \\
		\code {R\_OK} & tests read access \\
	    }

	\item authorized access: return = 0, prohibited: return = -1

    \end {itemize}
\end {frame}

\begin {frame} {Attributes modification}
    To modify some attributes:
    \begin {itemize}
	\item modify permissions
	    \prototype {
		\code {int chmod (const char *path, mode\_t mode)} \\
		\code {int fchmod (int fd, mode\_t mode)}
	    }

	\item change owner or group
	    \prototype {
		\code {int chown (const char *path, uid\_t uid, git\_t gid)} \\
		\code {int fchown (int fd, uid\_t uid, git\_t gid)} \\
	    }
    \end {itemize}
\end {frame}

\begin {frame} {Attributes modification}
    \begin {itemize}
	\item modify dates:
	    \prototype {
		\code {int utime (const char *path, const struct utimbuf *buf)}
	    }

	    \begin {itemize}
		\item \code {struct utimbuf} fields:

		    \ctableau {\fD} {|ll|} {
			\code {actime} & last acess \\
			\code {modtime} & last modification \\
		    }

		\item no third date (attributes
		    modification)
		    \\
		    \implique modified by \code {utime} itself
	    \end {itemize}
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Répertoires
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Directories}

\begin {frame} {What is a directory?}

    A directory is a file on the disk
    \begin {itemize}
	\item distinct type: not a regular file

	    \begin {itemize}
		\item some operations are prohibited
		    \\
		    Example: \code {\alert {open ("directory", O\_WRONLY)}}

		    \smallskip

		\item \implique use specialized primitives

	    \end {itemize}

	\item special structure

	    \begin {itemize}
		\item it is always a linear sequence of bytes
		\item but the kernel puts its own structure

	    \end {itemize}

	\item content: references to files (regular
	    or directories or ...)

    \end {itemize}
\end {frame}

\begin {frame} {What is a directory?}

    Original Unix directory structure (V7, 1977):

    \begin {center}
	\fig{rep-fmt-v7}{.6}
    \end {center}

    \begin {itemize}
	\item name: limited to 14 characters
	\item inode number: file number on disk
	\item 2 special inputs: ``.'' and ``..''
    \end {itemize}
\end {frame}

\begin {frame} {Inode number}
    What is a file number?

    \begin {itemize}
	\item each file (regular, directory) has an \textbf {inode}
	\item the inode gathers a file's attributes \\
	    ($\approx$ what \code {stat} returns + data location)
	\item inodes arranged sequentially on a portion of the
	    of the disk \\
	    \implique an inode is therefore identified by its number
	    \begin {itemize}
		\item convention: root directory inode = 2
	    \end {itemize}
	\item an inode number identifies a file \\
	    \implique attributes and content

    \end {itemize}
\end {frame}

\begin {frame} {What is a directory}
    \begin {center}
	\fig{arbo}{.9}
    \end {center}

    \begin {itemize}
	\item logical vision: tree structure
	\item physical view: series of inodes on disk \\
	    \implique directories and files
	\item the name is not part of the file attributes \\
	    \implique a name is just one entry in a directory
	\item \code {ls -i}: displays entries (name + inode number)
    \end {itemize}
\end {frame}

\begin {frame} {What is a directory}
    \begin{termblock}{Tree structure}
	The \code{ls} command, with the \code{-i} option, lets you
	visualize the tree structure:

	\lst{ls-inode.sh}{\fD}{language=sh}
    \end{termblock}
\end {frame}

\begin {frame} {What is a directory}
    Main operations
    \begin {itemize}
	\item \code {open ("/usr/include/stdio.h", ...)} \\
	    \begin {itemize}
		\item directory search
		\item ``directory traversal''
		\item requires the \textbf {execution} permissions on
		    directories
		\item absolute path: start at inode 2
		\item relative path: start at inode of current directory

	    \end {itemize}
	\item delete an entry
	    \begin {itemize}
		\item empty entry: inode number $\leftarrow$ 0
		\item the rest of the directory is not shifted (performance)
	    \end {itemize}
	\item add an entry
	    \begin {itemize}
		\item search for an available entry
		\item or enlarge the directory if necessary
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {What is a directory}
    1982: U. Berkeley releases BSD 4.2

    \begin {itemize}
	\item file system enhancement: performance,
	    features
	\item increase maximum name size ($\leq$ 255)
    \end {itemize}

    \begin {center}
	\fig{rep-fmt-ffs}{.8}
    \end {center}
\end {frame}

\begin {frame} {Reading a directory}
    How to read the contents of a directory?

    \begin {itemize}
	\item originally: \code {open ("rep", O\_RDONLY)} \\
	    \begin {itemize}
		\item read 2 bytes for the inode and 14 for the name, etc.
		\item ignore null inode numbers
		\item repeat until the end
	    \end {itemize}

	\item introduction of the BSD file system
	    \begin {itemize}
		\item change all programs (e.g.: \code {ls})
	    \end {itemize}

	\item new BSD system primitive: \code {\alert {getdirentries}}
	    \begin {itemize}
		\item not standardized by POSIX \implique do not use
	    \end {itemize}
	
	\item BSD offers new library functions:
	    \begin {itemize}
		\item \code {opendir}, \code {readdir}, \code {closedir}
		\item standardized by POSIX \implique \textbf {to use}!
	    \end {itemize}

	\item new file systems
	    \begin {itemize}
		\item local: lfs, unionfs, zfs, ext$n$fs, etc.
		\item network: nfs, smbfs, etc.
	    \end {itemize}
	    \implique \code {opendir} \& co: still usable
    \end {itemize}
\end {frame}

\begin {frame} {Reading a directory}
    \prototype {
	\code {DIR *opendir (const char *path)} \\
	\code {struct dirent *readdir (DIR *dp)} \\
	\code {int closedir (DIR *dp)}
    }

    \begin {itemize}
	\item fields of the \code {struct dirent} standardized by POSIX:

	    \ctableau {\fC} {|ll|} {
		\code {d\_ino} & inode number \\
		\code {d\_name} & name, terminated by a null byte \\
	    }

	\item warning: there may be other fields, but they are not
	    are not standardized by POSIX (e.g.: \alert{\texttt{d\_type}}) \\
	    \implique do not use!

	\item \code {readdir} successively returns all inputs
	    \begin {itemize}
		\item including ``.'' and ``..''
		\item returned address = local variable \code {static}
		    from \code {readdir}
		    \\
		    \implique no need to release it
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Reading a directory}
    Example:

    \lst{ex-dir.c}{\fD}{}
    
\end {frame}

\begin {frame} {Reading a directory}
    \code{readdir}~ design problem: to distinguish
    an error from the end of a directory (\code{NULL} in both cases),
    set \code{errno} to 0 before each call, then test
    its value after the call:

    \lst{ex-dir-err.c}{\fE}{}

    Note: this example uses the C "\code{,}" operator \\
    \fC
    (the expression \code{exp1,exp2} calculates \code{exp1} then \code{exp2},
    the result is \code{exp2})

\end {frame}

\begin {frame} {Creating and deleting directories}
    \prototype {
	\code {int mkdir (const char *path, mode\_t mode)} \\
	\code {int rmdir (const char *path)}
    }

    \begin {itemize}
	\item \code {mkdir}
	    \begin {itemize}
		\item automatically creates the two entries ``.'' and ``..''
		\item \code {mode} = directory permissions
		    \begin {itemize}
			\item right of execution: traverse directory
			\item basic rule: \code {mode = 0777}
			    (unless specific conditions apply)
			\item let the file creation mask do the work
			    (see later)
		    \end {itemize}
	    \end {itemize}

	\item \code {rmdir} can only delete empty directories
	    \begin {itemize}
		\item with the exception of ``.'' and ``..'' (cannot
		    be removed)
	    \end {itemize}

	\item \code {mkdir} and \code {rmdir} commands: simple
	    calls to system primitives

    \end {itemize}
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Liens
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Links}

\begin {frame} {Physical links}
    In shell:

    \begin {itemize}
	\item Shell command \code {ln}: \code {ln foo bar}
	\item \code {int link (const char *old, const char *new)}
	\item \implique adds a second name \code {bar} to the file foo
    \end {itemize}

    \medskip

    How does it work? Reminder:

    \begin {center}
	\fig{arbo}{.8}
    \end {center}

    In reality, the logical vision is skewed...

\end {frame}

\begin {frame} {Physical links}
    The tree is a directed graph G = <S, E> where:
    \begin {itemize}
	\item vectrices $\in$ S: inodes (regular directories or files)
	\item labeled edges $\in$ E: directory entries = names
	    \begin {center}
		\fig{lien-1}{.8}
	    \end {center}
	\item if we exclude ``.'' and ``..'': cycle-free graph
	\item nl: number of links (\code {st\_nlink} with \code {stat})

    \end {itemize}
\end {frame}

\begin {frame} {Physical links -- Adding}
    \begin {itemize}
	\item add an edge to the graph
	\item \code {link ("/usr/include/unistd.h", "/home/foo")}

	    \begin {center}
		\fig{lien-2}{.5}
	    \end {center}

	\item adding a name increases the number of links
	\item the new name has exactly the same status as the old one
	    \begin {itemize}
		\item there is no ``main'' name, both are equivalent
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Physical links -- Removal}
    \begin {itemize}
	\item delete an edge in the graph
	\item \code {int unlink (const char *path)}
	\item \implique decreases the number of links
	\item if the number of links = 0 \\
	    \implique the space occupied by the file is freed on disk
	\item \code {unlink} is the primitive for deleting a file
	    \begin {itemize}
		\item does not work for directories
		    (reminder: \code {rmdir})
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Physical links}
    Limitations on physical links:
    \begin {itemize}
	\item the new name must be on the same disk
	    \begin {itemize}
		\item a directory entry contains only one
		    an inode number \\
		    \implique the inode is searched on the same disk
	    \end {itemize}
	\item one cannot create a link to a directory
	    \begin {itemize}
		\item otherwise, cycles could be created in the graph
		\item difficult to make a recursive path \\
		    (find, recopy tree structure, tar, etc.)
	    \end {itemize}
	\item the old name must exist
	    \begin {itemize}
		\item no link to a file that does not yet exist
		\item example: a shortcut to a file on an unmounted
		    file system not yet mounted
		    (assembly on demand)

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Symbolic links}
    1982: U. Berkeley releases BSD 4.2

    \begin {itemize}
	\item symbolic links
	\item e.g.: \code {ln \alert {-s} /tmp /home/pda/temp}
	\item \code {int symlink (const char *old, const char *new)}
	\item new file type (in addition to regular and directory)
	    \begin {itemize}
		\item with \code {stat}: \code {S\_IFLNK} and
		    \code {S\_ISLNK()}
	    \end {itemize}
	\item a symbolic link contains the name of the referenced object
	    \begin {itemize}
		\item file, directory, symbolic link, or other
		\item may not even exist
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Symbolic links -- Implementation}
    \begin {itemize}
	\item a symbolic link contains the name of the referenced object
	\item new file type
	    \begin {center}
		\fig{lien-3}{.7}
	    \end {center}

	\item when a symbolic link is found:
	    \begin {itemize}
		\item if absolute name: start from the root
		\item if relative name: continue from the directory
		    where the link is
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Symbolic links -- Reading a link}
    \prototype {\code {ssize\_t readlink (const char *path, char *buf, size\_t size)}}

    \begin {itemize}
	\item \code {path} is a ``symbolic link'' type file
	\item the content of the link is placed in \code {buf}
	\item at most \code {size} bytes placed in \code {buf}
	    \begin {itemize}
		\item possible truncation
	    \end {itemize}
	\item returns the number of bytes placed in \code {buf} (or -1)
    \end {itemize}
\end {frame}

\begin {frame} {Symbolic links -- Back to stat}
    How does \code {stat} behave with symbolic links?

    \begin {itemize}
	\item on a symbolic link, \code {stat} reads the attributes of the
	    file pointed to

	    \begin {itemize}
		\item -1 if the link refers to a non-existent object
	    \end {itemize}

	\item to find out if the file is a symbolic link:

	    \code {int lstat (const char *path, struct stat *stbuf)}

	    \begin {itemize}
		\item \code {lstat} reads attributes, even
		    even if it is a symbolic link

		\item example: \code {tar} archives links, not
		    pointed files

	    \end {itemize}

    \end {itemize}
\end {frame}

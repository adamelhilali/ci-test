#define	CHK(op)	  do { if ((op)==-1) grumble(#op); } while (0)

void grumble (const char *msg)
{
    perror (msg) ;
    exit (1) ;
}

...

CHK (access ("bar", R_OK)) ;
...
CHK (fd = open ("foo", O_RDONLY)) ;
...

void grumble (const char *msg)	// easy-to-use function !
{
    perror (msg) ;
    exit (1) ;
}

...

fd = open ("foo", O_RDONLY) ;
if (fd == -1)
    grumble ("open foo") ;

...

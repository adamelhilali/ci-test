void f (int sig) {	 // function called when SIGINT is received 
    ...
}
int main (...) {
  struct sigaction s ;

  s.sa_handler = f ;		// address of the function to call 
  s.sa_flags = 0 ;		 // "classic" use 
  sigemptyset (&s.sa_mask) ;	 // cf. see below 
  sigaction (SIGINT, &s, NULL) ; // action definition
  ...
  // computing (which can be temporarily interrupted by \texttt{f})
}

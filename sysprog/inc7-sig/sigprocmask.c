void increment (void) {
    sigset_t new, old ;

    // masking SIGINT to prevent it from distrupting the increment 
    sigemptyset (&new) ;
    sigaddset (&new, SIGINT) ;		    // new $\leftarrow$ \{ SIGINT \}
    sigprocmask (SIG_BLOCK, &new, &old) ;   // start of critical section

    if (c.big == UINT32_MAX) {
	c.big = 0 ;
	c.verybig++ ;
    }
    else c.big++ ;

    // unmask SIGINT by resetting the previous signal mask 
    sigprocmask (SIG_SETMASK, &old, NULL) ; // end of critical section
}

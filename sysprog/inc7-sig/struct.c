struct sigaction {
  void    (*sa_handler) (int) ; // function address
  sigset_t  sa_mask ;           // set of signals to be masked 
  int       sa_flags ;          // to modify behavior 
  ...                           // oher fields exist
} ;

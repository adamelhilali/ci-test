void receive_sigint (int sig) {	// SIGHUP is automatically masked
    add_in_data_struct () ;	// must not be interrupted by \code{remove\_from\_data\_struct}
}					// SIGHUP is automatically unmasked

void receive_sighup (int sig) {	// SIGINT is automatically masked
    remove_from_data_struct () ;	// must not be interrupted \code{add\_in\_data\_struct}
}					// SIGINT is automatically unmasked

int main (...) {
    struct sigaction s1, s2 ;

    s1.sa_handler = receive_sigint ;
    s1.sa_flags = 0 ;
    sigemptyset (&s1.sa_mask) ;
    sigaddset (&s1.sa_mask, SIGHUP) ;	// mask SIGHUP during function execution
    sigaction (SIGINT, &s1, NULL) ;

    s2.sa_handler = receive_sighup ;
    s2.sa_flags = 0 ;
    s2gemptyset (&s2.sa_mask) ;
    s2gaddset (&s2.sa_mask, SIGINT) ;	// mask SIGINT during function execution
    s2gaction (SIGHUP, &s2, NULL) ;

    ...
}

sigset_t e ;		  // declare e as a set of signals
sigemptyset (&e) ;	  // $e \leftarrow \emptyset$ 
sigaddset (&e, SIGINT) ;  // $e \leftarrow e \cup SIGINT$
sigaddset (&e, SIGHUP) ;  // $e \leftarrow e \cup SIGHUP$ \implique e = \{SIGINT, SIGHUP\}

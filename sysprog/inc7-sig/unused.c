void f (int sig)
{
    (void) sig ;      // hack : expression whose result is ignored 
    // rest of the function that doesn't use sig
}

#include <stdio.h>
#include <signal.h>

volatile sig_atomic_t stop = 0 ;

void f (int sig) {
    stop = 1 ; // sig\_atomic\_t $\Rightarrow$ assignment in "one" operation
}

int main (...) {
    struct sigaction s ;

    s.sa_handler = f ;
    ...
    sigaction (SIGINT, &s, NULL) ;

    while (! stop) // volatile $\Rightarrow$ variable read again at each iteration
	compute () ;

    // if we arrive here, the signal has been taken into account 
}

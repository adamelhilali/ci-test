volatile sig_atomic_t received_signal = 0 ;

void f (int sig) {
  received_signal = 1 ;
}

int main (...) {
  struct sigaction s ;

  s.sa_handler = f ;
  sigaction (SIGINT, &s, NULL) ;
  ...
  while (! received_signal)
    ;			// empty loop waiting for the signal 
  // do what needs to be done when the signal is heeded 
}

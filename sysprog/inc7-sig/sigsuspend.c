sigset_t new, old, empty ;

sigemptyset (&new) ;
sigaddset (&new, SIGINT) ;
sigprocmask (SIG_BLOCK, &new, &old) ;	// critical section : mask SIGINT

if (! received_signal) {
  sigemptyset (&empty) ;
  sigsuspend (&empty) ; 	 	  	// critical section while waiting 
}

sigprocmask (SIG_SETMASK, &old, NULL) ;	// end of critical section : unmask SIGINT

// do what needs to be done when the signal is heeded 

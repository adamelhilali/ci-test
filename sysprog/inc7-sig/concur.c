// simply chained list header insertion 
new->next = head ;
head = new ;

// reading then modifying a file 
read (fd, &val, sizeof val) ;
val = val + ... ;
lseek (fd, SEEK_SET, 0) ;
write (fd, &val, sizeof val) ;

// decision making then modification
if (x == 25) {
    x++ ;
}

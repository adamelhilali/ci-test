// a large counter to count beyond $2^{32}$
struct large_counter {
    uint32_t verybig ;
    uint32_t big ;
} c = {0, 0} ;

void increment (void) {
    if (c.big == UINT32_MAX) {
	c.big = 0 ;
	c.verybig++ ;
    }
    else c.big++ ;
}
void f (int sig) {	// asynchronous part
    increment () ;
}

int main (...) {	// synchronous part
    struct sigaction s ;
    s.sa_handler = f ;
    ...
    sigaction (SIGINT, &s, NULL) ;
    ...
    while (...) {
	increment (); // computing
    }
}

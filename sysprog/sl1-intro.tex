\def\inc{inc1-intro}

\titreA {Introduction}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Organisation de l'UE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Organization of this course}

\begin {frame} {Organization of this course}
    Central theme of this course: how to use an operating system?

    \begin {itemize}
	\item at the lowest level:
	    \begin {itemize}
		\item using system primitives with the
		    C language
		    \\
		    \implique good level in C


		\item POSIX primitives
		    \\
		    \implique international standard
	    \end {itemize}

	\item understand the concepts handled by the system

	    \begin {itemize}
		\item idea: understand the scope of the operating
		    and the concepts manipulated by
		    using them

		\item processes, users, files, etc.
	    \end {itemize}

    \end {itemize}

\end {frame}

\begin {frame} {Organization of this course}
    Course supplemented by practical work

    \begin {itemize}
	\item using system primitives
	\item pratice, pratice, pratice...
	\item familiarize yourself with the concepts
    \end {itemize}
\end {frame}

\begin {frame} {Bibliography}

    \begin {itemize}
	\fC
	\item M. Rochkind, ``Advanced UNIX Programming'',
	    Dunod (1991)

	\item W.R. Stevens, S.A. Rago, ``Advanced Programming
	    in the UNIX Environment'' 3rd Ed, Addison-Wesley
	    (2013)

	\item IEEE Computer Society, The Open Group ``Standard
	    for Information Technology - Portable Operating
	    System Interface (POSIX®) - Base Specifications'',
	    IEEE Std 1003.1 (2017)

	    Online version: \url{https://pubs.opengroup.org/onlinepubs/9699919799/}

    \end {itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Qu'est-ce qu'un système d'exploitation?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {From the first computers to operating systems}

\begin {frame} {What is an operating system?}
    How to define an operating system (OS)?

    \begin {itemize}
	\item Easy, sir: Windows, Linux, FreeBSD, etc.!
    \end {itemize}

    \medskip

    Is it that simple?

    \begin {itemize}
	\item if Linux is an OS, what are Debian, Ubuntu, etc.?
	\item and Android, iOS?
	\item and QNX, RTLinux, VxWorks?
	\item and Contiki, TinyOS?
    \end {itemize}

    And, by the way...

    \begin {itemize}
	\item does an Internet access box have an OS?
	\item does an X terminal have an OS?
	\item does a connected watch have an OS?
	\item does a radiator thermostat have an OS?
	\item does a car have an OS?
    \end {itemize}

\end {frame}

\begin {frame} {What is an operating system?}

    To define what an OS is, we need to look at its history.

    \medskip

    \implique what needs / problems an OS must solve?

\end {frame}

\begin {frame} {History -- The first computers}

    Example: ENIAC (1946)

    \begin {center}
	\figcredit{eniac}{.8} {ENIAC -- {\ccpd} U.S. Army \href{https://commons.wikimedia.org/wiki/File:Eniac.jpg}{Wikimedia Commons}}
    \end {center}

\end {frame}

\begin {frame} {History -- The first computers}

    On these computers:

    \begin {itemize}
	\item programming: wiring connections between units

	\item results: to be consulted on luminous indicators

    \end {itemize}

    The computer:
    \begin {itemize}
	\item is very expensive (usually only one copy)
	\item is very difficult to program (originally 6 programmers
	    on the ENIAC, programming takes several weeks)
	\item is programmed by physical wiring
	\item runs only one program at a time
	\item has few or no peripherals
    \end {itemize}

    \implique no operating system

\end {frame}


\begin {frame} {History -- Key start}

    Next step:

    \begin {center}
	\figcredit{pdp1}{.5}{PDP-1 control board -- {\ccby} fjarlq / Matt \href{https://commons.wikimedia.org/wiki/File:PDP-1_control_board.jpg}{Wikimedia Commons}}
    \end {center}

    \begin {itemize}
	\item computer control panel
	\item program entry in memory with switches
	\item program start with switch
	\item reading results with indicator lights
    \end {itemize}
    \implique no operating system

\end {frame}


\begin {frame} {History -- Punched card}

    The punched card (early 1950s)

    \begin {minipage} [c] {.40\linewidth}
	\figcredit{carte-perfo}{}{{\ccbysa} Arnold Reinhold \href{https://commons.wikimedia.org/wiki/File:IBM1130CopyCard.agr.jpg}{Wikimedia Commons}}
    \end {minipage}
    \hfill
    \begin {minipage} [c] {.58\linewidth}
	\figcredit{ibm704}{} {IBM 704 with punched card reader -- {\ccpd} NASA \href{https://commons.wikimedia.org/w/index.php?curid=6455009}{Wikimedia Commons}}
    \end {minipage}

    \begin {itemize}
	\item program (and data) input device
	\item program in (read-only) memory to start the player,
	    store the program in memory, and start execution
    \end {itemize}

\end {frame}

\begin {frame} {History -- Monitor}

    Punched card \implique automated progam entry

    \medskip

    However:

    \begin {itemize}
	\item intervention of a human operator to:
	    \begin {itemize}
		\item place the card tray in the reader
		\item start playback
		\item wait for the end of the program
		\item retrieve results (on printer)
	    \end {itemize}

	\item the computer is very expensive: every minute of calculation
	    is expensive!

    \end {itemize}

    Hence: memory-resident ``\textbf {monitors}'' programs

    \begin {itemize}
	\item always one program at a time
	\item automated program switching
	\item ``batch'' processing (of cards) \implique
	    \textbf {batch processing}
	\item easy access to peripherals
	\item first embryos of operating systems
    \end {itemize}

\end {frame}

\begin {frame} {History -- Spooling}
    Expensive computer \implique better value for money?

    \begin {itemize}
	\item some peripherals (punched card reader,
	    printer) are slow

	    \fig{spool1}{.9}


	\item can we run a calculation while the slow peripherals
	    work?

	    \fig{spool2}{.9}

    \end {itemize}

\end {frame}

\begin {frame} {History -- Spooling}
    Example: IBM 7094 (1963)

    \begin {center}
	\figcredit{spool-tanenb}{.8} {Excerpt from Tanenbaum, A.S., Woodhull A.S.,
		``Operating Systems, Design and Implementation'', 3rd ed, Pearson}
    \end {center}

    \begin {itemize}
	\item IBM 7094: very expensive
	\item IBM 1401: inexpensive (for the time) \\
	    \implique dedicated to
	    transfers between slow peripherals and
	    magnetic tapes (fast)
	\item manual tape transfer \\
	    \implique evolution towards a direct connection \\
	    \implique evolution towards hard disks

    \end {itemize}
\end {frame}

\begin {frame} {History -- Spooling}
    In summary:

    \begin {itemize}
	\item evolution towards more ``intelligent''
	    ``peripherals''
	\item run in parallel with the central processor

    \end {itemize}

    \medskip

    \implique now it is time to take advantage of this parallelism

    \begin {itemize}
	\item the monitor must manage slow hardware resources
	    to exploit their parallelism
    \end {itemize}
\end {frame}

\begin {frame} {History -- Multiprogramming}
    And when the program is waiting for the result of an input/output?

    \begin {itemize}
	\item ``democratization'' of peripherals
	\item e.g. storage or retrieval of temporary data
	    on a peripheral device (magnetic tape, hard disk, etc.)

	\item processor downtime \\
	    \implique use this downtime for another program

    \end {itemize}

    \medskip

    Introduction of multiprogramming \implique \textbf {supervisor}
\end {frame}

\begin {frame} {History -- Multiprogramming}

    The supervisor loads several programs into memory:
    \begin {itemize}

	\item when the current program requests I/O, the
	    supervisor starts the next program
	\item when the I/O controller signals the end of I/O,
	    the first program resumes execution
    \end {itemize}

    \medskip

    Example: Atlas Supervisor, U. of Manchester (1962)
\end {frame}

\begin {frame} {History -- Multiprogramming}
    Questions raised by multiprogramming:

    \begin {itemize}
	\item how to protect programs from each other?
	    \\
	    \implique forbidden access to variables in another program

	    \implique \textbf {memory management unit} (hardware component)

	\item how the supervisor regains control
	    when the I/O controller has finished?

	    \implique \textbf {interrupt mechanism}
    \end {itemize}

\end {frame}

\begin {frame} {History -- Teletype}
    \begin {minipage} [c] {.4\linewidth}
	\figcredit{term-tty}{.9}{Teletype Model ASR33 -- {\ccbysa} AlisonW \href{https://commons.wikimedia.org/wiki/File:Teletype_with_papertape_punch_and_reader.jpg}{Wikimedia Commons}}
    \end {minipage}%
    \begin {minipage} [c] {.6\linewidth}
	This is a revolution!
	\begin {itemize}
	    \item fundamental change in interaction
		with the computer
	    \item no more waiting for the punched card tray to pass
		cards to the computer
	    \item keyboard commands \\
		\implique command language
	    \item directly printed result
	\end {itemize}
    \end {minipage}

\end {frame}

\begin {frame} {History -- Teletype}

    \begin {itemize}
	\item it is a peripheral like any other...

	\item ... but interaction modifies the need

	\item the supervisor launches actions in response to
	    commands typed into the console

	\item ``interactive'' use $\neq$ ``batch'' use

    \end {itemize}

    \bigskip

    Teletype in action: \url {https://youtu.be/X9ctLFYSDfQ}

\end {frame}

\begin {frame} {History -- Timesharing}
    Connect several terminals to a single computer:

    \begin {itemize}
	\item accommodate multiple users
	\item make the cost of a computer more profitable
	\item connection via a direct serial link, or via a modem
	\item give everyone the impression of having ``their own'' computer
	\item timesharing systems
    \end {itemize}

    Examples:
    \begin {itemize}
	\item CTSS (Compatible Time-Sharing System): MIT (1961)
	    \\
	    IBM 7094 modified by IBM, maximum 32 users
	\item STSS (Stanford Time-Sharing System): Stanford (1963)
	    \\
	    DEC PDP-1, 12 terminals
    \end {itemize}
\end {frame}

\begin {frame} {History -- Timesharing}
    Allocate the processor for small portions of time to
    each user:

    \begin {itemize}
	\item quantum: duration set by the system (e.g. 20 ms)
	\item for a human, the programs seem to run
	    in parallel
    \end {itemize}


    \begin {center}
	\fig{quantum}{.9}
    \end {center}

\end {frame}

\begin {frame} {History -- Timesharing}
    Questions raised by timesharing:

    \begin {itemize}
	\item how to get back control at the end of quantum?
	    \\
	    \implique \textbf {clock} mechanism with interruption

	\item how to manage multiple users?
	    \\
	    \implique identity: validation and authorizations \\
	    \implique software mechanisms

	\item how to prevent users from exceeding their permissions?
	    \\
	    \implique preferred and non-preferred
		\textbf {execution modes} \\
	    \implique some instructions prohibited in unprivileged mode

	    \smallskip

	    Example:
	    \begin {itemize}
		\item disk access: reserved for
		    privileged
		\item user programs: run
		    in unprivileged mode
		    \\
		    \implique through the system to
		    access files
	    \end {itemize}

    \end {itemize}

\end {frame}

\begin {frame} {History -- System call}
    When a program wishes to perform (for example) a disk access
    access, it makes a \textbf {system call}:

    \begin {itemize}
	\item special instruction (TRAP, SVC, INT, etc. depending on the
	    processor)

	\item leads to (among other things):
	    \begin {itemize}
		\item switching to privileged mode
		\item rerouting the program to a specific address
	    \end {itemize}

	\item specific address = operating system
	\item \textbf {check} of parameters, permissions, etc.
	    \\
	    \implique to prevent users from exceeding their permissions
	\item perform the action requested by the system call

    \end {itemize}

\end {frame}

\begin {frame} {History -- Timesharing}

    Early 1970s: cathode ray tube (CRT) terminals

    \begin {center}
	\figcredit{term-adm3a}{.4}{ADM 3A {\ccbysa} Rama \href{https://commons.wikimedia.org/wiki/File:Lear_Siegler_ADM-3A-IMG_1508.jpg}{Wikimedia Commons}}
    \end {center}

    \medskip

    Golden age of ``mainframes\footnote{\url{https://en.wikipedia.org/wiki/Mainframe_computer}}'' 

    \implique stabilization of operating systems around the
    concepts seen above

\end {frame}

\begin {frame} {Summary}
    Operating system features

    \begin {itemize}
	\item make computer use profitable
	    \\
	    \implique use all dead time
	\item share a computer between several users
	    \\
	    \implique share hardware resources fairly

	\item easier access to peripherals
	    \\
	    \implique provide an interface with the hardware
	    \\
	    \implique to take advantage of peripheral parallelism

	\item enable users to edit, develop,
	    launch programs
    \end {itemize}

    ... while offering the necessary safety guarantees
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Le noyau
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {The kernel}

\begin {frame} {Let us continue the story}

    Key operating systems:

    \begin {itemize}
	\item Burroughs MCP (1961)
	    \begin {itemize}
		\item designed for the Burroughs B5000 computer
		\item first OS programmed in \textbf {high-level
		    language}
		\item many innovative features (multiprocessor,
		    virtual memory)
	    \end {itemize}
	\item IBM OS/360 (1964)
	    \begin {itemize}
		\item ``Universal'' OS for IBM System/360 computers
		\item huge (tens of millions of lines of assembler)
		\item \textbf {complex}, many delays
		\item F. Brooks ``The Mythical Man-Month''
	    \end {itemize}
	\item CTSS (\textbf {MIT}, 1961)
	    \begin {itemize}
		\item first multi-user system
		\item IBM 7094 modified (2 copies)
	    \end {itemize}
    \end {itemize}

\end {frame}

\begin {frame} {Multics}

    \begin {itemize}
	\item Multics (MULtiplexed Information and Computing Service)
	    \ctableau {\fD} {|ll|} {
		1963 & specification (MIT) \\
		1964 & project start-up \\
		1964 & General Electric + Bell Labs join MIT \\
		1968 & first``self-hosting'' version \\
		1969 & \textbf {Bell Labs} withdraws from the project \\
		1973 & commercial announcement \\
		1985 & end of development \\
		2000 & closure of last site (Canada Defence Min) \\
	    }

	    \begin {itemize}
		\item Highly ambitious system
		\begin {itemize}
		    \item written in high-level language (PL/1)
		    \item uniform access to memory and files
		    \item hierarchical file system
		    \item dynamic link editing
		    \item dynamic hardware reconfiguration
		    \item ``daemon'' process
		    \item safety (B2 certification in 1985)
		\end {itemize}
		\item 84 sites in all (including 31 French universities/centres)
	    \end {itemize}
    \end {itemize}

\end {frame}

\begin {frame} {Unix}

    \begin {itemize}
	\item Unix
	    \ctableau {\fD} {|ll|} {
		\rcb
		1969 & Bell Labs withdraws from Multics project \\
		      & recovery of a PDP-7, writing in assembler \\
		1971 & purchase PDP-11/20 in exchange for a text processing system \\
		1972 & creation of the C language and rewriting of Unix in C \\
		1973 & first releases \\
		1979 & Unix v7 (around 10,000 lines of C and some
		    assembler) \\
		1980 & 4BSD distribution \\
		1992 & AT\&T vs BSDi vs Berkeley trial (ended in 1994) \\
	    }

	    \begin {itemize}
		\item Lots of innovative ideas
		    \begin {itemize}
			\item simple
			\item written in C
			\item clear separation of \textbf {kernel} / utilities
			    (or applications)
			\item everything is a file
			\item no internal file structure
			\item simple (minimalist) user interface
			\item Unix philosophy (kiss: ``keep it simple, stupid'')
			\item mobile
		    \end {itemize}
	    \end {itemize}
	    
    \end {itemize}

    Unix promotional video (1982): {\fB\url{https://youtu.be/XvDZLjaCJuw}}

\end {frame}

\begin {frame} {The kernel}
    Unix philosophy \implique minimalist approach,
    in contrast to previous operating systems

    \medskip

    The kernel has two fundamental objectives:
    \begin {itemize}
	\item \textbf {share resources fairly}
	\item \textbf {guarantee data security}
    \end {itemize}

\end {frame}

\begin {frame} {The kernel}
    Share resources fairly:

    \begin {itemize}
	\item RAM
	\item processor time
	\item network card
	\item disk space
	\item disk access
	\item etc.
    \end {itemize}
\end {frame}

\begin {frame} {The kernel}
    Guarantee data security

    \begin {itemize}
	\item a process must not access the data of another
	    process (unless authorized)

	\item users must not access unauthorized files

	\item users must not terminate processes of
	    other users

	\item etc.

    \end {itemize}
\end {frame}

\begin {frame} {The kernel}

    To achieve these two fundamental objectives:

    \begin {itemize}
	\item the \textbf {kernel} runs in \textbf {privileged mode}
	    \\
	    \implique it has access to all hardware resources

	\item programs (applications) run, in the
	    context of \textbf {processes}, in \textbf {unprivileged
	    mode}
	    \\
	    \implique calls to the kernel to execute certain operations

    \end {itemize}

    \medskip

    Kernel calls are \textbf {system primitives}

    \begin {itemize}
	\item Kernel programming interface
	    \\
	    \textit {Application Programming Interface} (API)
	\item Form: function calls in C
	\item Example:
	    \\
	    \code {int open (const char *path, int flag, mode\_t mode)}
    \end {itemize}


\end {frame}

\begin {frame} {The kernel}
    \begin {center}
	\fig{noyau}{.7}
    \end {center}
\end {frame}

\begin {frame} {The kernel}
    Definition:

    \begin {quote}
	The kernel consists of the minimum set of system primitives
	necessary to achieve the two fundamental objectives

    \end {quote}

\end {frame}

\begin {frame} {The kernel}
    \begin {itemize}
	\item Computer boot: kernel installed
	    in memory \\
	    \implique it will stay there until the end (reboot,
	    stop, or crash)

	\item The kernel runs in privileged mode

	    \begin {itemize}
		\item sensitive code
		\item difficult to develop and improve
		\item the smaller the better
		\item whatever can be put elsewhere must be put elsewhere
		    \\
		    \vspace* {0.8mm}
		    Example:
		    \begin {itemize}
			\item for the kernel, users = an
			    integer
			\item \code {ls -l} displays login names
			\item \implique the
			    ``integer $\leftrightarrow$ name'' conversion is
			    performed by \code {ls}

		    \end {itemize}
	    \end {itemize}

	\item Demonstrating the kernel concept \\
	    \implique Unix's major contribution

    \end {itemize}

\end {frame}

\begin {frame} {The kernel}
    Which functions must be implemented as system primitives?

    \begin {itemize}
	\item a process must not be allowed to access the disk
	    directly (violation of the second objective)
	    \\
	    \implique the kernel must present an abstraction with
	    permissions: file system, owner,
	    access permissions \\
	    \implique disk access via this
	    abstraction
	    \\
	    \implique \code {open} is a system primitive

	\item read current time \implique peripheral access
	    \\
	    \implique \code {time} is a system primitive

    \end {itemize}
\end {frame}

\begin {frame} {The kernel}
    Which functions must be implemented as system primitives?

    \begin {itemize}
	\item \code {strlen} computes a number of bytes by reading
	    in the memory of the current (i.e. authorized) process
	    \\
	    \implique \code {strlen} is therefore not a system primitive

	    \smallskip

	    Since it is useful, we put it\footnote {Or more precisely
	    its compiled code.} in a \textbf {library} once and for all,
		to avoid having to reprogram it each time

	\item \code {getlogin} returns the user's login name
	    \\
	    \implique this function retrieves the user's number
	    with a system primitive, then converts this number into a
	    name by browsing the \code {/etc/passwd} file (or using an external
		LDAP database in the case of turing).
	    \\
	    \implique \code {getlogin} is not a system primitive

    \end {itemize}
\end {frame}

\begin {frame} {User interface}
    \ctableau {\fD} {|ccc|} {
	\textbf{Date} & \textbf{Workstations}
		& \textbf{Personal computers}
	    \\ \hline
	1968 & \multicolumn {2} {c|} {Mouse prototype by
		Douglas Englebart (Stanford Research Institute)} \\
	1973 & Alto (Xerox) & \\
	1981 & Display Manager (Apollo Computer) & \\
	1983 & W Window System (Stanford) & Lisa (Apple) \\
	1984 & X Window System (MIT) & \\
	1985 & HP Windows (HP),	& Amiga Workbench (Commodore), \\
	\rca     & Sunview (Sun)	& Windows (Microsoft), etc. \\
    }

    The management of a ``graphical'' user interface
    is not a kernel task:
    \begin {itemize}
	\item Mouse, graphic display, touch screen (tablet, etc.) \\
	    \implique kernel-managed peripherals
	\item Window system \\
	    \implique applications
    \end {itemize}
\end {frame}

\begin {frame} {What is an operating system?}
    Back to our opening question:
    \begin {itemize}
	\item Unix (the original, then *BSD): kernel + applications
	\item Linux is a kernel
	    \begin {itemize}
		\item organizations (both voluntary and commercial) have
		    collected applications, compiled them and released,
		    with the kernel, ready-to-use \textbf {distributions}

	    \end {itemize}
	\item Windows: kernel + applications
	\item Android, iOS: kernel (with support for touchscreen peripherals,
	    audio, GSM, GPS, etc.) + applications
	\item Internet box, connected watch, car, etc.: kernel
	    + specific applications
	\item etc.
    \end {itemize}
\end {frame}

\begin {frame} {Some atypical systems}
    \begin {itemize}
	\item real-time systems
	\item constrained systems (e.g. Internet of Things)
    \end {itemize}
\end {frame}

\begin {frame} {Summary}
    \begin {itemize}
	\item Since Unix: distinction between kernel and rest of system
	    operating system

	\item Purpose of this course: to understand the operation of a kernel
	    operating system

    \end {itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% POSIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {POSIX}

\begin {frame} {System primitives interface}
    System primitives = set of functions (callable from C)
    to access kernel services

    \begin {itemize}
	\item Originally (Unix v6, 1975): 43 primitives
	\item Subsequent developments (1980s):
	    \begin {itemize}
		\item Bell Labs, AT\&T
		\item U. of Berkeley
		\item Commercial growth
	    \end {itemize}
	\item Result
	    \begin {itemize}
		\item many divergences \implique incompatibilities
		\item programs are no longer portable
		\item users are not happy
	    \end {itemize}
	\item Standardizing what already exists \implique POSIX
    \end {itemize}

\end {frame}

\begin {frame} {POSIX}

    IEEE POSIX Committee
    \begin {itemize}
	\item IEEE = Institute of Electrical and Electronics Engineers
	    \\
	    (American association that drafts standards)
	\item POSIX = Portable Operating System Interface
	\item IEEE standard: IEEE Std 1003.*
	\item International standard: ISO/IEC 9945
	\item First version: 1988
	\item Current version: 2017
    \end {itemize}

    \medskip

    Major impact \implique no operating system can
    afford not to be POSIX-compatible
\end {frame}

\begin {frame} {POSIX}

    POSIX standardizes many things:

    \begin {itemize}
	\item system primitives and library functions
	\item commands (\code {sh}, \code {ls}, \code {tr}, etc.)
	\item extensions (real time, threads, semaphores, etc.)
    \end {itemize}

    \medskip

    POSIX does not normalize everything:
    \begin {itemize}
	\item an implementation can have its own extensions \\
	    \implique non-standardized \implique non-portable
	\item Example with \code {ls}:
	    \begin {itemize}
		\item POSIX 2017: 26 options (it is a lot...) \\
		\item GNU (Linux): 59 options (it is too much!) \\
	    \end {itemize}
	\item program portable \implique respect POSIX
	\item use the instructions provided

    \end {itemize}

\end {frame}

\begin {frame} {POSIX}

    POSIX does not distinguish between system primitives and
    library function:

    \begin {itemize}
	\item allow freedom for implementation
	\item sometimes fine distinction
	    \begin {itemize}
		\item Example: 6 functions to run a program
		    (\code {execv}, \code {execl}, \code {execvp},
		    \code {execlp}, \code {execve} and \code {execle}),
		    only one is a primitive, the other 5 are
		    library functions
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {POSIX}
    General principles

    \begin {itemize}
	\item POSIX Types
	\item Constants
	\item Error management
	\item ``Pointer'' type parameters
    \end {itemize}
\end {frame}

\begin {frame} {POSIX -- Types}
    \begin {itemize}
	\item The Unix kernel manipulated objects using integers

	    \smallskip
	    Problem: portability (16/32/64 bits? Implementations?)

	\item POSIX has introduced new types (with \code {typedef})
	    \\
	    \implique hide implementation details \\
	    \implique program portability

	    \smallskip
	    A few examples:

	    \ctableau {\fC} {|ll|} {
		\code {uid\_t} & user number \\
		\code {gid\_t} & group number \\
		\code {pid\_t} & process number \\
		\code {mode\_t} & permissions \\
		\code {time\_t} & current time \\
		\code {size\_t} & size, result of sizeof (defined by ISO C) \\
	    }

    \end {itemize}
\end {frame}

\begin {frame} {POSIX -- Types}
    \begin {itemize}
	\item Type definition:
	    \begin {itemize}
		\item size chosen by the system implementer
		\item see inclusion files or current leaflet
	    \end {itemize}
	\item Problem: how to display values with \code{printf}?
	    \begin {itemize}
		\item \code {\%d}, \code {\%ld}, \code {\%lld}? \implique no!
	    \end {itemize}
	\item Solution: convert to \code{intmax\_t}
	    and use the \code{\%jd} format
	    \begin {itemize}
		\item \code {j}: size = that of an
		    \code{intmax\_t}/\code{uintmax\_t}
		\item \code {d}: signed integer
		\item example: \\
		    \code{printf ("size=\%jd", (intmax\_t) stbuf.st\_size)}
	    \end {itemize}
	\item For the rare unsigned types (such as \code{size\_t}):
	    \\
	    \implique convert to \code{uintmax\_t}
	    and use \code{\%ju} format
	    \begin {itemize}
		\item \code {u}: integer \code{unsigned}
	    \end {itemize}

	    % \end {itemize}
    \end {itemize}

\end {frame}

\begin {frame} {POSIX -- Constants}
    \begin {itemize}
	\item Some primitives require an operation to be specified

	\item Example:
	    \ctableau {\fC} {|ll|} {
		\code {r = access ("foo", 0)}
		    & does the file exist? \\
		\code {r = access ("foo", 1)}
		    & can I run the file? \\
		\code {r = access ("foo", 2)}
		    & can I modify the file? \\
		\code {r = access ("foo", 4)}
		    & can I read the file? \\
	    }

	    \medskip

	\item Remember values and their meaning
    \end {itemize}

\end {frame}

\begin {frame} {POSIX -- Constants}
    \begin {itemize}

	\item Defining constants (in include files)

	    File \code {unistd.h}:
	    \lst{unistd.h}{\fD}{}

	\item The previous example becomes:
	    \ctableau {\fC} {|ll|} {
		\code {r = access ("foo", F\_OK)}
		    & does the file exist? \\
		\code {r = access ("foo", X\_OK)}
		    & can I run the file? \\
		\code {r = access ("foo", W\_OK)}
		    & can I modify the file? \\
		\code {r = access ("foo", R\_OK)}
		    & can I read the file? \\
	    }
	    \medskip

	\item Using constants makes programs more readable

	\item Sometimes constants are less practical (permissions)
	    or not widely used

    \end {itemize}

\end {frame}

\begin {frame} {POSIX -- Constants}
    Some constants are not constant...

    \begin {itemize}
	\item semantic arguments for primitives: inclusion files
	    \begin {center}
		\fE
		Examples:
		\tableau {} {|lll|} {
		    \code {R\_OK}
			& reading test for \code {access}
			& \code {unistd.h}
			\\
		    \code {O\_WRONLY}
			& writing opening for \code {open}
			& \code {fcntl.h}
			\\
		    \code {S\_IFDIR}
			& ``directory'' file type
			& \code {sys/stat.h}
			\\
		    ... & &
			\\
		}
	    \end {center}

	\item arbitrary limits: \code {limits.h}
	    \begin {center}
		\fE
		Examples:
		\tableau {} {|ll|} {
		    \code {PATH\_MAX}
			& maximum size of a complete path
			\\
		    \code {LOGIN\_NAME\_MAX}
			& maximum size of a login name
			\\
		    \code {CHILD\_MAX}
			& maximum number of child processes
			\\
		}
	    \end {center}

	\item limits depending on system configuration \\
	    \code {long sysconf (int parameter})
	    \begin {center}
		\fE
		Examples:
		\tableau {} {|ll|} {
		    \code {\_SC\_LOGIN\_NAME\_MAX}
			& maximum username length
			\\
		    \code {\_SC\_OPEN\_MAX}
			& maximum number of file openings
			\\
		    \code {\_SC\_CHILD\_MAX}
			& maximum number of child processes
			\\
		}
	    \end {center}

	\item file system-dependent limits \\
	    \code {long pathconf (const char *path, int parameter})
	    \begin {center}
		\fE
		Examples:
		\tableau {} {|ll|} {
		    \code {\_PC\_LINK\_MAX}
			& maximum number of links on a file
			\\
		    \code {\_PC\_NAME\_MAX}
			& maximum file name size
			\\
		    \code {\_PC\_PATH\_MAX}
			& maximum size of a complete path
			\\
		}
	    \end {center}

    \end {itemize}
\end {frame}

\begin {frame} {POSIX -- Error management}
    In the event of an error, the primitives:
    \begin {itemize}
	\item return -1 on error
	    \begin {itemize}
		\item ... except for a few very rare exceptions
	    \end {itemize}

	\item place a code reflecting the error in the \code {errno} variable

    \end {itemize}

    \medskip

    File \code {errno.h}:
    \lst{errno.h}{\fD}{}

    \medskip

    Description of all possible errors for a primitive \\
    \implique consult POSIX or the \code {man} of the primitive
\end {frame}

\begin {frame} {POSIX -- Error management}
    Example of use (laborious, not recommended):

    \lst{ex-errno1.c}{\fD}{}
\end {frame}

\begin {frame} {POSIX -- Error management}
    Even better: put the display in a function

    \lst{perror.c}{\fD}{}

    The function \code{perror} is \alert{very useful} \\
    \implique it is \alert{integrated} into the standard library:
    \begin {itemize}
	\item \code {void perror (const char *msg)}
	\item \code {char *strerror (int numerr)}
    \end {itemize}
\end {frame}

\begin {frame} {POSIX -- Error management}
    Example of use (final):

    \lst{ex-errno2.c}{\fD}{}
\end {frame}

\begin {frame} {POSIX -- Error management}
    \begin {casseroleblock} {Recommendations}
    \begin {itemize}
	\item \alert {always check} primitive returns

	    \implique leaving an error undetected is equivalent to
	    putting a time bomb in your program

	\item \alert {display} the reason for errors

	    \implique not displaying the exact reason may lead you
	    trying to solve a false problem
    \end {itemize}
    \end {casseroleblock}
\end {frame}

\begin {frame} {POSIX -- Error management}
    It is often useful to have a function for handling
    error cases:

    \medskip

    \lst{ex-errno3.c}{\fD}{}
\end {frame}

\begin {frame} {POSIX -- Error management}
    Even better: the \code{CHK} macro makes testing easier \\
    \implique no reason to miss out!

    \medskip

    \lst{ex-errno4.c}{\fD}{}
\end {frame}

\begin {frame} {POSIX -- ``Pointer'' type parameters}

    Some primitives return more complex results
    than a simple integer

    \begin {itemize}
	\item Pointer-type parameter (on an object to be filled)
	\item Examples:
	    \begin {itemize}
		\item \code {int stat (const char *path,
				\alert {struct stat *stbuf})}
		    \\
		    Returns the file attributes to the 
			location marked by \code{\alert {stbuf}}
		\item \code {pid\_t wait (\alert {int *status})}
		    \\
		    Places in the location marked by \code {\alert
		    {status}} information about the termination of the
		    process
		\item etc.
	    \end {itemize}

	\item The pointed memory location \textbf {must exist}!
    \end {itemize}
\end {frame}

\begin {frame} {POSIX -- ``Pointer'' type parameters}
    Examples of use:

    \ctableau {\fC} {|p{.49\linewidth}|p{.49\linewidth}|} {
	\multicolumn {1} {|c|} {\textbf {Correct}}
	    & \multicolumn {1} {c|} {\textbf {\alert {Faux}}}
	    \\ \hline
	\lst{ex-ptr-ok.c}{\fE}{}
	    &
	    \lst{ex-ptr-bad.c}{\fE}{}
	    \\
	The variable \code {stbuf} exists, we pass its address
	    & The variable \code {stbuf} is an uninitialized pointer,
		the kernel will write the result somewhere (where?) in
		memory
	    \\
    }

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tracer les primitives
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Drawing system primitives}

\begin {frame} {Drawing system primitives}
    On many systems, it is possible to trace the system primitives
    executed by a process

    \medskip

    Commands not standardized by POSIX:

    \ctableau {} {|ll|} {
	Linux & \code{strace} command \\
	FreeBSD & \code{truss} command \\
    }
\end {frame}

\begin {frame} {Drawing system primitives}
    Example on Linux:

    \lst{strace.out}{\fD}{}

    \medskip

    Simplified example, without process initialization and termination
    process

\end {frame}

\begin {frame} {Drawing system primitives}
    \code{strace} benefits (or equivalent):

    \begin {itemize}
	\item discover primitives
	\item understand how system controls work
	    \begin {itemize}
		\item ... if they are simple
		\item no need to try \code{cc} or \code{doxygen}...
	    \end {itemize}
	\item distinguish between primitives and library functions
	    \begin {itemize}
		\item \code{strace} = spy placed at the edge of the core
		\item library functions are therefore not displayed
	    \end {itemize}
	\item debug your own programs
	    \begin {itemize}
		\item ``oddly enough, my program ends
		    before calling the primitive thingy''
	    \end {itemize}
    \end {itemize}

    \medskip

    Feel free to use the \code{strace} command!
\end {frame}

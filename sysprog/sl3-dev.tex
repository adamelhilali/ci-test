\def\inc{inc3-dev}

\titreA {Devices}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Introduction}
    \begin {center}
	\fig{arch-old}{.7}
    \end {center}

    With Unix, devices are accessed as files.
    \\
    \implique ``all is file'' adage
\end {frame}

\begin {frame} {Introduction}
    \begin {itemize}
	\item print on printer:
	    \lst{lpr.c}{\fD}{}
	\item read the contents of a hard disk:
	    \lst{dsk.c}{\fD}{}
    \end {itemize}

    Where is the magic?
\end {frame}

\begin {frame} {New file types}
    Two new special file types (devices):
    \ctableau {\fD} {|p{.45\linewidth}p{.45\linewidth}|} {
	\multicolumn {1} {|c} {\textbf {``character'' mode}}
	    & \multicolumn {1} {c|} {\textbf {``block'' mode}}
	    \\ \hline
	with \code {stat}: \code {S\_IFCHR} and \code {S\_ISCHR()}
	    & with \code {stat}: \code {S\_IFBLK} and \code {S\_ISBLK()}
	    \\
	should have been called: ``raw'' mode
	    & should have been called: ``buffered'' mode
	    \\
	any sequence of bytes passed to \code {read} or
		\code {write} is transferred immediately
	    & any sequence of bytes passed to \code {read} or
		    \code {write} is buffered, before being
		    transferred to the device
	    \\
	almost all devices
	    & mainly hard disks
	    \\
	device identified by a couple
		<\textit {major}, \textit {minor}>
	    & device identified by a couple
		<\textit {major}, \textit {minor}>
	    \\
    }

    \medskip

    Files located (traditionally) in \code {/dev}

\end {frame}

\begin {frame} {Device number}
    Devices are identified by a pair <major, minor>

    \ctableau {\fB} {|ll|} {
	major & driver number (raw or buffered) \\
	minor & device number managed by this driver \\
	\rcb & (+ any other information)
	    \\
    }

    \begin {itemize}
	\item Example: ``sd'' disk driver (Linux)
	    \begin {itemize}
		\item major = 8
		\item minor = disk address (bits $\geq$ 4) +
		    partition number (bits 0..3)
	    \end {itemize}
	\item type \code {dev\_t}: major + minor

    \end {itemize}
\end {frame}

\begin {frame} {Device files -- Creation}
    \prototype {
	\code {int mknod (const char *path, mode\_t mode, dev\_t dev)}
    }

    \begin {itemize}
	\item creates a device file
	\item primitive restricted to system administrator
	\item \code {mode}: similar to \code {st\_mode} (type
	    and permissions)
	\item use not supported by POSIX
	\item obsolete primitive: automatically created devices
    \end {itemize}
\end {frame}

\begin {frame} {Device files -- Primitive stat}
    Back to \code {stat}:

    \begin {itemize}
	\item whether the file is a device (block or character)
	    \begin {itemize}
		\item \code {st\_mode} field tested with \code {S\_ISBLK()}
		    or \code {S\_ISCHR()}

		\item \code {st\_rdev} field: device number
	    \end {itemize}

	\item for all files

	    \begin {itemize}
		\item directories, symbolic links,
		    devices, etc.
		\item the file resides on a disk \item \code
		{st\_dev} field: device number
		    disk
	    \end {itemize}
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pilotes de périphériques
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Device drivers}

\begin {frame} {Device drivers}
    \begin {itemize}
	\item device drivers:
	    \begin {itemize}
		\item set of functions
		\item compiled code added to kernel
		\item functions referenced in the
		    kernel drivers
	    \end {itemize}
	\item different functions depending on the type of driver:
	    \ctableau {\fC} {|p{.45\linewidth}p{.45\linewidth}|} {
		\multicolumn {1} {|c} {\textbf {``character'' mode}}
		    & \multicolumn {1} {c|} {\textbf {``block'' mode}}
		    \\ \hline
		\code {open}, \code {close}, \code {read},
		    \code {write}, \code {ioctl} and interrupt functions
		    & \code {open}, \code {close}, \code
		    {strategy} and interrupt functions
		    \\
	    }
    \end {itemize}
\end {frame}

\begin {frame} {Device drivers}
    Query flow:

    \begin {center}
	\fig{cdevsw}{}
    \end {center}

\end {frame}

\begin {frame} {Character mode}
    Driver interface:
    \begin {itemize}
	\item \code {open}, \code {close}, \code {read} and
	    \code {write} functions: well known
	\item interrupt processing: called when an interrupt
	    is generated at the end of processing by the device
	\item \code {ioctl} function: corresponds to the primitive

	    \prototype {
		\code {int ioctl (int fd, int request, .... parameter)}
	    }
	    \begin {itemize}
		\item not standardized by POSIX (for devices)
		\item specific operations
		    that do not fit into the read/write model
		\item examples:
		    \begin {itemize}
			\item ask the printer for its status 
			    status (printing, out of paper, etc.).
			    paper, etc.).

			\item eject the media (magnetic cartridge,
			    CD, DVD, etc.)

			\item change serial link speed
		    \end {itemize}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Character mode}
    A special example: the terminal (teletype)

    \medskip

    \begin {minipage} [c] {.3\linewidth}
	\fig{tty}{}
    \end {minipage}%
    \begin {minipage} [c] {.7\linewidth}
	\begin {itemize}
	    \fB
	    \item it is connected by a serial link
		\begin {itemize}
		    \fC
		    \item parameters: link speed, number of
			bits, parity
		    \item hang up the modem at the end of the connection
		    \item etc.
		\end {itemize}
	    \item it has its own characteristics
		\begin {itemize}
		    \fC
		    \item characters for deletion, stop, suspension
			end of file, etc.
		    \item buffer characters per line or
			send them without waiting
		    \item number of rows, columns
		    \item etc.

		\end {itemize}
	\end {itemize}
    \end {minipage}

    \medskip

    \implique \code {stty} command to change parameters (\implique
    \code {ioctl})

\end {frame}

\begin {frame} {Character mode}
    A special example: the terminal (teletype)

    \begin {itemize}
	\fB
	\item the driver's job is to route the bytes
	    to the terminal

	\item some programs (e.g: \code {zsh}, \code {vi},
	    \code {more}, etc.) must also be able to clear the screen,
	    position the cursor at a certain location,
	    recognize function keys, etc.

	\item different control sequences for different terminals
	\item position the cursor on row X and column Y: send...
	    \begin {itemize}
		\fD
		\item \framebox {ESC} \framebox {[} X \framebox {;}
		    Y \framebox {f} for a DEC VT100 terminal

		\item \framebox {ESC} \framebox {\&} \framebox {a}
		    Y \framebox {c} X \framebox {Y} for a
		    HP 2645 terminal

		\item Note: \framebox {ESC} = code byte 27
	    \end {itemize}

	\item \code {termcap} database, then \code {terminfo} for all
	    sequences
	    \begin {itemize}
		\item \code {TERM} environment variable: indicates
		    terminal model
	    \end {itemize}

	\item programs (\code {zsh}, \code {vi}, etc.) need to
	    use this database
	    \\
	    \implique this is not the driver's mission

    \end {itemize}
\end {frame}

\begin {frame} {Character mode}
    Same principle applies to most devices:

    \begin {itemize}
	\item driver routes bytes to device
	\item this does not prevent applications from managing the
	    protocol of each device

	    \begin {itemize}
		\item several different mouse protocols \\
		    \implique only the X-Window server needs to know them
		\item each printer has its own ``control
		    language''
		    \\
		    \implique any program wishing to print must have
		    a collection of adapters for the various printers
		    (sometimes erroneously ``drivers'')
		\item etc.

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Block mode}
    Driver interface:
    \begin {itemize}
	\item \code {open}, \code {close} functions: called to
	    mounting/unmounting the file system in the tree structure
	\item interrupt processing: same as raw mode
	\item \code {strategy} function: 2 roles
	    \begin {itemize}
		\item reads a block from the memory, in the ``\textit {buffer
		    cache}'' (later)
		\item writes a modified block from the ``\textit {buffer cache}''
		    towards the disk
		\item make some optimizations possible \\
		    (ex: elevator algorithm)
	    \end {itemize}
    \end {itemize}

    \medskip

    Note: most block-mode drivers also come with a
    character-mode driver (for \code {ioctl})
\end {frame}

\begin {frame} {Pseudo-devices}
    A driver can offer a service accessible through \code {read}
    or \code {write} without a real device

    \medskip

    Examples:

    \ctableau {\fB} {|ll|} {
	\code {/dev/null} & trash \\
	\code {/dev/mem} & all computer memory \\
	\code {/dev/random} & source of random data \\
    }

\end {frame}

\begin {frame} {Pseudo-devices}
    Special case: pseudo-terminals

    \begin {itemize}
	\item many programs are designed to be connected
	    to a terminal

	\item How does \code {vi} do when connected through
	    \code {ssh} or through an X-Window terminal?
	    \\
	    \implique one must simulate a terminal and a serial link

	\item abstraction: pseudo-terminal

	    \begin {itemize}
		\item pair of pseudo-devices: master and slave
		\item managed by the same driver
		\item the ssh server controls the master
		\item session programs access
		    the slave: it simulates a “real” terminal
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Pseudo-devices}

    \begin {minipage} [c] {.4\linewidth}
	\fig{pty}{}
    \end {minipage}%
    \begin {minipage} [c] {.6\linewidth}
	\begin {itemize}
	    \fB
	    \item the ssh server opens the pair
	    \item any byte sent to the master (resp. slave)
		is transmitted to the slave (resp. master)
	    \item the ssh server transmits bytes received from the network
		the network to the master
	    \item ssh session programs (\code {sh},
		\code {vi}) are connected to the slave
	    \item any change in terminal parameters (via \code {ioctl})
		is transmitted to the ssh server
	\end {itemize}
    \end {minipage}

    \begin {itemize}
	\item other uses:
	    \begin {itemize}
		\item ``terminal'' windows in a graphic environment
		\item \code {script} (session registration)
		\item \code {screen} and \code {tmux} programs
	    \end {itemize}
    \end {itemize}

\end {frame}

\begin {frame} {Evolutions}
    Reality: unfortunately very complex...
    \begin {center}
	\fig{arch-now}{.6}
    \end {center}

    Example: disks connected via the SATA bus, SCSI,
    USB or even via an ancestral IDE bus.

\end {frame}

\begin {frame} {Evolutions}
    \begin {itemize}
	\item increased hardware complexity

	    \begin {itemize}
		\item management of different bus levels
		\item code sharing between drivers (e.g. disks)
	    \end {itemize}

	\item device dynamicity

	    \begin {itemize}
		\item connect ou disconnect ``hot-plug'' devices
		\item auto-recognition of devices
	    \end {itemize}
    \end {itemize}

    \implique driver complexity
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Le répertoire /dev
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {The /dev directory}

\begin {frame} {The /dev directory}
    Historically, the \code {/dev} directory was populated ``by
    hand''

    \begin {itemize}
	\item few device additions or deletions
	\item \code {mknod} command
	\item \code {MAKEDEV} script in \code {/dev}
    \end {itemize}
\end {frame}

\begin {frame} {The /dev directory}
    Evolution \implique dynamic addition of devices

    \begin {itemize}

	\item ``devfs'' file system

	    \begin {itemize}
		\item automatic creation/deletion of special files
		\item requires devices that can identify themselves
		    \begin {itemize}
			\item ``plug and play''
		    \end {itemize}
	    \end {itemize}

	\item Additional program for exception handling

	    \begin {itemize}
		\item I want a \code {/dev/CDROM} link to the
		    third CD drive on my system

		\item I want to be able to connect my camera
		    without administrator rights
	    \end {itemize}
    \end {itemize}
\end {frame}

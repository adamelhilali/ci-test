fd = open ("foo", O_WRONLY | O_CREAT | O_TRUNC, 0666) ;
if (fd == -1)
    grumble ("open") ;
if (close (1) == -1)
    grumble ("close") ;
if (dup (fd) == -1)
    grumble ("dup") ;
if (close (fd) == -1)
    grumble ("close") ;
execv (path, tabargv) ;
grumble ("execv") ;

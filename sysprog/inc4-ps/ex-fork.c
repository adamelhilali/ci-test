pid_t v ;

switch (v = fork ())
{
    case -1 :		// error : don't forget this case 
	grumble ("fork") ;

    case 0 :		// the child :
	child () ;	  // isolate the child 
	exit (0) ;	  // sanitary cordon

    default :		// the parent continues
	...
}

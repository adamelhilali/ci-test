fd = open ("foo", O_WRONLY | O_CREAT | O_TRUNC, 0666) ;
if (fd == -1)
    grumble ("open") ;
if (dup2 (fd, 1) == -1)
    grumble ("dup2") ;
if (close (fd) == -1)
    grumble ("close") ;
execv (path, tabargv) ;
grumble ("execv") ;

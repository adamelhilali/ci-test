pid_t v ; int status ;

switch (v = fork ())
{
    case -1 :
	grumble ("fork") ;
    case 0 :		// the child 
	child () ;
	exit (0) ;
    default :		// the parent

	if (wait (&status) == -1)
	    grumble ("wait") ;

	if (WIFEXITED (status))
	    printf ("exit(%d)\n", WEXITSTATUS (status)) ;
	else if (WIFSIGNALED (status))
	    printf ("signal %d\n", WTERMSIG (status)) ;
	else
	    printf ("other status\n") ;
}

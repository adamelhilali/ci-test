pid_t pid, ppid ;
uid_t uid ;
gid_t gid ;

pid = getpid () ;
printf ("I am the process %jd\n", (intmax_t) pid) ;
ppid = getppid () ;
printf ("my parent is %jd\n", (intmax_t) ppid) ;
uid = getuid () ;
printf ("my owner is %ju\n", (intmax_t) uid) ;
gid = getgid () ;
printf ("and my group is %ju\n", (intmax_t) gid) ;

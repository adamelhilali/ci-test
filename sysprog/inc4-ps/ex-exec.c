switch (fork ())
{
    case -1 :
	grumble ("fork") ;

    case 0 :		// child 
	execl ("/bin/echo", "echo", "a", NULL) ;
	grumble ("execl") ;

    default :		// parent
	if (wait (&status) == -1)
	    grumble ("wait") ;
	if (WIFEXITED (status) && WEXITSTATUS (status) == 0)
	    printf ("ok\n") ;
	else printf ("not ok\n") ;
}

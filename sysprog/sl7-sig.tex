\def\inc{inc7-sig}

\newcommand {\ctrl} [2] {\framebox{#2{}CTRL}\framebox{#2{}#1}}

\titreA {Signals}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Introduction}
    Definition:
    \begin {quote}
	signal = event notified by the kernel to a process
    \end {quote}

    Exemples:
    \begin {itemize}
	\item hardware events
	    \begin {itemize}
		\item disconnect (\code {SIGHUP})
		\item press on \ctrl{C}{\fD}
		    (\code {SIGINT})
	    \end {itemize}
	\item events due to a program action
	    \begin {itemize}
		\item memory addressing error (\code {SIGSEGV})
		\item illegal instruction (\code {SIGILL})
		\item process alarm (\code {SIGALRM})
		\item writing in a pipe without a reader (\code {SIGPIPE}), etc.
	    \end {itemize}
	\item events with no associated semantics for the kernel
	    \begin {itemize}
		\item ``user'' signals (\code {SIGUSR1} and
		    \code {SIGUSR2})
		\item termination signal (\code {SIGTERM})
		\item absolute termination signal (\code {SIGKILL})
	    \end {itemize}

    \end {itemize}

    Signals are represented by integers \implique \code {SIG*}
\end {frame}

\begin {frame} {Introduction -- Signal emission}
    The previous examples show 3 main categories:

    \ctableau {\fC} {|ll|} {
	\textbf{Event...} & \textbf{Signal emission by...} \\
	hardware & the kernel in response to the event \\
	program action & the kernel following an exception detected by the processor \\
	without semantics & the kernel at the request of a process via the \code{kill} primitive \\
    }

\end {frame}

\begin {frame} {Introduction -- Signal emission}
    \code{kill} primitive: send a signal to a process:

    \prototype {
	\code {int kill (pid\_t pid, int sig)} \\
    }

    \begin{itemize}
	\item one must have rights on the process
	    \begin{itemize}
		\item example: the emitting process has
		    the same owner as the receiving process

	    \end{itemize}
	\item any signal can be emitted
	    \begin{itemize}
		\item all, not just those without associated semantics
		\item example: \code{kill(getppid(), SIGINT);}
	    \end{itemize}
	\item Unix \code{kill} command
	    \begin{itemize}
		\item example: \code{kill -HUP 1234}
		\item simply calls the \code{kill} primitive
		    after converting the argument into a signal number
	    \end{itemize}
    \end{itemize}
\end {frame}

\begin {frame} {Introduction -- Default action}
    Process notification \implique default process action

    \begin {itemize}
	\item end process
	    \begin {itemize}
		\item example: \ctrl{C}{\fD}
		\item example: \code{SIGSEGV} \implique memory
		    addressing error
		\item some specific signals generate a
		    \code {core} file
		    \begin {itemize}
			\item for posterior memory analysis
			\item example: \code {\$ gdb a.out core}
			\item may require: \code {\$ ulimit
			    -c unlimited}
			    \\
			    {\fD
			    (in addition to \code{sudo sysctl -w kernel.core\_pattern=core} on Ubuntu)}

		    \end {itemize}
	    \end {itemize}
	\item ignore signal
	    \begin {itemize}
		\item example: child termination
		    \implique \code{SIGCHLD}
	    \end {itemize}
	\item suspend process execution
	    \begin {itemize}
		\item example: \ctrl{Z}{\fD}
		    \implique \code{SIGTSTP} (terminal stop)
		\item or send \code{SIGSTOP}
	    \end {itemize}
	\item resume process execution
	    \begin {itemize}
		\item Job control: \code{fg}/\code{bg} in shell \implique
		    \code {SIGCONT}

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Introduction -- Default action}
    Some signals and their default action:
    \ctableau {\fD} {|lccl|} {
	\textbf {Signal}
	    & \textbf {Action}
	    & \textbf {core}
	    & \textbf {Signification}
	    \\ \hline
	SIGALRM  & End     &     & Process alarm \\
	SIGCHLD  & Ignore  &     & Termination of a child \\
	SIGCONT  & Resume  &     & \code{fg}/\code{bg} \\
	SIGFPE   & End     & yes & Invalid expression (e.g. division by 0) \\
	SIGHUP   & End     &     & Disconnect \\
	SIGINT   & End     &     & \ctrl{C}{\fE} \\
	SIGKILL  & End     &     & Atomic weapon... \\
	SIGPIPE  & End     &     & Writing in a pipe without a reader \\
	SIGQUIT  & End     & yes & \ctrl{\textbackslash}{\fE} \\
	SIGSEGV  & End     & yes & Access to an invalid memory cell \\
	SIGSTOP  & Suspend &     & Suspend the process \\
	SIGTERM  & End     &     & Process termination request \\
	SIGTSTP  & Suspend &     & \ctrl{Z}{\fE} \\
	SIGUSR1  & End     &     & Signal without system 1 definition \\
	SIGUSR2  & End     &     & Signal without system 2 definition \\
	SIGWINCH & Ignore  &     & Change window size \\
    }
\end {frame}

\begin {frame} {Introduction -- Change action}

    Default action is not always desirable \\
    \implique a specific action can be associated with each signal

    \begin {itemize}
	\item \code{SIG\_IGN}: ignore the signal
	\item \code{SIG\_DFL}: the default action
	\item execute a predefined function
	    \begin {itemize}
		\item the function \alert{interrupts} program execution
		\item when the function ends, the program
		    \alert{resumes} where it left off

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Introduction -- Change of action}

    How does the program react when a signal occurs and the associated
    action is the execution of a function?

    \bigskip

    \begin {minipage} [c] {.45\linewidth}
	\hspace*{-2.5em}
	\fig{derout-gen}{1.15}
    \end {minipage}%
    \begin {minipage} [c] {.55\linewidth}
	Execution of a predefined function:
	\begin {itemize}
	    \item the function \alert{interrupts} program execution
	    \item when the function ends, the program
		\alert{resumes} where it left off

	\end {itemize}
    \end {minipage}
\end {frame}

\begin {frame} {Examples of signal use}

    \begin {itemize}
	\item save the current calculation in case of an interrupt
	    \begin {itemize}
		\item press on \ctrl{C}{\fD}
		    \implique \code {SIGINT}
		\item call up the function programmed for \code {SIGINT}
	    \end {itemize}

	\item interrupt an action without exiting the program
	    \begin {itemize}
		\item example: \ctrl{C}{\fD} with \code{vi}
	    \end {itemize}

	\item end the program ``cleanly''
	    \begin {itemize}
		\item user sends \code {SIGTERM} signal
		\item call up the function programmed for \code {SIGTERM}
		    \begin {itemize}
			\item save data in memory,
			    delete temporary files, etc.
		    \end {itemize}
	    \end {itemize}

	\item continue the program even after a disconnection
	    \begin {itemize}
		\item disconnection \implique \code {SIGHUP}
		\item ignore the signal
	    \end {itemize}

	\item plan an action to be executed in 3 minutes
	    \begin {itemize}
		\item setting an alarm \implique \code {SIGALRM}
		\item call up the function programmed for \code {SIGALRM}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Special case}
    Two special signals: action cannot be modified

    \begin {itemize}
	\item \code {SIGKILL}
	    \begin {itemize}
		\item Signals can be used to execute a function
		    instead of terminating the default process

		\item If it is possible to program a function for each
		    signal, you can have ``immortal'' processes

		\item Hence the \code {SIGKILL} signal:
		    \begin {itemize}
			\item action = default action
			    \implique end the process
			\item impossible to modify this action
			\item there is always a way to end a process!
			\item do not send \code{SIGKILL} (= 9)
			    directly to a process \\
			    \implique the process cannot be terminated
				``cleanly'' \\
			    \implique fire warning shots first
				(e.g.: \code{SIGHUP}, \code{SIGTERM})

		    \end {itemize}

	    \end {itemize}

	\item \code {SIGSTOP}
	    \begin {itemize}
		\item Similar to \code{SIGKILL}: 
		    imperative process suspension
		\item Not to be confused with \code {SIGTSTP}
		    (sent after \ctrl{Z}{\fD})

	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {The two stages of a signal}
    The notification of a signal to a process takes place in two stages:
    \begin{enumerate}
	\item the kernel \alert{sends} the signal to the destination process
	\item the receiving process \alert{takes} the signal \alert{into account}
    \end{enumerate}
    \bigskip
    \implique it is important to distinguish between these two stages
\end {frame}

\begin {frame} {The two stages of a signal -- First stage}
    First stage: \alert{signal emission}

    \medskip

    \begin{itemize}
	\item each process has a ``\alert{pending
	    signals}'' attribute

	    \begin{itemize}
		\item ``bit field'' type
		    (cf. see below: POSIX \code{sigset\_t} type)
		\item $s$ bit set to 1 $\Leftrightarrow$ $s$ signal waiting
		    to be processed
	    \end{itemize}
	\item sending a signal $s$ to a process $p$ is equivalent to:
	    \begin{itemize}
		\item set bit $s$ to 1 for process $p$
		\item wake up the $p$ process if needed
		    \begin{itemize}
			\item waking up the process does not mean
			    that it is instantly put back on the
			    processor
			\item heavily loaded hardware \implique
			    can take several seconds
		    \end{itemize}
	    \end{itemize}
    \end{itemize}

    \medskip

    Practical consequence:

    \begin{itemize}
	\item transmitting a signal twice quickly
	    does not mean that it will be received twice
	    \begin{itemize}
		\item setting a bit to 1 twice
		    does not set it to 2!
	    \end{itemize}
    \end{itemize}
\end {frame}

\begin {frame} {The two stages of a signal -- Second stage}
    Second stage: \alert{signal processing}

    \medskip

    When a process exits the kernel (end of a primitive, end of hardware
    processing of a hardware event, etc.), it examines its
    ``pending signals'' attribute:

    \begin{itemize}
	\item for each bit $s$ set to 1
	    \begin {itemize}
		\item the process performs the \alert{action
		    associated} with the signal $s$
		\item bit $s$ is reset to 0
	    \end {itemize}
    \end{itemize}

    It is how the signal is taken into account by the process
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Définir l'action associée à un signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Action associated with a signal}

\begin {frame} {Action associated with a signal -- sigaction}
    \code{sigaction} primitive: defines the action associated with a signal

    \prototype {
	\code {int sigaction (int sig, const struct sigaction *new,}
	\\
	\hspace* {.4\linewidth} {\code {struct sigaction *old)}}
    }

    \begin{itemize}
	\item \code{sigaction} defines the action to be taken
	    when the signal \alert{will} occur
	    \begin{itemize}
		\item primitive to call \alert{before} the signal
		    occurs!
	    \end{itemize}
	\item \code{sig}: number of the signal whose action is defined
	\item \code{new}: definition of the new action
	    \begin{itemize}
		\item the action is \alert{copied} into the attributes
		    of the process
		\item the attribute is \alert{inherited} during a \code{fork}
	    \end{itemize}
	\item \code{old}: to get the old action
	    \begin{itemize}
		\item if \code{old == NULL}, the old action is
		    not copied back
	    \end{itemize}
    \end{itemize}
\end {frame}

\begin {frame} {Action associated with a signal -- sigaction}
    An action is described by a \code {struct sigaction}:

    \lst{struct.c} {\fC} {}

    \begin{itemize}
	\item \code{sa\_handler}: address of a function that takes
	    an integer (the signal number) as an argument and returns
	    nothing
	    \begin{itemize}
		\item we cannot change this prototype
		\item two special ``function address'' type values:
		    \begin{itemize}
			\item \code{SIG\_DFL}: default action
			\item \code{SIG\_IGN}: ignore the signal
		    \end{itemize}
	    \end{itemize}

	\item \code{sa\_mask}: signals to be masked
	    execution of the function
	    \begin{itemize}
		\item see below (page~\ref{samask})
	    \end{itemize}

	\item \code{sa\_flags}: for advanced uses
	    \begin{itemize}
		\item unless otherwise agreed, we will always use \code{0}
	    \end{itemize}

    \end{itemize}
\end {frame}

\begin {frame} {Action associated with a signal -- Example}
    \lst{sigaction.c} {\fC} {}

    \begin{casseroleblock}{Errors to avoid}
	Writing ``\code{s.sa\_handler = f(5);}'' is \alert{invalid}
	\\ (calls \code{f} with the argument and puts the result in
	\code{s.sa\_handler})
	\\
	\implique do not confuse \alert{address} and function
	\alert{call}
    \end{casseroleblock}
\end {frame}

\begin {frame} {Action associated with a signal -- sigaction}
    \begin {minipage} [c] {.45\linewidth}
	\hspace*{-2.5em}
	\fig{derout-api}{1.2}
    \end {minipage}%
    \begin {minipage} [c] {.55\linewidth}
	\begin {itemize}
	    \item \code {sigaction} defines the action to do
		when the signal \alert{will} occur
		\begin {itemize}
		    \item reminder: call \code{sigaction} \alert{before}
			the signal occurs
		\end {itemize}
	    \item prototype \code {f} fixed
		\begin {itemize}
		    \item signal number passed as
			argument
		    \item no other argument possible
		\end {itemize}
	\end {itemize}
    \end {minipage}
\end {frame}

\begin {frame} {Action associated with a signal -- sigaction}
    \begin {casseroleblock} {Clock radio analogy}
    \begin {itemize}
	\item \code{sigaction} \alert{does not wait} for the arrival of a signal
	    \begin {itemize}
		\item \code{sigaction} specifies the action to be taken when
		    the signal \alert{will} occur
	    \end {itemize}
	\item such as a clock radio or smartphone
	    \begin {itemize}
		\item one \alert{sets up} the radio station to listen when
		    the alarm clock \alert{will ring}
		\item one \alert{indicates} the song to listen to when
		    the alarm clock \alert{will ring}
	    \end {itemize}
	\item when the alarm goes off, one is interrupted
	    \begin {itemize}
		\item if one was asleep, sleep is interrupted
	    \end {itemize}
    \end {itemize}
    \end {casseroleblock}
\end {frame}

\begin {frame} {Signal function -- Prototype}
    The called function \alert{must} have the following
    prototype:

    \lst{proto.c}{\fD}{}

    Notes:
    \begin {itemize}
	\item it is not possible to change the return type
	\item it is not possible to change arguments

	\item with the \code {gcc} compiler \code {-Wall -Werror}
	    options, one must avoid generating an
	    if the argument is not used:

	    \lst{unused.c}{\fD}{}
    \end {itemize}
\end {frame}

\begin {frame} {Signal function -- Rule of good practice}
    \label{automask}
    Signal handler execution cannot be
    interrupted by the \alert{same} signal

    \begin{itemize}
	\item the signal remains transmitted (in the ``signals on
	    hold'') attribute...
	\item ... but is not immediately processed
	\item it will only be processed when
	    the function will be finished

    \end{itemize}

    \medskip

    Practical consequence: if the function takes too long to execute,
	there may be a risk of \alert{event loss}

    \bigskip

    \begin{casseroleblock} {Rule for the correct use of signals}
	First rule of best signal practices:
	\begin{itemize}
	    \item minimize the duration of the function associated
		to the signal
	\end{itemize}
    \end{casseroleblock}

\end {frame}

\begin {frame} {Signal function -- Recommendations}
    Recommendations for limiting function duration:

    \begin {itemize}
	\item Limit the function to the modification of a global variable
	\item Test the variable in the main program
	\item Use a ``\code {volatile sig\_atomic\_t}'' variable
	    \begin {itemize}
		\item \code {volatile} qualifier: prevent
		    certain inappropriate optimizations
		    \begin{itemize}
			\item if the compiler detects that the variable
			    is not modified, it can ``imagine''
			    that it is a constant

		    \end{itemize}
		\item \code {sig\_atomic\_t} type: assigned variable
		    in a single operation
		    \begin{itemize}
			\item C instructions can be compiled
			    into several assembly instructions
			\item with \code{sig\_atomatic\_t}, assignment
			    is compiled into a single
			    instruction

		    \end{itemize}
	    \end {itemize}
    \end {itemize}

    \bigskip

    Other actions in the function \implique you have to like risk...
\end {frame}

\begin {frame} {Signal function -- Recommendations}
    Example:
    \lst{volatile.c}{\fD}{firstline=4}
\end {frame}


\begin {frame} {Primitive interruption}
    When a primitive is waiting for a resource, it can be
    interrupted by a signal

    \medskip

    \begin{itemize}
	\item examples:
	    \begin{itemize}
		\item \code{read} expects characters on standard input
		\item \code{write} waits until there is room in the pipe
		\item \code{wait} waits for a child process to terminate
		\item etc.
	    \end{itemize}
	\item the primitive ends
	\item the function is executed
	\item the primitive's return code is \code{-1} and
	    \code{errno} equals to \code{EINTR}

    \end{itemize}
\end {frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Concurrence et masquage des signaux
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Signal competition and masking}


\begin {frame} {Asynchronism \implique potential problems}
    Taking a signal into account at any given time
    can lead to problems.

    \medskip

    The program is divided into two parts:
    \begin{itemize}
	\item a \alert{synchronous} part: all functions
	    called from \code{main} or inner function
	    \\
	    \implique execution flow is predictable and controlled (hopefully)
	    by the programmer

	\item an \alert{asynchronous} part: all functions
	    called up when a signal is processed
	    \\
	    \implique execution flow is unpredictable, and can interrupt
	    the rest of the program at potentially any time
    \end{itemize}

    \medskip

    When the asynchronous part interferes with the operation of the
    synchronous part, there is a \alert{competition} problem.

    What problems are involved?

\end {frame}

\begin {frame} {Competition problem}
    Example: incrementing very large numbers ($> 2^{32}$)
    %%% ATTENTION: si on change les lignes, il faut changer l'explication
    %%% dans les transparents qui suivent
    \begin{minipage}[c] {.5\linewidth}
	\lst{compteur.c}{\fE}{lastline=13,numbers=left}
    \end{minipage}%
    \begin{minipage}[c] {.45\linewidth}
	\lst{compteur.c}{\fE}{firstline=14,numbers=left,firstnumber=last}
    \end{minipage}

    \medskip

    Hypotheses:
    \begin{itemize}
	\item we already have incremented a lot...
	\item the current value of \code{c} is \code{\{3, UINT32\_MAX\}}
	    (or $2^{34}-1$)
	\item the program has just finished executing line 8,
	    from \code{main}
	\item the process takes into account the \code{SIGINT} signal
	    at that moment
    \end{itemize}
\end {frame}

\begin {frame} {Competition problem}
    Problem:
    \begin{itemize}
	\item when \code{f} ends, \code{c} is \code{\{4, 0\}}...
	\\ ... and the synchronous part starts again from line 9
	\item so \code{c} is at the end \code{\{5, 0\}}!
    \end{itemize}

    \medskip

    In other words, we used \code{increment} twice, but the
    counter has been incremented by $2^{32}+1$ instead
    of by $2$!

    \bigskip

    The problem is that the synchronous part manipulates a
    data structure that is in an intermediate state when
    the asynchronous part accesses it:

    \begin{itemize}
	\item the synchronous part has made a choice (line 8) based on the
	    the current state of the structure
	\item when the synchronous part reaches line 9, the state of
	    the structure has been modified by \code{f} and the choice is
	    no longer valid
    \end{itemize}

    Solution: \alert{prevent the signal from being taken into account} between 8 and 12.
\end {frame}

\begin {frame} {Competition problem}
    The same problem can be found in several situations:

    \begin{itemize}
	\setlength{\itemsep}{-.9ex}
	\item \lst{concur.c} {\fC} {linerange=1-3}

	\item \lst{concur.c} {\fC} {linerange=5-9}

	\item \lst{concur.c} {\fC} {linerange=11-14}

	\item even in a single instruction as innocuous as
	    \code{n++}!
	    \\
	    \implique the code generated by the compiler often contains
	    several distinct instructions
    \end{itemize}

    Solution: \alert{temporarily prevent the signal from being taken into account}
\end {frame}

\begin {frame} {Masking a signal -- sigprocmask}
    Prevent a signal from being taken into account = \alert{mask} the signal

    \prototype {
	\fC
	\code {int sigprocmask (int comment, const sigset\_t *new, sigset\_t *old)}
    }

    \begin {itemize}
	\item \code {sigprocmask} masks or unmasks signals
	    \begin {itemize}
		\item new process attribute: \alert{signal mask}
		\item bit $s$ = 1 $\Leftrightarrow$ signal $s$ is masked
	    \end{itemize}

	\item during masking, a transmitted signal is not lost
	    \begin {itemize}
		\item it remains in the process ``pending signals''
		    attribute
		\item the signal will be processed when \alert{unmasking}
		\item reminder: only one bit for signal reception
		    \begin{itemize}
			\item \implique signal sent twice: it
			    will only be processed once
		    \end{itemize}
	    \end {itemize}
	\item the mask is specified by \code{new}
	\item get back the old mask with \code{old} (if not null)
	\item possible \code {comment} values:
	    \ctableau {\fD} {|ll|} {
		\code {SIG\_BLOCK}
		    & signals $\in$ \code {new} are added to the current mask
		    \\
		\code {SIG\_UNBLOCK}
		    & signals $\in$ \code {new} are removed from the current mask
		    \\
		\code {SIG\_SETMASK}
		    & current mask $\leftarrow$ \code {new}
		    \\
	    }
    \end {itemize}
\end {frame}

\begin {frame} {Set of signals -- type sigset\_t}
    The \code{sigprocmask} \code{new} and \code{old} arguments
    are of type \code{sigset\_t *}

    \medskip

    \begin{minipage}[c]{.55\linewidth}
	\begin {itemize}
	    \item \code{sigset\_t} = set of signals
	    \item \code {sigset\_t} type = bit fields
	    \item signal $s \in$ the set $\Leftrightarrow$ bit $s$ = 1
	\end {itemize}
    \end{minipage}%
    \begin{minipage}[c]{.45\linewidth}
	\fig{sigset}{1.1}
    \end{minipage}

    \bigskip

    Handling:

    \begin{itemize}
	\item with library functions (cf. manual):
	    \ctableau {\fD} {|ll|} {
		\code {sigemptyset}
		    & empties the set \\
		\code {sigfillset}
		    & fills the set
		    \\
		\code {sigaddset}
		    & adds a signal to the set
		    \\
		\code {sigdelset}
		    & removes a signal from the set
		    \\
		\code {sigismember}
		    & tests if a signal is part of the set
		    \\
	    }

	\item example:

	    \lst{sigset.c}{\fD}{}

    \end {itemize}
\end {frame}

\begin {frame} {Masking a signal -- sigprocmask}
    It is now possible to prevent disturbances from the
    asynchronous part on the synchronous part:

    \lst{sigprocmask.c}{\fD}{}

    \bigskip

    Part of the program where signals are masked (where the
    execution of the asynchronous part is exclued) is called the
	\alert{critical section}
\end {frame}

\begin {frame} {Masking a signal}
    \label{samask}
    Until now: competition between synchronous and asynchronous parts

    \medskip

    \begin{itemize}
	\item
	    there may also be competition problems
	    \alert{between asynchronous parts}:

	    \begin{itemize}
		\item example: functions associated with \code{SIGINT}
		    and \code{SIGHUP} operate on the same data
		    structures

	    \end{itemize}

	\item rather than using \code{sigprocmask} to create a
	    critical section, it is simpler to use the
	    \code{sa\_mask} field of the \code{struct sigaction} associated
	    with the concerned signals:
	    \begin{itemize}
		\item \code{sa\_mask} contains signals
		    to be temporarily masked during
		    the function execution...
		\item ... in addition to the current signal (see
		    page~\ref{automask})
		\item at the end of execution, the current mask
		    is restored
		\item \implique implicit critical sections

	    \end{itemize}
    \end{itemize}
\end {frame}

\begin {frame} {Masking a signal}
    Example:

    \lst{samask.c}{\fE}{}
\end {frame}


\begin {frame} {Process attributes -- Summary}
    Summary of signal-related process attributes

    \bigskip

    Each process stores the following information:

    \begin{itemize}
	\item \alert{action} associated with each signal
	    \begin{itemize}
		\item \code{void (*[]) (int)} type
		    \\
		    (array of pointers to functions, one item
		    per signal)
		\item values:
		    \code{SIG\_DFL},
		    \code{SIG\_IGN} or an address in the process
		\item \code{sigaction} primitive: modifies and/or reads
		    elements of this attribute
	    \end{itemize}
	\item signals \alert{waiting} to be processed
	    \begin{itemize}
		\item \code{sigset\_t} type
		\item \code{sigpending} primitive: read
		    this attribute
		    \begin{itemize}
			\item \code{int sigpending (sigset\_t *val)}
			\item primitive not often used in practice
		    \end{itemize}
	    \end{itemize}
	\item current \alert{masked} signals
	    \begin{itemize}
		\item \code{sigset\_t} type
		\item \code{sigprocmask} primitive: modifies and/or reads
		    this attribute
	    \end{itemize}
    \end{itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Attendre un signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Waiting for a signal}

\begin {frame} {Waiting for a signal -- Active waiting}
    How to \alert{wait} for a signal?

    \medskip

    \alert{Wrong} solution: waiting in an infinite loop

    \begin{itemize}
	\item \lst{att-act.c}{\fE}{}

	\item the process monopolizes processor time
	    to do nothing
	    \begin{itemize}
		\item consumes electricity, heats the
		    planet, not eco-friendly
	    \end{itemize}

	\item it is the \alert{active waiting}, and should be \alert{avoided}
	    at all costs

    \end{itemize}
\end {frame}

\begin {frame} {Waiting for a signal -- Eternal waiting}
    \alert{Correct} solution: ask the kernel to put the process
    to sleep while waiting for a signal
    % \\
    \implique it is the principle of \alert{passive waiting}

    \begin{itemize}
	\item assuming that a \code{wait\_signal} primitive exists:

	    \lst{att-pasv.c}{\fE}{numbers=left}

	\item it cannot work that simply:
	    \begin{itemize}
		\item if the signal is taken into account just after line 1...
		    \\
		    \implique the process goes to sleep when the signal
		    has already occured

		\item simply prevent the signal from arriving
		    between 1 and 3 (that means mask
		    before 1, unmask after 3)...
		    \\
		    \implique no more wake-up calls as the signal
		    is masked

	    \end{itemize}

	\item in all cases: \alert{eternal waiting}
	    \label{attente-eternelle}
	    \implique problem!

    \end{itemize}
\end {frame}

\begin {frame} {Waiting for a signal -- sigsuspend primitive}
    To use \alert{passive waiting}, one needs to use
    the \code{sigsuspend} primitive:

    \prototype {
	\code {int sigsuspend (const sigset\_t *mask)}
    }

    \begin {itemize}
	\item waits for one or more signals
	    \begin{itemize}
		\item process is sleeping
		\item no processor consumption
		\item sustainable computing, good for the planet
	    \end{itemize}

	\item temporarily masks or unmasks (while
	    waiting) unwanted signals using
	    \code{mask}
	    \begin{itemize}
		\item when the process is put back on the processor,
		    the mask is reset to its initial value

		\item allows the critical section to be temporarily ``lifted''
		    and the signal to be taken into account

	    \end{itemize}

	\item warning: do not test the return code of this
	    primitive

	    \begin{itemize}
		\item it always returns \code{-1} because it is
		    is interrupted by a signal...
	    \end{itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Waiting for a signal -- sigsuspend primitive}
    Example:

    \lst{sigsuspend.c}{\fE}{numbers=left}

    \code{sigsuspend} is \alert{the} solution to wait for a signal

    \begin{itemize}
	\item passive waiting $\Leftrightarrow$ certainty of an
	    event at every stage
	    \begin{itemize}
		\item in this example: each step = each turn of the loop
		\item if we get to line 11, we are sure to have
		    received the signal
		    \\
		    \implique while waiting if needed
	    \end{itemize}
    \end{itemize}
\end {frame}

\begin {frame} {Waiting for a signal}
    \begin{casseroleblock}{Bug indicators}
	The \code{sigsuspend} primitive is \alert{the} solution to
	wait for a signal

	\medskip

	Do not believe to both \alert{wrong} solutions:
	\begin{itemize}
	    \item make an infinite loop without using passive waiting

		\implique bad for the planet

	    \item use the obsolete \code{pause} primitive

		\implique risk of eternal waiting
		    (cf. page~\pageref{attente-eternelle})

	\end{itemize}

	Both are \alert{bug presence indicators}...

    \end{casseroleblock}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minuteries de processus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Process timers}

\begin {frame} {Process timers}
    Need to emit a signal on a given date?
    \\
    \implique several primitives, from the simplest to the most complex

    \begin{itemize}
	\item \code{unsigned int alarm (unsigned int s)}

	    \begin{itemize}
		\item caues \code{SIGALRM} to be emitted
		    in \code{s} seconds
		\item only one possible timer
		    \begin{itemize}
			\item if \code{s} = 0, the timer is deactivated
		    \end{itemize}
	    \end{itemize}

	\item \code{setitimer}

	    \begin{itemize}
		\item microsecond precision
		\item several ways of counting time
		    \begin{itemize}
			\item real time, monotonic time, processor time
		    \end{itemize}
		\item causes \code{SIGALRM}, \code{SIGVTALRM}
			or \code{SIGPROF} to be emitted
		\item cf. manual
	    \end{itemize}

	\item \code{timer\_create}, \code{timer\_settime}, \code{timer\_delete}, etc.

	    \begin{itemize}
		\item nanosecond precision
		\item very general primitives
		\item causes the selected signal to be emitted
		\item cf. manual
	    \end{itemize}

    \end{itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fork et signaux
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Fork and signals}

\begin {frame} {Fork and signals -- Heritage}
    When \code{fork} is called, the child inherits certain attributes
    from the parent:

    \ctableau {\fC} {|cc|} {
	\textbf{Attribute} & \textbf{Inherited} \\
	\hline
	actions associated with signals & yes \\
	current signal mask & yes \\
	waiting signals & no (reset to 0) \\
    }
\end {frame}



\begin {frame} {Fork and signals -- Trap}

    \begin{minipage}[c]{.6\linewidth}
	Beware of \code{fork} signals:
    \end{minipage}%
    \begin{minipage}[c]{.35\linewidth}
	\fig {piege}{}
    \end{minipage}

    \begin {itemize}
	\item the parent sends a signal to the child \implique the child
	    must get ready

	    \begin {itemize}
		\item impossible because \code{fork} is non-deterministic:
		    the parent can be put back on the processor before
		    the child had time to start up
		\item only solution: prepare for signal reception
		    \alert{in the parent} (before \code{fork}) in order to
		    \alert{inherit} action in the child
	    \end {itemize}

	\item similar problem if the child sends a signal to the parent:

	    \begin {itemize}
		\item if the parent prepares after \code{fork}, the
		    child may start too quickly!
		\item solution: the parent must prepapre \alert{before}
		    the call to \code{fork}

	    \end {itemize}
    \end {itemize}
    \implique it's important to know who sends the signal first 
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Anciennes primitives
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Old primitives}

\begin {frame} {Old primitives}
    Old primitives remain from the early days of Unix:

    \begin{itemize}
	\item \code{void (*signal (int, void (*) (int))) (int)}
	     \\
	     \implique replaced by \code{sigaction}

	\item \code{int break (void)}
	     \\
	     \implique replaced by \code{sigsuspend}
    \end{itemize}

    \bigskip

    They still exist and are standardized by POSIX, but:

    \begin{itemize}
	\item their use is \alert{not recommended}
	\item they are \alert{insufficient} for real cases
	\item they are just handy for ``quick and dirty'' programs
    \end{itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Analogie avec les interruptions matérielles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Analogy with hardware interrupts}

\begin {frame} {Analogy with hardware interrupts}
    Hardware mechanism:

    \begin {minipage} [c] {.4\linewidth}
	\fig{bus}{}
    \end {minipage}%
    \begin {minipage} [c] {.6\linewidth}
	\begin {itemize}
	    \fC
	    \item when a controller has completed a request,
		it activates the interrupt line on the control bus

	    \item when the processor finishes executing the
		current instruction, it checks the interrupt line

	    \item if it is in the ``active'' state, the processor
		interrupts the current program

	    \item controller remains ``interruptive'' until
		it is interrogated by the processor
	\end {itemize}
    \end {minipage}

    \medskip

    Three processor registers involved:
    \ctableau {\fC} {|ll|} {
	PC & Program Counter (ordinal counter) \\
	SP & Stack Pointer (stack pointer) \\
	SR & Status Register (status register) \\
    }

\end {frame}

\begin {frame} {Analogy with hardware interrupts}
    Processor actions after an interrupt:
    \begin {enumerate}
	\item when the interrupt occurs, PC points the 
	    process code, SP the process stack and SR
	    indicates the ``non-privileged'' mode (for example)

	\item the processor saves these registers

	\item the processor then modifies these registers:
	    \begin {itemize}
		\item SR:
		    \begin {itemize}
			\item switch to ``privileged'' mode
			\item interrupt blocking (masking)
		    \end {itemize}

		\item PC: initialized from the interrupt vector

		\item SP: points to the kernel stack
	    \end {itemize}

    \end {enumerate}

    \medskip

    \implique all this is done by the hardware
\end {frame}

\begin {frame} {Analogy with hardware interrupts}
    Interrupt vector:

    \begin {itemize}
	\item kernel-internal function address table
	\item located at a fixed address for the processor
	\item indexed by the interrupt number
	    \begin {itemize}
		\item example: keyboard interrupts,
		    disk interrupts, etc.
	    \end {itemize}
	\item initialized by the kernel at system startup
    \end {itemize}

\end {frame}

\begin {frame} {Analogy with hardware interrupts}
    Interrupt masking:

    \begin {itemize}
	\item prevents the processor from checking interrupt line
	\item selective mechanism (depending on hardware)
	    \begin {itemize}
		\item example: mask what is less important
		    than the current interrupt

		\item implicit masking of current interrupt

	    \end {itemize}

	\item masking \implique controller remains ``interruptive''
	\item benefit: prevent the kernel from modifying a data structure
	    altered by interrupt processing
    \end {itemize}
\end {frame}

\begin {frame} {Analogy with hardware interrupts}
    \begin {center}
	\fig{ps-except}{}
    \end {center}
\end {frame}

\begin {frame} {Analogy with hardware interrupts}
    Once the context (PC, SP, SR) has been initialized, the processor executes
    the kernel code:

    \begin {enumerate}
	\item (in assembler) saves the rest of the CPU
	    context (general registers, etc.)

	\item (in assembler) setting up a stack context
	    for a procedure call in a high-level language (e.g. C)

	\item (in assembler) connection to an address

	\item (in C) check reason for interrupt

	    \begin {itemize}
		\item interrogation of device controllers
		    to identify the origin of the interrupt
	    \end {itemize}

	\item (in C) action corresponding to the interrupt

    \end {enumerate}
\end {frame}

\begin {frame} {Analogy with hardware interrupts}
    On the way back:

    \begin {itemize}
	\item symmetrical software actions at the end of the exception (in
	    C then assembler)

	\item actions (in hardware) symmetrical to the consideration
	    of the exception: special instruction (IRET for x86,
	    RTE for 68000)

    \end {itemize}
\end {frame}

\begin {frame} {Analogy with hardware interrupts}
    Summary:

    \ctableau {\fC} {|lcc|} {
	\multicolumn{1}{|p{.2\linewidth}}{~}
	    & \multicolumn{1}{p{.3\linewidth}}{\centering\textbf {Interrupts}}
	    & \multicolumn{1}{p{.3\linewidth}|}{\centering\textbf {Signals}}
	    \\ \hline
	Level & Hardware & Software \\
	Transmitter & Device & Kernel \\
	Receiver & Processor (kernel) & Process \\
	Masking & Yes & Yes \\
    }
\end {frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bilan
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Summary}

\begin {frame} {Summary}

    Each process has 3 attributes:
    \begin {itemize}
	\item table of actions associated with each signal
	\item signal masking
	\item waiting signals
    \end{itemize}

    \bigskip

    Two-stage mechanism:

    \begin{itemize}
	\item signal transmission
	    \begin{itemize}
		\item set a bit to 1 in the ``waiting
		    signals'' attribute of the destination process
	    \end{itemize}
	\item the signal is taken into account by the receiving process
	    \begin{itemize}
		\item if the signal is not masked, or when the
		    signal is unmasked
		    \begin{itemize}
			\item add \code{sa\_mask} and the current signal at the current mask
			\item execute the function
			\item put back the original mask
		    \end{itemize}
	    \end{itemize}
    \end{itemize}
\end {frame}

\begin {frame} {Recommendations}
  
    \begin {casseroleblock} {Recommendations / reminders}
    \begin {itemize}
	\item \code {sigaction} \textbf {does not wait} the arisal of a signal
	    \begin {itemize}
		\item simply defines the action associated with the signal
	    \end {itemize}
	\item do \textbf {as little as possible} in the function
	    \begin {itemize}
		\item avoids competition problems
	    \end {itemize}
	\item active waiting is \textbf{prohibited}
	    \begin {itemize}
		\item use \code{sigsuspend} to save the planet
	    \end {itemize}
	\item beware of competition problems
    \end {itemize}
    \end {casseroleblock}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% API POSIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

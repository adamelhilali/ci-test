DIR *dp ;
struct dirent *d ;

dp = opendir ("/tmp") ;
if (dp == NULL)
    grumble ("opendir") ;

while (errno = 0, (d = readdir (dp)) != NULL)
{
    if (strcmp (d->d_name, ".") != 0
	&& strcmp (d->d_name, "..") != 0)
    {
	printf ("%ju %s\n", (uintmax_t) d->d_ino, d->d_name) ;
    }
}
if (errno != 0)
    grumble ("readdir") ;

if (closedir (dp) == -1)
    grumble ("closedir") ;

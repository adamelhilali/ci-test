FILE *fp1, *fp2 ;
int c ;

fp1 = fopen ("foo", "r") ;
if (fp1 == NULL)
    grumble ("fopen foo") ;
fp2 = fopen ("bar", "w") ;
if (fp2 == NULL)
    grumble ("fopen bar") ;

while ((c = getc (fp1)) != EOF)
    if (putc (c, fp2) == EOF)
	grumble ("putc") ;
if (ferror (fp1))
    grumble ("getc") ;

if (fclose (fp1) == -1)
    grumble ("fclose foo") ;
if (fclose (fp2) == -1)
    grumble ("fclose bar") ;

# Date displayed by default : last data modification 
> ls -l ch1-intro.tex
-rw-r--r-- 1 pda users 378 Jan 22  2019 ch1-intro.tex

# Date displayed with \texttt{-c} : last attribute modification 
> ls -lc ch1-intro.tex
-rw-r--r-- 1 pda users 378 Jan  9 19:20 ch1-intro.tex

# Date displayed with \texttt{-u} : last file access (= last \textbf{u}se)
> ls -lu ch1-intro.tex
-rw-r--r-- 1 pda users 378 Jul  8 18:37 ch1-intro.tex

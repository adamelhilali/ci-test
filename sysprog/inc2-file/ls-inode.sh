> ls -a -i /		# \texttt{-a} : also displays entries starting with «~\texttt{.}~»
2 .
2 ..
3 etc
8 usr
...

> ls -ai /usr
8 .
2 ..
9 bin
10 include
...

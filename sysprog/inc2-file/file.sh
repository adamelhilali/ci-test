# Guesses the contents of these two files...
> file ch2-file.tex ch2-file.pdf
ch2-file.tex: LaTeX 2e document, Unicode text, UTF-8 text
ch2-file.pdf: PDF document, version 1.5

# The first 10 bytes of the \texttt{ch2-file.pdf} file in hexadecimal (and in characters)
> od -t xCc -N 10 ch2-file.pdf
0000000  25  50  44  46  2d  31  2e  35  0a  25
          %   P   D   F   -   1   .   5  \n   %

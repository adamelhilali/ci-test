time_t hour ;
struct tm *tm ;
char *s1, s2 [MAX] ;
size_t n ;

hour = time (NULL) ;

tm = localtime (&hour) ;

s1 = asctime (tm) ;
printf ("now: %s\n", s1) ;

n = strftime (s2, sizeof s2, "the %d/%m/%Y at %H:%M:%S", tm) ;
if (n == 0)
    printf ("s2 is too small\n") ;
else printf ("now: %s\n", s2) ;

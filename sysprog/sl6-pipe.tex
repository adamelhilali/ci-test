\def\inc{inc6-pipe}

\titreA {Pipes}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Introduction}
    Tubes are one of Unix's major innovations

    \begin {itemize}
	\item Shell example: \code {\$ ls -l | wc -l}

	    \begin {center}
		\fig{principe}{.8}
	    \end {center}

	\item the \code {ls} process writes to the pipe
	    \begin {itemize}
		\item use of the \code {write} primitive
		\item if \code {ls} writes too fast (\implique full pipe),
			\code {write} waits
	    \end {itemize}
	\item the \code {wc} process reads from the pipe
	    \begin {itemize}
		\item use of the \code {read} primitive
		\item if \code {wc} reads too fast (\implique empty pipe),
		    \code {read} waits
	    \end {itemize}
	\item both processes run in parallel
	    \begin {itemize}
		\item implicit synchronization
	    \end {itemize}
	\item no limitation on the amount of data transferred
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Création des tubes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Pipe creation}

\begin {frame} {Pipe creation}
    \prototype {
	\code {int pipe (int pipefd [2])}
    }

    \begin {itemize}
	\item creation of the pipe and two descriptors
	\item after \code {pipe} call:
	    \begin {center}
		\fig{creation-0}{.7}
	    \end {center}
	\item reading via \code {pipefd [0]}
	\item writing via \code {pipefd [1]}
	\item utility: with \code {fork}...
    \end {itemize}
\end {frame}


\begin {frame} {Example [1/6]}
    \begin {center}
	\fig{creation-1}{}
    \end {center}
\end {frame}

\begin {frame} {Example [2/6]}
    \begin {center}
	\fig{creation-2}{}
    \end {center}
\end {frame}

\begin {frame} {Example [3/6]}
    \begin {center}
	\fig{creation-3}{}
    \end {center}
\end {frame}

\begin {frame} {Example [4/6]}
    \begin {center}
	\fig{creation-4}{}
    \end {center}
\end {frame}

\begin {frame} {Example [5/6]}
    \begin {center}
	\fig{creation-5}{}
    \end {center}
\end {frame}

\begin {frame} {Example [6/6]}
    \begin {center}
	\fig{creation-6}{}
    \end {center}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Règles de fonctionnement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Operating rules}

\begin {frame} {Operating rules [1/3]}
    Special rules for pipe reading (\code {read}):

    \begin {itemize}
	\item Blocking \code {read} primitive
	    \begin {itemize}
		\item \code {read} returns 0 when the pipe is empty and
		    no writer
	    \end {itemize}

	\item Partial readings
	    \begin {itemize}
		\item \code {read} returns what is available in the pipe
		\item probably less than required
		\item \implique be prepared for \code
		    {read} to return less than requested
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Operating rules [2/3]}
    Special rules for pipe writing (\code {write}):

    \begin {itemize}
	\item Partial writing
	    \begin {itemize}
		\item \code {write} is limited by maximum pipe size
		\item variable maximum size (even on the same system)
		\item if one runs out of space, \code{write} waits
		\item \implique no partial writing
		\item \code{write} should normally write what is
		    required \\
		    (unless interrupted by a signal or an
		    abnormal condition)

	    \end {itemize}

	\item Writing in a pipe without a reader
	    \implique problem!
	    \begin {itemize}
		\item returning \code {-1} is not enough
		    \begin {itemize}
			\item poorly written programs do not
			    test errors...
		    \end {itemize}
		\item \code {SIGPIPE} signal sent \implique
		    process end
		\item example: \code {\$ find / | ./a.out}
		    \begin {itemize}
			\item ``premature'' shutdown of \code {a.out} \implique
			    automatic kill \code {find} process
			    without continuing to generate unnecessary data
		    \end {itemize}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Operating rules [3/3]}
    Special rules for pipe sharing:

    \begin {itemize}
	\item There can be several readers and several writers
	    \begin {itemize}
		\item ``normal'' situation (example: right after \code {fork})
		\item beware of ``end-of-file'' detection
		    \begin {itemize}
			\item no more writers...
			\item \implique close descriptors as soon
			    they are no longer in use
		    \end {itemize}
	    \end {itemize}

	\item Simultaneous writing by several writers
	    \begin {itemize}
		\item size $\leq$ \code {PIPE\_BUF}: no mixing
		    between writers
		\item size $>$ \code {PIPE\_BUF}: possible mix
		    of writers
		\item POSIX specifies that \code{PIPE\_BUF} is at least
		    512 bytes
		    \begin {itemize}
			\item \code {PIPE\_BUF} = 512 (FreeBSD) or 4096 (Linux)
		    \end {itemize}
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Do not forget...}
    \begin {casseroleblock} {Principles to respect}

    \begin {itemize}
	\item \code {pipe} call must be made \alert {before}
	    \code {fork} call
	    \begin {itemize}
		\item otherwise the pipe is not inherited
	    \end {itemize}
	\item \alert {close} descriptors as soon as they are
	    no longer in use
	    \begin {itemize}
		\item essential in certain cases with pipes
		\item \implique doing it systematically avoids having
		    to think!
	    \end {itemize}
    \end {itemize}
    \end {casseroleblock}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tubes nommés
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Named pipes}

\begin {frame} {Named pipes}
    \prototype {\code {int mkfifo (const char *path, mode\_t mode)}}

    \begin {itemize}
	\item Primitive \code {pipe}: creates an anonymous pipe
	    \begin {itemize}
		\item must be created by a common ancestor of
		    processes
		\item inheritance of opening descriptors
	    \end {itemize}
	\item Named pipes: file name in tree structure
	\item New file type: ``\textit {fifo}''
	    \begin {itemize}
		\item with \code {stat}: \code {S\_IFIFO} and
		    \code {S\_ISFIFO()}
	    \end {itemize}
	\item Creation with \code {mkfifo}
	    \begin {itemize}
		\item then acess with \code {open}, \code {read},
		    \code {write}, and \code {close}
		\item \implique as with any regular file
	    \end {itemize}
	\item Operating rules: cf. anonymous pipes
	    \begin {itemize}
		\item ... after the start-up of a reader and writer
		\item \code {read} or \code {write} blocked while waiting
		    the other part
	    \end {itemize}
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redirections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Redirections}

\begin {frame} {Redirections}

    \begin {itemize}
	\item How to redirect input or ouput with a pipe?
	    \begin {itemize}
		\item example: \code {\$ ls -l | wc -l}
	    \end {itemize}

	\item Similar principle to classic redirections
	    \begin {enumerate}
		\item create an anonymous pipe before the call to \code {fork}
		\item in the writer process (here \code {ls}):
		    \begin {enumerate}
			\item \code {dup2 (tube [1], 1)}
			\item \code {close (tube [0])}
			\item \code {close (tube [1])}
		    \end {enumerate}
		\item in the reader process (here \code {wc}):
		    \begin {enumerate}
			\item \code {dup2 (tube [0], 0)}
			\item \code {close (tube [0])}
			\item \code {close (tube [1])}
		    \end {enumerate}
	    \end {enumerate}
    \end {itemize}
\end {frame}

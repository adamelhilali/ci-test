\def\inc{inc4-ps}

\titreA {Processes}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Process definition (high level)}
    Definition (high level):

    \begin {quote}
	a process is an instance of a running program
    \end {quote}

    ... but not just any execution:

    \begin {itemize}
	\item I execute \code {ls /tmp}: the process corresponds to
	    the execution of the \code {ls} program with the
	   \code {/tmp} data

	\item someone else runs \code {ls /tmp} at the same time:
	    it is not the same execution, even if it is the
	    same program and data

	    \begin {itemize}
		\item It is not the same process
		\item even if ``someone else'' is me
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Process attributes}
    A process has attributes:

    \begin {itemize}
	\fB
	\item status (ready to run, waiting, etc.)
	\item process identifier (pid)
	\item parent process identifier (ppid)
	\item owner (uid), group (gid)
	\item file openings
	\item current directory
	\item control terminal
	\item memory location
	\item CPU time consumption
	\item etc.
    \end {itemize}

\end {frame}

\begin {frame} {Process definition (low-level)}
    Definition (low level):

    \begin {quote}
	a process is described by:
	\begin {itemize}
	    \fB
	    \item a memory space for program and data
	    \item attributes
	    \item a hardware context
		\begin {itemize}
		    \fC
		    \item processor registers
		    \item address translation
			($\Leftrightarrow$ usable part of the memory)
		\end {itemize}
	\end {itemize}
    \end {quote}

    { \fB (see \code {task\_struct} on
	\url {https://www.tldp.org/LDP/tlk/ds/ds.html} )}

    \medskip

    Each time:
    \begin {itemize}
	\fB
	\item a process is removed from the processor
	    \begin {itemize}
		\fC
		\item its context is saved (memory space, processor registers
		    registers, etc.)
	    \end {itemize}
	\item a process is placed on the processor
	    \begin {itemize}
		\fC
		\item its context is restored
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Process memory space}
    The process memory space is divided into 3 zones:

    \medskip

    \begin {minipage} [c] {.3\linewidth}
	\fig{ps-mem}{}
    \end {minipage}%
    \begin {minipage} [c] {.7\linewidth}
	\begin {itemize}
	    \fB
	    \item ``text'' segment
		\begin {itemize}
		    \fC
		    \item program (compiled code)
		    \item address 0 not used: why?
		\end {itemize}
	    \item ``data'' segment
		\begin {itemize}
		    \fC
		    \item global variables (+ local \code {static})
		    \item stack (memory allocated by \code {malloc})
		    \item explicit extension (using \code {malloc})
		\end {itemize}
	    \item ``stack'' segment: the execution stack
		\begin {itemize}
		    \fC
		    \item local variables
		    \item function arguments
		    \item return addresses
		    \item implicit extension (stack use)
		\end {itemize}
	    \item other zones can be added
		\begin {itemize}
		    \fC
		    \item shared libraries
		    \item memory shared between processes
		    \item \implique cf. course on Operating Sytems architecture
		\end {itemize}
	\end {itemize}
    \end {minipage}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gestion des attributs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Attributes management}

\begin {frame} {Identity}
    \prototype {
	\code {pid\_t getpid (void)} 
	\hspace* {10mm}
	\code {pid\_t getppid (void)} \\
	\code {uid\_t getuid (void)}
	\hspace* {10mm}
	\code {gid\_t getgid (void)}
    }

    \begin {itemize}
	\item Simple primitives...
	\item Do not return -1 on error (no error possible)
	\item Example:
	    \lst{getpid.c}{\fD}{}
    \end {itemize}
\end {frame}

\begin {frame} {Identity}
    \prototype {
	\code {int setuid (uid\_t uid)}
	\hspace* {10mm}
	\code {int setgid (gid\_t gid)}
    }

    \begin {itemize}
	\item Primitives restricted to the administrator (uid = 0)

	\item Used for admission to the system:
	    \begin {itemize}
		\item \code {/bin/login}, \code {sshd} or equivalent for
		    X-Window
		\item launched by process number 1 or one of its
		    descendants
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Identity}

    System admission algorithm:
    \begin {enumerate}
	\item request login and password
	\item search entry in \code {/etc/passwd}
	    \begin {itemize}
		\item lines with the format: \\
		    {\fD \code {foo:salt+encrypted-password:uid:gid:name:directory:shell}}
		\item salt and encrypted password are stored in a
		    separate file (\code {/etc/shadow} on Linux)
	    \end {itemize}
	\item encrypt the password (with the ``salt'' mentioned
	    in the entry)
	\item compare password with input
	\item if identical
	    \begin {itemize}
		\item generate a new process
		\item \code {setuid (uid)} / \code {setgid (gid)}
		    in this new process
		\item run the shell (or ``window manager'')
	    \end {itemize}
    \end {enumerate}
\end {frame}

\begin {frame} {File creation mask}
    \prototype {
	\code {mode\_t umask (mode\_t mask)}
    }

    \begin {itemize}
	\item File creation (\code {open (}... \code
	    {O\_CREAT}...\code {)}, \code {mkdir} or \code {mknod})

	    \begin {itemize}
		\item permissions of the created file = mode \code {\& \~{}} umask
		\begin {center}
		    \fig{umask}{.7}
		\end {center}
	    \end {itemize}

	\item Recommendation: in programs \implique 0666 or 0777
	    \begin {itemize}
		\item the programs are general
		\item let users manage their privacy level
		    \begin {itemize}
			\item Shell \code {umask} command
		    \end {itemize}
		\item unless there are special security concerns
	    \end {itemize}

	\item \code {umask} returns the old mask (before modification)

    \end {itemize}
\end {frame}

\begin {frame} {Current directory}
    \prototype {
	\code {int chdir (const char *path)}
    }

    \begin {itemize}
	\item Modifies the current process directory
	\item Reminder: the current directory is a process attribute
	    \begin {itemize}
		\item changes in one process do not affect others
		    processes
	    \end {itemize}

	\item No system primitive to retrieve the name
	    of current directory

	    \begin {itemize}
		\item it is a library function
		    \prototype {
			\code {char *getcwd (char *buf, size\_t max)}
		    }
		\item how does it work?
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Current root}
    \prototype {
	\code {int chroot (const char *path)}
    }

    \begin {itemize}
	\item Modifies the current process root
		\begin {center}
		    \fig{chroot}{.9}
		\end {center}

	\item No bypass possible
	    \begin {itemize}
		\item to the new root, ``\code {..}'' remains in
		    the same place
		\item you can only go down the tree structure
	    \end {itemize}
	\item Accessible only to system administrator
	\item Primitive not standardized by POSIX
    \end {itemize}
\end {frame}

\begin {frame} {Current root}
    \code {chroot} is a containment system:

    \begin {itemize}
	\item restrict the environment to certain files
	    only
	    \begin {itemize}
		\item example: specific user accounts
		    to only access an application
		\item example: ``anonymous FTP'' server \\
		    FTP service for distributing files, restricted
		    to a portion of the tree structure

	    \end {itemize}

	\item \code {chroot}: foundations of modern ``container'' systems
		\begin {itemize}
		    \item LXC on Linux, Jails on FreeBSD
		    \item complemented by other containment systems
			\begin {itemize}
			    \item restricted process visibility
			    \item restricted visibility of network connections
			    \item etc.
			\end {itemize}
		    \item low-cost ``virtual'' machines
		\end {itemize}

    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Création des processus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Processes creation}

\begin {frame} {Processes creation}
    \prototype {
	\code {pid\_t fork (void)}
    }

    Create a process $\Longleftrightarrow$ duplicate the process

    \begin {itemize}
	\item \code {fork} primitive = process photocopier
	\item Process = memory + attributes + hardware context
	    \begin {itemize}
		\item (almost) everything is duplicated
		\item duplicated memory \implique duplicated variables
		\item CPU registers too: each program
		    will evolve (PC register) separately
	    \end {itemize}
	\item Duplication \implique information \textbf {inherited}
	    from the parent process
	    \begin {itemize}
		\item current directory, file openings, etc.
		\item except for a few attributes: pid, ppid, CPU time, etc.
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Processes creation}
    \begin {minipage} [c] {.3\linewidth}
	\lst{fork.c}{\fD}{}
    \end {minipage}%
    \begin {minipage} [c] {.7\linewidth}
	\fig{fork}{1.1}
    \end {minipage}

    \medskip

    After \code {fork}: \code {x} and \code {v} variables
    evolve separately
\end {frame}

\begin {frame} {Processes creation}
    \begin {itemize}
	\item Particularity of \code {fork}:
	    \begin {itemize}
		\item one call, two returns
		    \begin {itemize}
			\item as \code {fork} duplicates the process,
			    for the second, everything happens as if he
			    had called \code {fork} himself
		    \end {itemize}

	    \end {itemize}
	\item \code {fork} return value:
	    \begin {itemize}
		\item -1 in case of error (classic)
		\item 0 for the child process
		\item a value > 0 for the parent process
		    \begin {itemize}
			\item it is the child's pid
		    \end {itemize}
	    \end {itemize}
	\item No determinism:
	    \begin {itemize}
		\item after \code {fork}, it is not known whether the parent or
		    child is put back on the processor first
		    \\
		    \implique depends on process scheduling
		\item not a problem: processes evolve independently
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Processes creation}
    Back to the photocopier analogy

    \bigskip

    \begin {minipage} [c] {.55\linewidth}
	After passing through the photocopier:
	\begin{itemize}
	    \item the photocopied sheet is separate from the original
	    \item what is written on each sheet is identical
		at the beginning
	    \item you can then write on a sheet of paper,
		it does not change the other
	    \item to distinguish the sheets, a marker is required
		(the return value of \code{fork})
	\end{itemize}
    \end {minipage}%
    \begin {minipage} [c] {.45\linewidth}
	\fig{photocop}{1.1}
    \end {minipage}%
\end {frame}

\begin {frame} {Processes creation}
    In practice, the \code{fork} primitive is of interest only if we distinguish
    what the parent should do and what the child should do

    \medskip

    The return code must be tested:

    \begin{itemize}
	\item if it is \code{-1} \implique error
	\item if it is \code{0} \implique we are in the child
	\item if it is neither \code{-1}, nor \code{0} \implique we are in the parent
    \end{itemize}
\end {frame}

\begin {frame} {Processes creation}
    \begin {itemize}
	\item What is the mnemonic for the return value?
	    \begin {itemize}
		\item Each process knows its parent
		    \begin {itemize}
			\item ppid attribute, \code {getppid} primitive
			\item easy-to-find information
			\item \implique no need to retrieve
			    the parent's pid with \code {fork}
			\item \code {fork} returns 0 for the child
		    \end {itemize}

		\item No easy way to retrieve child's pid
		    \begin {itemize}
			\item there can be many, which one to send back?
			\item no attribute, no primitive
			\item \implique the only way to retrieve the pid of the
			    child = \code {fork}
			\item \code {fork} returns the child's pid to the parent
		    \end {itemize}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Processes creation}
    \begin {casseroleblock} {Tips for taming \code {fork}}

    \begin {itemize}
	\item \code {switch} with 3 cases: \code {-1}, \code {0} and
	    \code {default}

	    \begin {itemize}
		\item to make sure to not miss any case
	    \end {itemize}
	\item in case 0 (child), call a \code {child} function
	    and end with \code {exit}
	    \begin {itemize}
		\item isolate the code from the child
		\item set up a ``sanitary cordon'' to prevent the child
		    from executing the code intended for the parent
	    \end {itemize}
    \end {itemize}
    \end {casseroleblock}
\end {frame}

\begin {frame} {Processes creation}
    Example:

    \lst{ex-fork.c}{\fC}{}
\end {frame}

\begin {frame} {Process completion}
    \prototype {
	\code {void exit (int code)}
    }

    \begin {itemize}
	\item End the current process
	\item No return
	    \begin {itemize}
		\item ... and therefore not -1 in case of error
	    \end {itemize}
	\item Almost all resources are released
	    \begin {itemize}
		\item memory, CPU, etc.
		\item special case for zombie processes (more on this later)
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Process completion}

    \begin {casseroleblock} {\code {exit} argument}
    \begin {itemize}
	\item code $\in$ [0..255]
	    \begin {itemize}
		\item if you see \code {\alert {exit (-1)}} in a
		    program, is that its author has not assimilated
		    her system course...
	    \end {itemize}
	\item value 0: ok
	    \begin {itemize}
		\item convention used by shells
		\item if the parent is not a shell, we can
		    use another convention
	    \end {itemize}
	\item POSIX constants: \code {EXIT\_SUCCESS} and \code
	    {EXIT\_FAILURE}

    \end {itemize}
    \end {casseroleblock}

    Note: POSIX constant \code{EXIT\_FAILURE} unsuitable \\
    {\fB (\implique for shells, any value $\neq$
    \code{EXIT\_SUCCESS} corresponds to a failure)}


\end {frame}

\begin {frame} {Process completion}
    In reality, \code {exit} is a library function
    \begin {itemize}
	\item empties the buffers of files opened by \code {fopen}
	\item calls up the functions registered by \code {atexit}
	\item the real primitive is called \code {\_exit}
	    \begin {itemize}
		\item never called directly
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Process completion}
    \prototype {
	\code {pid\_t wait (int *status)} \\
	\code {pid\_t waitpid (pid\_t pid, int *status, int options)} \\
    }

    \vspace* {-3ex}

    \begin {itemize}
	\item Waits for one of the child processes to terminate
	    \begin {itemize}
		\item \code {wait} waits for any child
		    \begin {itemize}
			\item if a child process has already finished,
			    no waiting
			\item if no child process, returns -1
			\item if there is at least one child process, wait
			\item if waiting interrupted by a signal, returns -1
		    \end {itemize}
		\item \code {waitpid} waits for a specific process
		    (see manual)
	    \end {itemize}
	\item Termination can have several causes:
	    \begin {itemize}
		\item the child calls \code {exit}
		\item the child receives a signal
		    \begin {itemize}
			\item \framebox {\fD CTRL} \framebox {\fD C}, segment
			    violation, etc.
		    \end {itemize}
		\item it can be something other than an ending
		    \begin {itemize}
			\item processes that have reached a breakpoint with
			    a debugger
		    \end {itemize}

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Process completion}
    \begin {itemize}
	\item Integer pointed by \code {status}: reason for termination

	\item Example of coding several items of information:

	    \ctableau {\fE} {|p{.2\linewidth}p{.2\linewidth}p{.2\linewidth}p{.2\linewidth}|} {
		\multicolumn {1}{|c}{\textbf {}} & 
		    \multicolumn {1}{c}{\textbf {return code}} &
		    \multicolumn {1}{c}{\textbf {high-order}} &
		    \multicolumn {1}{c|}{\textbf {low-order}}
		    \\
		    \hline
		process stopped in trace mode &
		    \centering pro\-cess identifier &
		    \centering signal number &
		    \multicolumn {1}{c|}{0177}
		    \\
		process terminated by exit &
		    \centering pro\-cess identifier &
		    \centering 8-bit exit argument &
		    \multicolumn {1}{c|}{0}
		    \\
		process terminated by signal &
		    \centering pro\-cess identifier &
		    \centering 0 &
		    signal number (+0200 if core)
		    \\
		wait interrupted by signal &
		    \multicolumn {1}{c}{-1} &
		    \multicolumn {1}{c}{?} &
		    \multicolumn {1}{c|}{?}
		    \\
	    }

	    \smallskip

	    Warning: coding not standardized by POSIX

	\item POSIX makes work easier: the most common macros

	    \ctableau {\fC} {|ll|} {
		Stop with \code {exit}? & \code{WIFEXITED()} \\
		\implique if yes, return code & \code{WEXITSTATUS()} \\
		Stop on signal? & \code{WIFSIGNALED()} \\
		\implique if yes, signal number & \code{WTERMSIG()} \\
	    }
    \end {itemize}
\end {frame}

\begin {frame} {Process completion}
    Example:

    \lst{ex-wait.c}{\fD}{}
\end {frame}

\begin {frame} {Special case -- Zombie process}
    Definition:

    \begin {quote}
	A zombie process is a terminated process whose
	parent has not yet registered the termination with wait
    \end {quote}

    \begin {itemize}
	\item a zombie process is ``almost'' complete
	    \begin {itemize}
		\item almost all its resources are released...
		\item ... except the process descriptor
		    \begin {itemize}
			\item it contains the reason for the ending
			\item as well as a summary of
			    resources
		    \end {itemize}
		\item unlimited duration
		\item it remains visible with \code {ps}
	    \end {itemize}


	\item when the parent uses \code {wait}:

	    \begin {itemize}
		\item it collects the necessary information
		\item the process descriptor is released
		    \begin {itemize}
			\item the process completely disappears
			    from the system
		    \end {itemize}

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Special case -- Orphan process}
    What happens when the parent of a process terminates?

    \medskip

    \begin {minipage} [c] {.25\linewidth}
	\fig{orphan}{}
    \end {minipage}%
    \begin {minipage} [c] {.75\linewidth}
	\begin {itemize}
	    \item parent ends \implique zombie
	    \item the child is ``reparented''
		    (ppid $\leftarrow$ 1)
	    \item process 1 is special
		\begin {itemize}
		    \item never stops
		    \item (re-)starts system programs
		\end {itemize}
		\lst{algo-ps1.c}{\fE}{}
	\end {itemize}
    \end {minipage}

    \smallskip

    \implique the child is immediately reparented \\
    \implique orphan status therefore does not exist in the kernel

    \medskip

    Note: on some Linux versions, the parent becomes the
    user session manager (\code{systemd}) and not the
    process 1

\end {frame}

\begin {frame} {Process states}
    Process states:
    \begin {center}
	\fig{etats}{.8}
    \end {center}
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Exécution d'un fichier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Executing a file}

\begin {frame} {Executing a file}
    \prototype {
	\fC \code {int execl (char *path, char *arg, ... )} \\
	\fC \code {int execv (char *path, char *argv [])} \\
	\fC \code {int execle (char *path, char *arg, ..., char *envp [])} \\
	\fC \code {int execve (char *path, char *argv [], char *envp [])} \\
	\fC \code {int execlp (char *file, char *arg0, ...)} \\
	\fC \code {int execvp (char *file, char *argv [])}
    }

    \begin {itemize}
	\item \code {exec*} primitives: replace the program of the
	    process with a new program and its arguments

	    \begin {itemize}
		\item the context of the process remains virtually unchanged
		    \begin {itemize}
			\item unchanged attributes: pid, ppid, uid
			    (with exceptions), umask, current directory,
			    resource consumption, etc.

			\item modified attributes: reference to executable,
			    file openings (with some exceptions
			    for 0, 1 and 2), etc.
		    \end {itemize}
		\item memory initialized with the new program
		    \begin {itemize}
			\item text, data and stack segments cannot be loaded
		    \end {itemize}
	    \end {itemize}
	\item Return value = -1 (always)
	    \begin {itemize}
		\item return \implique new unloaded program
		    \implique error
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Executing a file}
    \begin {center}
	\fig{exec}{.8}
    \end {center}
\end {frame}

\begin {frame} {Executing a file -- Example}
    No return for \code {exec*} \implique most often used
    with \code {fork}

    \medskip

    \lst{ex-exec.c}{\fD}{}
\end {frame}

\begin {frame} {Executing a file -- Example}
    Another example: Shell's (coarse) algorithm

    \begin {enumerate}
	\item read a line
	\item divide the line into elements
	\item if elements [0] $\in$ internal commands
	    \begin {itemize}
		\item then execute the corresponding function
		\item return to 1
	    \end {itemize}
	\item locate elements [0] file in \code {PATH}
	\item if not found, then error and return to 1
	\item \code {fork} \implique child
	    \begin {itemize}
		\item \code {exec (} elements \code {)}
	    \end {itemize}
	\item if elements [end] $\neq$ \code {\&}
	    \begin {itemize}
		\item then wait for the end of the child
	    \end {itemize}
	\item return to 1
    \end {enumerate}
\end {frame}

\begin {frame} {Executing a file -- Environment}
    In addition to the arguments, \code {exec*} passes the environment:

    \bigskip

    \begin {minipage} [c] {.35\linewidth}
	\fig{env}{1.2}
    \end {minipage}%
    \begin {minipage} [c] {.65\linewidth}
	\begin {itemize}
	    \item \code {extern char **environment}
	    \item library function \code {getenv}
	    \item Shell-modified environment
		\begin {itemize}
		    \item inherited by all processes launched
			by the Shell
		    \item reminder: use \code {export} in Shell to
			export an environment variable

		\end {itemize}
	\end {itemize}
    \end {minipage}
\end {frame}

\begin {frame} {Executing a file}
    Six \code {exec*} functions:

    \begin {itemize}
	\item passing arguments:
	    \ctableau {\fC} {|ll|} {
		\code {execl}
		    & in list: \code {execl (..., "echo", "a", "b", NULL)} \\
		\code {execv}
		    & in vector: \code {execv (..., tabargv)} \\
	    }

	    \medskip

	\item search in shell variable \code {PATH}:
	    \ctableau {\fC} {|ll|} {
		\code {exec[vl]}
		    & no: \code {execv ("/bin/echo", tabargv)} \\
		\code {exec[vl]p}
		    & yes: \code {execvp ("echo", tabargv)} \\
	    }

	    \medskip

	\item environment transition:

	    \ctableau {\fC} {|ll|} {
		\code {exec[vl]}
		    & implicit: \code {execv ("/bin/echo", tabargv)} \\
		\code {exec[vl]e}
		    & explicit: \code {execve ("echo", tabargv, tabenvp)} \\
	    }

    \end {itemize}

    \medskip

    Only one of these forms is a system primitive (which one?) \\
    \implique the others are library functions
\end {frame}

\begin {frame} {Magic numbers}
    \code {exec*} can execute several types of files:

    \begin {itemize}
	\item Binary files (compiled)
	    \begin {itemize}
		\item several formats possible
		    \begin {itemize}
			\item format evolution,
			    compatibility with older versions
			\item on FreeBSD: Linux compatibility mode
		    \end {itemize}
	    \end {itemize}
	\item Interpreted files
	    \begin {itemize}
		\item non-directly executable files
		    \begin {itemize}
			\item use of an interpreter
			\item Shell, Awk, Perl, Tcl, Ruby, Python, etc.
		    \end {itemize}
		\item the interpreter opens the file and ``executes'' it
	    \end {itemize}
    \end {itemize}

    \smallskip

    Presence of a ``magic number'' at the beginning of the file

    \ctableau {\fC} {|ll|} {
	binary files
	    & \code {0x7f 'E' 'L' 'F'} (ELF format under Linux) \\
	interpreted files
	    &  \code {'\#' '!'} (following the interpreter's path) \\
    }

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Droits d'exécution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Execution permissions}

\begin {frame} {Execution permissions}
    \begin {center}
	\fig{droits}{.8}
    \end {center}

    \begin {itemize}
	\item \code {fork}: child inherits uid (from Alice)
	\item \code {exec}: does not change the uid (from Alice)
	\item user Bob's \code {secret.txt} file cannot be opened
	    by a process belonging to Alice
	\item \implique the rights system works well!

    \end {itemize}
\end {frame}

\begin {frame} {Execution permissions -- Privilege elevation}
    In some cases, you need to be able to run a program with higher
    permissions:

    \begin {itemize}
	\item users change password \implique
	    they must write the new encrypted password in \code
	    {/etc/passwd}

	    \begin {itemize}
		\fC
		\item \code {/etc/passwd} cannot be modified by
		    the user \\
		    \fC
		    \code {\$ ls -l /etc/passwd} \\
		    \code {-rw-r--r-- 1 root 12345 Jan 1  1970 /etc/passwd}

	    \end {itemize}

	\item users insert their camera's SD card \\
	    \implique
	    the file system on the card must be ``mounted''

	    \begin {itemize}
		\fC
		\item the mount operation (\code
		    {mount} system primitive) is restricted to the administrator
	    \end {itemize}

	\item user wishes to use the \code {ping} command

	    \begin {itemize}
		\fC
		\item \code {ping} accesses low-level
		    network layers and requires 
		    administrator privileges
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Execution permissions -- Privilege elevation}
    Back to file permissions:

    \begin {center}
	\fig{perm}{.7}
    \end {center}

    \begin {itemize}
	\item bit ``\textit {set-user-id-on-exec}'' (or ``suid'' bit)
	\item bit ``\textit {set-group-id-on-exec}'' (or ``sgid'' bit)
    \end {itemize}
\end {frame}

\begin {frame} {Execution permissions -- set-user-id-on-exec bit}

    \begin {itemize}
	\item Call to \code {exec}: if the ``suid'' bit is set,
	    then the process uid becomes the uid of the owner
	    of the file

	    \begin {itemize}
		\item in other words: command executed with
		    the privileges of the owner (of the command) and not
		    those of the user

	    \end {itemize}

	\item Example:
	    \begin {itemize}
		\item the \code {/bin/passwd} command belongs to root
		\item in its permissions, the ``suid'' bit is at 1
		\item \implique the process can therefore modify
		    \code {/etc/passwd}
		\item \implique users can change their
		    password!
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Execution permissions -- set-user-id-on-exec bit}

    \begin {itemize}
	\item Problem 1: \code {/bin/passwd} must know the uid
	    of the user changing the password...
	    \\
	    \implique 2 distinct uid notions:

	    \ctableau {\fC} {|lll|} {
		real uid
		    & the human behind the terminal
		    & \code {uid\_t getuid (void)}
		    \\
		effective uid
		    & the uid used to test rights
		    & \code {uid\_t geteuid (void)}
		    \\
	    }
    \end {itemize}
\end {frame}

\begin {frame} {Execution permissions -- set-user-id-on-exec bit}
    \begin {itemize}
	\item Problem 2: for certain operations, you must use
	    the actual uid
	    \begin {itemize}
		\item example: file creation
		    \implique owner = effective uid
		\item sometimes it is necessary to temporarily revert to the identity
		    of the real user
		    \begin {itemize} 
			\item example: to create the file under the correct identity
		    \end {itemize}

		\item hence a third notion: ``saved'' uid
		    \begin {itemize}
			\item the saved uid saves the actual uid
			    if it is changed (with \code {int seteuid (uid\_t euid)})
			\item allows you to change to the identity of the
			    real uid, then switch back to the
			    privileged identity
		    \end {itemize}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Execution permissions -- set-user-id-on-exec bit}
    Another example:
    \begin{itemize}
	\item \code{scores} file containes the best scores of the
	    ``xyz'' game: \\
	    {\fD
		\code{Alice~7,540} \\
		\code{Bob~~~6,920} \\ 
	    }
	\item if the user \code{Carol} has a score of 8,627, she must
	    be able to write it down

	\item if this file can be modified by anyone, the user
	    \code{pirate} can open the file and modify it to enter a
		fictitious score

    \end{itemize}
    This creates a problem...

    \bigskip

    Solution:
    \begin{itemize}
	\item create a fictitious user (e.g.: \code{superxyz})
	\item \code{scores} file: belongs to \code{superxyz},
	    permissions = \code{rw-r--r--}
	\item \code{xyz} game executable: belongs to \code{superxyz}
	    with the set-user-id-on-exec bit
	\item beware of programming errors in the executable...
    \end{itemize}


\end {frame}

\begin {frame} {Execution permissions -- set-group-id-on-exec bit}
    Applying the same principles to the group:

    \begin {itemize}
	\item ``set-group-id-on-exec'' bit
	\item 3 identities
	    \begin {itemize}
		\item real gid
		\item effective gid
		\item saved gid
	    \end {itemize}
    \end {itemize}

    \bigskip
    Note:

    \begin {itemize}
	\item \code {ls} displays these bits
	\item example:

	    \smallskip

	    {\fD
	    \code {\$ ls -l /usr/bin/passwd /usr/bin/crontab} \\
	    \code {-rw\alert {s}r-xr-x 1 root root\ \ \ \  12345 Jan 1  1970 /usr/bin/passwd} \\
	    \code {-rwxr-\alert {s}r-x 1 root crontab 23456 Jan 1  1970 /usr/bin/crontab}
	    }

    \end {itemize}
\end {frame}

\begin {frame} {Execution permissions -- Privilege elevation}
    ``suid'' and ``sgid'' bits: change privilege level

    \begin {itemize}
	\item most often: to raise the level
	    \begin {itemize}
		\item example: \code{sudo} command
		\item even if it is possible to decrease it
	    \end {itemize}

	\item these are potential security issues

	    \begin {itemize}
		\item privileges \implique program with care!
		\item check the permissions
		\item no security ``hole'' \\
		    \implique buffer overflow, primitive testing,
		    etc.
	    \end {itemize}

	\item limit the number of executables with these bits

	    \begin {itemize}
		\item Example on a Linux system (Ubuntu 23.04):

		    \ctableau {\fB} {|ll|} {
			``suid'' bit & 27 files \\
			``sgid'' bit & 18 files \\
		    }

		\item \code{find / -type f -perm -04000}
	    \end {itemize}
    \end {itemize}
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redirections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Redirections and shared file openings}

\begin {frame} {Redirections}
    Shell enables redirections:

    \begin {itemize}
	\item \code {\$ wc -l < entry > result 2> errors}
	\item Reminder: 3 default openings:
	    \ctableau {\fC} {|ll|} {
		\code{0} & standard input \\
		\code{1} & standard output \\
		\code{2} & standard error output \\
	    }
	    \smallskip
	\item Redirection = modify descriptor 0, 1 or 2
	\item To be done in the child (and not in the parent), before \code {exec}
    \end {itemize}
\end {frame}

\begin {frame} {Redirections}
    Example: the shell redirects the standard output
    \begin {enumerate}
	\item read a line
	\item divide the line into elements
	\item if elements [0] $\in$ internal commands
	    \begin {itemize}
		\item then execute the corresponding function
		\item return to 1
	    \end {itemize}
	\item locate the elements [0] file in \code {PATH}
	\item if not found, then error and return to 1
	\item \code {fork} \implique child
	    \begin {itemize}
		\item \code {\alert {close (1)}}
		\item \code {\alert {open ("foo", O\_WRONLY | O\_CREAT...)}}
		    \implique returns 1
		\item \code {exec (} elements \code {)}
	    \end {itemize}
	\item if elements [end] $\neq$ \code {\&}
	    \begin {itemize}
		\item then wait for the end of the child
	    \end {itemize}
	\item return to 1
    \end {enumerate}
\end {frame}

\begin {frame} {Redirections}
    Several ways to modify descriptors:

    \begin {enumerate}
	\item close a descriptor, then open a new file

	    \begin {itemize}
		\item cf. previous example
		\item by definition, \code {open} takes the
		    smallest avaible descriptor
	    \end {itemize}
    \end {enumerate}
\end {frame}

\begin {frame} {Redirections}
    Several ways to modify descriptors:

    \begin {enumerate}
	\addtocounter {enumi} {1}

	\item \code {int dup (int fd)} primitive: duplicates a
	    file opening

	    \begin {itemize}
		\item example:
		    \lst{ex-dup.c}{\fD}{}
		\item benefit: open the file in the parent, before
		    \code {fork}, to avoid generating a process
		    in the event of an error
	    \end {itemize}

    \end {enumerate}
\end {frame}

\begin {frame} {Redirections}
    Several ways to modify descriptors:

    \begin {enumerate}
	\addtocounter {enumi} {2}
	\item primitive \code {int dup2 (int oldfd, int newfd)}

	    \begin {itemize}
		\item example:
		    \lst{ex-dup2.c}{\fD}{}
		\item benefit 1: \code {dup2} closes the
		    destination descriptor if necessary
		\item benefit 2: makes it easy to select
		    the new descriptor
	    \end {itemize}
    \end {enumerate}
\end {frame}

\begin {frame} {File opening sharing -- dup/dup2}

    Kernel data structures:
    \begin {minipage} [c] {.65\linewidth}
	\begin {itemize}
	    \item a global table for all
		file openings
	    \item one table per process for its descriptors
	\end {itemize}
    \end {minipage}%
    \begin {minipage} [c] {.35\linewidth}
	\lst{prtg-dup.c}{\fE}{}
    \end {minipage}

    \begin {center}
	\fig{prtg-dup}{.9}
    \end {center}
\end {frame}

\begin {frame} {File opening sharing -- dup/dup2}
    Reading data from the file:
    \begin {minipage} [c] {.65\linewidth}
	\begin {itemize}
	    \item offset shared between fd1 and 5
	    \item fd2's own offset
	\end {itemize}
    \end {minipage}%
    \begin {minipage} [c] {.35\linewidth}
	\lst{prtg-dup.c}{\fE}{}
    \end {minipage}

    \begin {center}
	\fig{prtg-data}{.8}
    \end {center}
\end {frame}

\begin {frame} {File opening sharing -- fork}
    What happens if \code {fork} is called after opening a
    file?

    \begin {center}
	\fig{prtg-fork}{.7}
    \end {center}

    \implique sharing the opening between the two processes
\end {frame}

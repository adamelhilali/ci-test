\def\inc{inc5-time}

\titreA {Time}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Time measurement}
    How does the kernel measure time?

    \begin {enumerate}
	\item Get time at startup
	\item Counting the passing of time
    \end {enumerate}
\end {frame}

\begin {frame} {Time measurement}
    How to obtain the start-up time?
    \begin {itemize}
	\item Solution 1: read the time on the RTC device

	    \begin {itemize}
		\item RTC: \textit {Real Time Clock}
		\item hardware clock
		\item runs on battery when power is off
	    \end {itemize}

	\item Solution 2: request time at kernel startup

	    \begin {itemize}
		\item ``historical'' solution
		\item even today (e.g.: Raspberry PI)
	    \end {itemize}

	\item Solution 3: let the kernel start at the wrong date...

	    \begin {itemize}
		\item ... and use the network to synchronize
		    the clock
		    \begin {itemize}
			\item NTP server (\emph{Network Time Protocol\/})
			\item mobile telephony protocol
		    \end {itemize}
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Time measurement}
    How to count the passing of time?

    \begin {itemize}
	\item The kernel needs to get control back at regular intervals
	\item Essential hardware assistance
	    \begin {itemize}
		\item periodic processor interrupt mechanism
		\item historically: frequency of the electrical sector
		    \begin {itemize}
			\item very stable frequency (unlike voltage)
			\item in the United States: 60 Hz \implique 60 interruptions
			    per second
			\item in Europe: 50 Hz \implique 50 interrupts
			    per second
		    \end {itemize}

		\item now: quartz-based hardware component
		    \begin {itemize}
			\item frequency programmable by the kernel
			\item depending on kernel configuration
			\item between 50 and 1000 times per second
			    (period between 1 and 20 ms)
		    \end {itemize}
	    \end {itemize}
	\item At each interrupt, increment a counter
	\item When the counter reaches the frequency
	    \begin {itemize}
		\item a second has passed
		\item increment system time
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Time units}
    The kernel uses two time units:

    \begin {enumerate}
	\item a precise moment in time
	\item short duration (e.g. CPU consumption)
    \end {enumerate}
\end {frame}

\begin {frame} {Time units -- time\_t}

    Precise instant (to the second) in time: \code {time\_t} type

    \begin {itemize}
	\item example: current time, file date, etc.
	\item value: number of seconds since ``The Epoch''
	    \begin {itemize}
		\item Epoch: january 1st 1970, 00:00:00, UTC
		\item UTC: Universal Time Coordinated
		    \\
		    {\fC (before 1986: GMT = Greenwich Mean Time)}

	    \end {itemize}

	\item time is kept in UTC
	    \begin {itemize}
		\item independent of time zone
	    \end {itemize}

	\item \code {time\_t} historically on 32 bit
	    \begin {itemize}
		\item year 2038 bug (number of seconds $\geq 2^{31}$)
		\item solution: upgrade to 64-bit (e.g. Linux $\geq 3.17$ $\rightarrow$ 2014)
		\item numerous file formats with
		    32 bit dates
	    \end {itemize}

	\item \code {time\_t} type value...
	    \begin {itemize}
		\item ... easy to maintain by the kernel
		\item ... not easy for a human to read
		    \begin {itemize}
			\item it is not the kernel's problem
		    \end {itemize}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Time units -- time\_t}

    Converting a \code {time\_t} into an intelligible value:
    \begin {itemize}
	\item ``interesting'' problem, because it takes into account:
	    \begin {itemize}
		\item the time zone
		\item summer and winter time
		\item changes in summer and winter time dates
		    \begin {itemize}
			\item e.g.: no time change before 1976
			    in France
		    \end {itemize}
		\item leap years
		\item leap seconds
		    \begin {itemize}
			\item variations in the Earth's rotational speed
			\item e.g.: 1 second added after 23:59:59"
			    the 12/31/2016
		    \end {itemize}
		\item easy? uh...
		    {\fC \url{https://youtu.be/-5wpm-gesOY}}
	    \end {itemize}

	\item ... but does not concern the kernel
	\item \implique in charge of library functions
	    \begin {itemize}
		\item \code {localtime}, \code {asctime}, \code {strftime}, etc.
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Time units -- clock\_t}
    short term: \code {clock\_t} type

    \begin {itemize}
	\item example: CPU time consumed by a process
	\item value: number of clock tops (or ``\textit {ticks}'')
	\item the unit depends on kernel configuration
	    \begin {itemize}
		\item POSIX provides the primitive
		    \code {long sysconf (int parameter})
		\item \code {parameter}: queried configuration parameter
		\item example: \code {freq = sysconf (\_SC\_CLK\_TCK)}
		    \\
		    \implique gives the number of clock beats per second
	    \end {itemize}
	\item CPU consumption measurement:
	    \begin {itemize}
		\item each time the clock is interrupted, the kernel increments
		    the current process counter
		\item \implique approximate consumption
	    \end {itemize}
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Heure courante
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Current time}

\begin {frame} {Current time}
    \prototype {
	\code {time\_t time (time\_t *hour)} \\
	\code {int stime (time\_t *hour)}
    }

    \begin {itemize}
	\item \code {time} retrieves current time
	    \begin {itemize}
		\item as return value (or -1)
		\item and to the address indicated
	    \end {itemize}
	\item \code {stime} changes the current time
	    \begin {itemize}
		\item primitive restricted to the administrator
	    \end {itemize}
    \end {itemize}

    \medskip

    Note: the current time is sometimes called ``\textit {wall clock}'' \\
    (i.e. the time shown on the wall clock)
\end {frame}

\begin {frame} {Current time}
    Example of use:

    \lst{ex-lib.c}{\fD}{}

    \begin {itemize}
	\item ``system primitive'': \code {time}
	\item library functions: \code {localtime}, \code
	    {asctime} and \code {strftime}
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Temps CPU
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {CPU time}

\begin {frame} {CPU time}
    \prototype {
	\code {clock\_t times (struct tms *buf)} \\
    }

    \begin {itemize}
	\item stores CPU consumption at the address pointed to by \code {buf}
	    \begin {itemize}
		\item \code {struct tms} content:

		    \ctableau {\fD} {|ll|} {
			\code {tms\_utime}
			    & process CPU consumption
				in user mode \\
			\code {tms\_stime}
			    & process CPU consumption
				in system mode \\
			\code {tms\_cutime}
			    & cumulative CPU consumption of children
				in user mode \\
			\code {tms\_cstime}
			    & cumulative CPU consumption of children
				in system mode \\
		    }

		    \smallskip

		\item all fields are of type \code {clock\_t}
	    \end {itemize}

	\item returns the actual time elapsed since an
	    arbitrary time in the past (or -1)

	    \begin {itemize}
		\item typically kernel startup
		\item not very useful
		\item may exceed the size allocated to a \code {clock\_t}
		\item in short: return value to be ignored (except for error
		    test)...
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {CPU time}
    CPU consumption recovery:

    \begin {center}
	\fig{times}{.5}
    \end {center}

    \begin {itemize}
	\item ``zombie'' state allows to transmit:
	    \begin {itemize}
		\item return code (\code {exit} primitive)
		\item aggregated CPU consumption in user mode
		\item aggregated CPU consumption in system mode
	    \end {itemize}
	\item information transmitted when the parent calls \code {wait}
	\item one can not get the consumption of an unfinished child
    \end {itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dates des fichiers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {File dates}

\begin {frame} {File dates}
    Reminder:

    \prototype {
	\code {int stat (const char *path, struct stat *stbuf)}
	\\
	\code {int utime (const char *path, const struct utimbuf *ut)}
    }

    \vspace* {-1ex}

    \begin {itemize}
	\item \code{stat} primitive: retrieve file attributes
	\item File attributes:

	    \ctableau {\fD} {|ll|} {
		\code {st\_mtime}
		    & date of last data modification \\
		\code {st\_ctime}
		    & date of last inode modification \\
		\code {st\_atime}
		    & last access date \\
	    }

	    \smallskip

	\item \code {utime} primitive: modification of file
	    dates
	\item \code {struct utimbuf} fields:
	    \ctableau {\fD} {|ll|} {
		\code {actime} & last access date \\
		\code {modtime} & date of last data modification \\
	    }

	    \smallskip

	\item No last inode modification \implique why?

    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minuteries de processus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Process timers}

\begin {frame} {Process timers}
    Generate an alarm after a set time:
    \prototype {
	\code {unsigned int alarm (unsigned int s)}
    }

    \begin{itemize}
	\item triggers the \code{SIGALRM} signal in
	    \code{s} seconds
	    \begin{itemize}
		\item see chapter on signals
	    \end{itemize}
	\item how to receive this notification?
	    \begin{itemize}
		\item by default: \alert{ends} the current process
		\item see chapter on signals for actual use
	    \end{itemize}
	\item only one timer possible at a time
	    \begin{itemize}
		\item if \code{s} = 0, the timer is disabled
	    \end{itemize}
    \end{itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Évolution de la précision de la mesure du temps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Precision evolution}

\begin {frame} {Time measurement accuracy}
    Time measurement accuracy is approximate
    \\
    Example of CPU time consumption:

    \begin {center}
	\fig{precision}{.9}
    \end {center}

    \begin {itemize}
	\item time for a process updated when
	    the clock interrupts the CPU
	    \begin {itemize}
		\item B's time before its I/O is not taken into account
		    \\
		    \implique time charged to C
		\item the disk interrupt is processed while
		    A is on the CPU \\
		    \implique I/O processing time from B
			imputed to A
	    \end {itemize}

	\item \implique repeat several measures
    \end {itemize}
\end {frame}

\begin {frame} {Precision evolution}
    \begin{itemize}
	\item in the 1970s, time measurement did not need to be
	    to be precise:

	    \begin{itemize}
		\item computers slower than today
		\item clock interrupt approximately every 20 ms
		\item second accuracy was sufficient
	    \end{itemize}

	\item over time, need for greater accuracy
	    \begin{itemize}
		\item \code{gettimeofday} primitive by the U. of Berkeley
		\item but microsecond precision is no longer
		    sufficient
		\item \code{gettimeofday} is now considered
		    obsolete
	    \end{itemize}

	\item POSIX: adding primitives to go down to the nanosecond level
	    \begin{itemize}
		\item 1 ns = $10^{-9}$ s
		\item the API allows ns accuracy, but not
		    implementations
	    \end{itemize}
    \end{itemize}
\end {frame}

\begin {frame} {Evolution -- Current time -- gettimeofday}
    The \code{time} primitive limits precision to the second \\
    The University of Berkeley added the primitive \code{gettimeofday}:

    \prototype {
	\code {int gettimeofday (struct timeval *tv, struct timezone *tz)}
    }

    \begin {itemize}
	\item micro-second precision
	\item \code {struct timeval} content:
	    \ctableau {\fD} {|ll|l|} {
		\code {time\_t} & \code {tv\_sec}
		    & number of seconds since ``The Epoch'' \\
		\code {suseconds\_t}  & \code {tv\_usec}
		    & number of microseconds since the last second \\
	    }

	    \smallskip

	\item caution: just because there is a field whose
	    unit is the $\mu$s does not mean that the granularity
	    of time measurement is $\mu$s

	\item \code {time} has now become a
	    function that calls the \code {gettimeofday} primitive

    \end {itemize}
\end {frame}

\begin {frame} {Evolution -- Current time -- gettimeofday}
    \code {struct timeval} example:

    \bigskip
    
    \begin {minipage} {.35\linewidth}
    \ctableau {\fD} {|lr|} {
	\code {tv\_sec}
	    & 1,500,000,000
	    \\
	\code {tv\_usec}
	    & 123,456
	    \\
    }
    \end {minipage}
    \hfill
    $\Leftrightarrow$
    \hfill
    \begin {minipage} {.55\linewidth}
	\fC 07/14/2017 at 02:40:00 and 123456 $\mu s$ UTC
    \end {minipage}

    \bigskip

    \begin {center}
	\fig{timeval}{.9}
    \end {center}
\end {frame}

\begin {frame} {Evolution -- Current time -- clock\_gettime}
    \label{clockgettime}
    POSIX added for nanosecond accuracy:
    \vspace* {-3ex}
    \prototype {
	\code {int clock\_gettime (clockid\_t clock, struct timespec *ts )}
    }
    \vspace* {-1ex}

    \begin{itemize}
	\item returns the current time in the \code{clock} reference frame:

	    \ctableau{\fD}{|ll|}{
		\code{CLOCK\_REALTIME} & actual current date and time \\
		\code{CLOCK\_MONOTONIC} & monotonous clock \\
		\code{CLOCK\_PROCESS\_CPUTIME\_ID} & CPU time consumed by the process \\
		\code{CLOCK\_THREAD\_CPUTIME\_ID} & CPU time consumed by the thread \\
	    }

	    \smallskip

	    \code{CLOCK\_MONOTONIC} is a clock that never goes back,
	    even if the current time is adjusted (to set it back)

	\item uses a \code{struct timespec} with fields:

	    \ctableau {\fD} {|ll|l|} {
		\code {time\_t} & \code {tv\_sec}
		    & number of seconds since ``The Epoch'' \\
		\code {long}  & \code {tv\_nsec}
		    & number of nanoseconds since the last second \\
	    }

	    \smallskip

	    \implique similar to \code{gettimeofday}'s \code{struct timeval} 
		\begin{itemize}
		    \item uses \code{tv\_nsec} instead of \code{tv\_usec}
		    \item uses ns instead of $\mu$s
		\end{itemize}

	\item to be preferred from now on (instead of \code{time}
	    and \code{gettimeofday})

    \end{itemize}

\end {frame}

\begin {frame} {Evolution -- CPU time -- getrusage}
    \code{times} primitive: limited to CPU times and seconds

    \prototype {
	\code {int getrusage (int who, struct rusage *res)}
    }

    \begin {itemize}
	\item primitive \code {times} limited to CPU consumption
	\begin {itemize}
	\item \implique need a more general primitive to get
	    all resources used by a process
	\end {itemize}
	\item parameter \code {who}:
	    \ctableau {\fD} {|ll|} {
		\code {RUSAGE\_SELF}
		    & consumption of the process itself \\
		\code {RUSAGE\_CHILDREN}
		    & cumulative children consumption \\
	    }

	\item examples (not exhaustive) of \code {struct rusage} fields:

	    \ctableau {\fD} {|ll|} {
		\code {ru\_utime} & CPU in user mode \\
		\code {ru\_stime} & CPU in system mode \\
		\code {ru\_maxrss} & maximum memory used \\
		\code {ru\_inblock} & number of disk readings \\
		\code {ru\_outblock} & number of disk writes \\
	    }

	    Note: only the first two are standardized by POSIX

	\item values transmitted from son to father during \code{wait}

    \end {itemize}
\end {frame}

\begin {frame} {Evolution -- File dates -- stat}
    Originally, Unix only managed file dates to the nearest second.

    \medskip

    POSIX has changed things:
    \begin{itemize}
	\item evolution of \code{st\_mtime},
	    \code{st\_ctime} and \code{st\_atime} fields

	\item new definition:

	    \ctableau {\fD} {|ll|} {
		\code {st\_mtim}
		    & date of last data modification \\
		\code {st\_ctim}
		    & date of last inode modification \\
		\code {st\_atim}
		    & last access date \\
	    }

	\item each of these fields is a \code{struct timespec}
	    \\
	    \implique as for \code{clock\_gettime}

	\item compatibility preserved. For example, \code{stat.h} contains:

	    \code{\#define st\_mtime st\_mtim.tv\_sec}

	    \implique applications can continue to use
	    \code{st\_mtime}

    \end{itemize}
\end {frame}

\begin {frame} {Evolution -- File dates -- utimes}
    \code{utimes} primitive replaces \code{utime} which is limited to the second

    \prototype {
	\code {int utimes (const char *path, const struct timeval tv [2])}
    }

    \begin{itemize}
	\item \code {utimes} primitive: similar to \code {utime}, but with
	    \code {struct timeval} (like \code {gettimeofday})
	    \begin {itemize}
		\item \code {tv[0]}: last acess
		\item \code {tv[1]}: last data modification
	    \end {itemize}
    \end{itemize}
\end {frame}

\begin {frame} {Evolution -- Process timers}
    \code{alarm} primitive: too restricted

    \begin{itemize}
	\item POSIX has defined \emph{timers}

	    \begin{itemize}
		\item multiple timer options
		\item choice of reference clock \\
		    \implique cf. \code{clock\_gettime}
			(page~\ref{clockgettime})
		\item choice of notification mechanism
		    \\
		    \implique nothing (not very useful), send signal, call function
	    \end{itemize}

	\item main primitives:

	    \ctableau {\fC} {|ll|} {
		\code{timer\_create} & creating a timer \\
		\code{timer\_settime} & setting a timer \\
		\code{timer\_gettime} & consulting a timer \\
		\code{timer\_getoverrun} & number of missed notifications \\
		\code{timer\_delete} & deleting a timer \\
	    }

	    \implique see manual
    \end{itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bilan
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Summary}

\begin {frame} {Summary}
    Three main families of primitives are standardized by POSIX:
    \begin{itemize}
	\item ``historicals'': \code{time}, \code{times}, \code{stat},
	    \code{utime}
	    \\
	    \implique based on \code{time\_t} and \code{clock\_t} types
	\item based on \code{struct timeval}: \code{gettimeofday},
	    \code{utimes}...
	    \\
	    \implique from U. of Berkeley, precision limited
		to $\mu$s, obsolete
	\item ``POSIX creations'': \code{clock\_gettime}, \code{timer\_*}
	    \\
	    \implique based on \code{struct timespec}
	    \\
	    \implique perennial, ns precision, but more complex
    \end{itemize}

    \begin{casseroleblock} {Which primitives to use?}
	\begin{itemize}
	    \item common use: ``historicals''
	    \item need for greater precision: ``POSIX creations''
	\end{itemize}
    \end{casseroleblock}
\end {frame}

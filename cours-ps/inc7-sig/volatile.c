#include <stdio.h>
#include <signal.h>

volatile sig_atomic_t arret = 0 ;

void f (int sig) {
    arret = 1 ; // sig\_atomic\_t $\Rightarrow$ affectation en "une" opération
}

int main (...) {
    struct sigaction s ;

    s.sa_handler = f ;
    ...
    sigaction (SIGINT, &s, NULL) ;

    while (! arret) // volatile $\Rightarrow$ variable relue à chaque itération
	calcul () ;

    // si on arrive ici, c'est que le signal a été pris en compte
}

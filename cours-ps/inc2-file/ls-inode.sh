> ls -a -i /		# \texttt{-a} : affiche aussi les entrées commençant par «~\texttt{.}~»
2 .
2 ..
3 etc
8 usr
...

> ls -ai /usr
8 .
2 ..
9 bin
10 include
...
